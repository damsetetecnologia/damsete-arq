package br.com.damsete.arq.audit.config;

import br.com.damsete.arq.audit.infrastructure.filter.OperationFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

/**
 * Created by andre on 21/06/2017.
 */
@Configuration
public class AuditFilterConfig implements EnvironmentAware {

    private Environment environment = null;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public FilterRegistrationBean operationFilter() {
        FilterRegistrationBean operationFilter = new FilterRegistrationBean(new OperationFilter(this.environment.getProperty("damsete.audit.system")));
        operationFilter.setDispatcherTypes(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.ERROR));
        operationFilter.addUrlPatterns("/*");
        operationFilter.addUrlPatterns("*.*");
        operationFilter.addInitParameter("extensionsDiscarded", "jpg,gif,doc,xls,png,js,css,ico,jar,jnlp,svg,woff2");
        operationFilter.addInitParameter("directoriesDiscarded", "/javax.faces.resource");
        operationFilter.setOrder(Ordered.LOWEST_PRECEDENCE);
        return operationFilter;
    }
}
