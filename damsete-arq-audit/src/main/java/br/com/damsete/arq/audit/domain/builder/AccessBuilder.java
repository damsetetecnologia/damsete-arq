package br.com.damsete.arq.audit.domain.builder;

import br.com.damsete.arq.audit.domain.model.Access;
import br.com.damsete.arq.log.core.LogKeyGenerator;
import br.com.damsete.arq.utils.Networks;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by andre on 21/10/2017.
 */
public class AccessBuilder {

    private Access access = null;

    public AccessBuilder(String username, String system, String ip, String userAgent) {
        this.access = new Access();

        this.access.setIdAccess(new LogKeyGenerator().generateKey(system));
        this.access.setDateTime(LocalDateTime.now());
        this.access.setHash(UUID.randomUUID().toString());
        this.access.setSystem(system);
        this.access.setUsername(username);
        this.access.setIp(ip);
        this.access.setIpNat(Networks.getLocalAddress());
        this.access.setServer(Networks.getLocalName());
        this.access.setUserAgent(userAgent);
    }

    public Access build() {
        return this.access;
    }
}
