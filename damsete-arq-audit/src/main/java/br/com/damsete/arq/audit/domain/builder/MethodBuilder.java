package br.com.damsete.arq.audit.domain.builder;

import br.com.damsete.arq.audit.domain.model.Method;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Created by andre on 15/03/2018.
 */
public class MethodBuilder {

    private Method method = null;

    public MethodBuilder(String name, Object[] arguments, Object result, long duration) {
        this.method = new Method();
        this.method.setDateTime(LocalDateTime.now());
        this.method.setName(name);

        if (arguments != null) {
            this.method.setArguments(Arrays.toString(arguments));
        }

        if (result != null) {
            this.method.setResult(result.toString());
        }

        this.method.setDuration(duration);
    }

    public Method build() {
        return this.method;
    }
}
