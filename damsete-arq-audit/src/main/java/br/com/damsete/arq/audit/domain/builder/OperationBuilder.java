package br.com.damsete.arq.audit.domain.builder;

import br.com.damsete.arq.audit.domain.model.Error;
import br.com.damsete.arq.audit.domain.model.Operation;
import br.com.damsete.arq.audit.utils.Errors;
import br.com.damsete.arq.audit.utils.Params;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * Created by andre on 21/10/2017.
 */
public class OperationBuilder {

    private Operation operation = null;

    public OperationBuilder(String system, String url, Map<String, String[]> params, long duration, Throwable e) {
        this.operation = new Operation();
        this.operation.setDateTime(LocalDateTime.now());
        this.operation.setSystem(system);
        this.operation.setUrl(url);
        this.operation.setDuration(duration);

        this.operation.setParams(Params.paramMapToString(params));

        if (e != null) {
            Error error = Errors.fromException(e);
            error.setDateTime(this.operation.getDateTime());
            this.operation.setError(error);
        }
    }

    public Operation build() {
        return this.operation;
    }
}
