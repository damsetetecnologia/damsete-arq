package br.com.damsete.arq.audit.domain.model;

import br.com.damsete.arq.log.core.LogPersistable;
import br.com.damsete.arq.utils.Dates;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.time.LocalDateTime;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by andre on 09/04/2016.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Access implements LogPersistable, Serializable {

    private static final long serialVersionUID = 1L;

    private String idAccess = null;
    private LocalDateTime dateTime = null;
    private String hash = null;
    private String system = null;
    private String username = null;
    private String ip = null;
    private String ipNat = null;
    private String server = null;
    private String userAgent = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public String getLog() {
        if (isEmpty(this.username)) {
            this.username = "";
        }

        if (isEmpty(this.ip)) {
            this.ip = "";
        }

        if (isEmpty(this.ipNat)) {
            this.ipNat = "";
        }

        if (isEmpty(this.system)) {
            this.system = "";
        }

        if (isEmpty(this.userAgent)) {
            this.userAgent = "";
        }

        if (isEmpty(this.idAccess)) {
            this.idAccess = "";
        }

        return String.format("%s %s %s %s %s %s %s",
                Dates.formatLocalDate(this.dateTime, "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"),
                this.username, this.ip, this.ipNat, this.system,
                this.server, this.idAccess);
    }
}