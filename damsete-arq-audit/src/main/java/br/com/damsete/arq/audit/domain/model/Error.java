package br.com.damsete.arq.audit.domain.model;

import br.com.damsete.arq.log.core.LogPersistable;
import br.com.damsete.arq.utils.Dates;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.time.LocalDateTime;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by andre on 09/04/2016.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Error implements LogPersistable, Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime dateTime = null;
    private String message = null;
    private String rootCause = null;
    private String shortTrace = null;
    private String className = null;
    private String methodName = null;
    private String rootClassName = null;
    private String rootMethodName = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public String getLog() {
        if (isEmpty(this.message)) {
            this.message = "";
        }
        if (isEmpty(this.rootCause)) {
            this.rootCause = "";
        }
        if (isEmpty(this.shortTrace)) {
            this.shortTrace = "";
        }
        if (isEmpty(this.className)) {
            this.className = "";
        }
        if (isEmpty(this.methodName)) {
            this.methodName = "";
        }
        if (isEmpty(this.rootClassName)) {
            this.rootClassName = "";
        }
        if (isEmpty(this.rootMethodName)) {
            this.rootMethodName = "";
        }
        return String.format("%s %s %s %s %s %s %s %s",
                Dates.formatLocalDate(this.dateTime, "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"),
                this.message, this.rootCause, this.shortTrace, this.className,
                this.methodName, this.rootClassName, this.rootMethodName);
    }
}