package br.com.damsete.arq.audit.domain.model;

import br.com.damsete.arq.log.core.LogPersistable;
import br.com.damsete.arq.utils.Dates;
import lombok.*;

import java.time.LocalDateTime;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by andre on 15/03/2018.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Method implements LogPersistable {

    private LocalDateTime dateTime = null;
    private String name = null;
    private String arguments = null;
    private String result = null;
    private long duration = 0;

    @Override
    public String getLog() {
        if (isEmpty(this.name)) {
            this.name = "";
        }
        if (isEmpty(this.arguments)) {
            this.arguments = "";
        }
        if (isEmpty(this.result)) {
            this.result = "";
        }
        return String.format("%s %s %s %s %sms",
                Dates.formatLocalDate(this.dateTime, "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"),
                this.name, this.arguments, this.result, this.duration);
    }
}
