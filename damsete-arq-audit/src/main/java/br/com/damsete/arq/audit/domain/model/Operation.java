package br.com.damsete.arq.audit.domain.model;

import br.com.damsete.arq.log.core.LogPersistable;
import br.com.damsete.arq.utils.Dates;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.time.LocalDateTime;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * Created by andre on 09/04/2016.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Operation implements LogPersistable, Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime dateTime = null;
    private String system = null;
    private String url = null;
    private String params = null;
    private long duration = 0;
    private Error error = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public String getLog() {
        if (isEmpty(this.params)) {
            this.params = "null";
        }
        return String.format("%s %s %s %s %s %s",
                Dates.formatLocalDate(this.dateTime, "yyyy-MM-dd'T'HH:mm:ss.SSSS'Z'"),
                this.system, this.url, this.params, this.duration, this.error != null);
    }
}
