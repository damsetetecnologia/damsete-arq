package br.com.damsete.arq.audit.infrastructure.aspect;

import br.com.damsete.arq.audit.domain.builder.MethodBuilder;
import br.com.damsete.arq.audit.domain.model.Method;
import br.com.damsete.arq.log.core.LoggerAPI;
import org.apache.logging.log4j.Level;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StopWatch;

/**
 * Created by andre on 14/03/2018.
 */
@Aspect
@Configuration
public class MethodLogger {

    @Around("execution(* *(..)) && @annotation(br.com.damsete.arq.annotation.MethodLoggable)")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        StopWatch watch = new StopWatch();

        Object result = null;

        try {
            watch.start();

            result = point.proceed();
        } finally {
            watch.stop();

            Method method = new MethodBuilder(MethodSignature.class.cast(point.getSignature()).getMethod().getName(),
                    point.getArgs(), result, watch.getTotalTimeMillis()).build();

            LoggerAPI.log(Level.INFO, method);
        }

        return result;
    }
}
