package br.com.damsete.arq.audit.infrastructure.filter;

import br.com.damsete.arq.audit.domain.builder.AccessBuilder;
import br.com.damsete.arq.audit.domain.model.Access;
import br.com.damsete.arq.audit.utils.Constants;
import br.com.damsete.arq.log.core.LoggerAPI;
import br.com.damsete.arq.security.infraestructure.filter.AfterLogonFilter;
import br.com.damsete.arq.security.application.authenticate.dto.Credential;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by andre on 16/04/2016.
 */
@Component
public class AccessFilter implements EnvironmentAware, AfterLogonFilter {

    private Environment environment = null;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void processAfterLogon(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        try {
            String system = this.environment.getProperty("damsete.audit.system", "none");

            Credential credential = (Credential) authentication.getPrincipal();

            Access access = new AccessBuilder(credential.getUsername(), system.toLowerCase(), request.getRemoteAddr(),
                    request.getHeader("User-Agent")).build();

            ThreadContext.put(Constants.HASH_ACCESS_SESSION_ATTRIBUTE, access.getHash());

            request.getSession(true).setAttribute(Constants.ACCESS_SESSION_ATTRIBUTE, access);

            LoggerAPI.log(Level.INFO, access);
        } finally {
            ThreadContext.remove(Constants.HASH_ACCESS_SESSION_ATTRIBUTE);
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
