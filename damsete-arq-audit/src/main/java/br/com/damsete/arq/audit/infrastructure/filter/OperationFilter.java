package br.com.damsete.arq.audit.infrastructure.filter;

import br.com.damsete.arq.audit.domain.builder.AccessBuilder;
import br.com.damsete.arq.audit.domain.builder.OperationBuilder;
import br.com.damsete.arq.audit.domain.model.Access;
import br.com.damsete.arq.audit.domain.model.Operation;
import br.com.damsete.arq.audit.utils.Constants;
import br.com.damsete.arq.log.core.LoggerAPI;
import br.com.damsete.arq.utils.Filters;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.util.StopWatch;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class OperationFilter extends OncePerRequestFilter {

    @Getter
    @Setter
    private String extensionsDiscarded = null;
    @Getter
    @Setter
    private String directoriesDiscarded = null;

    private String system;

    public OperationFilter(String system) {
        this.system = system;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        if (StringUtils.isEmpty(this.system)) {
            throw new IllegalArgumentException("unidentified system");
        }

        if (!Filters.isExtensionDiscarded(request, this.extensionsDiscarded) && !Filters.isDirectoryDiscarded(request, this.directoriesDiscarded)) {
            StopWatch watch = new StopWatch();
            try {
                watch.start();

                ThreadContext.put(Constants.HASH_OPERATION_SESSION_ATTRIBUTE, UUID.randomUUID().toString());

                Access access = (Access) request.getSession(true).getAttribute(Constants.ACCESS_SESSION_ATTRIBUTE);
                if (access == null) {
                    access = getAndLogPublicAccess(request, this.system);
                }

                ThreadContext.put(Constants.HASH_ACCESS_SESSION_ATTRIBUTE, access.getHash());

                chain.doFilter(request, response);
            } finally {
                watch.stop();

                logOperation(request, watch, this.system);

                ThreadContext.remove(Constants.HASH_ACCESS_SESSION_ATTRIBUTE);
                ThreadContext.remove(Constants.HASH_OPERATION_SESSION_ATTRIBUTE);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    private Access getAndLogPublicAccess(HttpServletRequest req, String system) {
        Access access = new AccessBuilder("public", system.toLowerCase(), req.getRemoteAddr(),
                req.getHeader("User-Agent")).build();

        ThreadContext.put(Constants.HASH_ACCESS_SESSION_ATTRIBUTE, access.getHash());

        req.getSession(true).setAttribute(Constants.ACCESS_SESSION_ATTRIBUTE, access);

        LoggerAPI.log(Level.INFO, access);

        return access;
    }

    private void logOperation(HttpServletRequest req, StopWatch watch, String system) {
        String url = req.getRequestURL().toString() + (req.getQueryString() != null ? "?" + req.getQueryString() : "");

        Operation operation = new OperationBuilder(system.toLowerCase(), url,
                req.getParameterMap(), watch.getTotalTimeMillis(),
                (Throwable) req.getAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE)).build();

        LoggerAPI.log(Level.INFO, operation);
        if (operation.getError() != null) {
            LoggerAPI.log(Level.ERROR, operation.getError());
        }
    }
}
