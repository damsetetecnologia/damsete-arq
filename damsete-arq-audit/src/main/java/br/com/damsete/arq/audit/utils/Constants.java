package br.com.damsete.arq.audit.utils;

/**
 * Created by andre on 21/10/2017.
 */
public interface Constants {

    String HASH_OPERATION_SESSION_ATTRIBUTE = "hash_operation";
    String HASH_ACCESS_SESSION_ATTRIBUTE = "hash_access";
    String ACCESS_SESSION_ATTRIBUTE = "__acessInSession";
}
