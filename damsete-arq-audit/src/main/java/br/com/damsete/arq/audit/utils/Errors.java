package br.com.damsete.arq.audit.utils;

import br.com.damsete.arq.audit.domain.model.Error;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.exception.ExceptionUtils.getMessage;
import static org.apache.commons.lang3.exception.ExceptionUtils.getRootCauseMessage;

/**
 * Created by andre on 16/04/2016.
 */
public class Errors {

    private static final String INDENT = "\t";
    private static List<String> _suppressedPackages = newArrayList("org.hibernate", "$Proxy", "org.junit", "java.lang.reflect.Method", "sun.", "org.eclipse", "junit.framework", "org.springframework", "java.lang.Thread", "javax.servlet", "br.ufrn.arq.auditoria.advice");
    private static List<String> _suppressedSuffixes = newArrayList("$$FastClassBySpringCGLIB", "$$EnhancerBySpringCGLIB");

    public static Error fromException(Throwable exception) {
        if (exception == null) {
            return null;
        }

        Error erro = new Error();
        erro.setMessage(getMessage(exception));
        erro.setShortTrace(getFilteredStackTrace(exception));
        erro.setRootCause(getRootCauseMessage(exception));

        StackTraceElement element = exception.getStackTrace()[0];
        erro.setClassName(element.getClassName());
        erro.setMethodName(element.getMethodName());

        Throwable rootCause = ExceptionUtils.getRootCause(exception);
        if (rootCause != null) {
            StackTraceElement rootElement = rootCause.getStackTrace()[0];
            erro.setRootClassName(rootElement.getClassName());
            erro.setRootMethodName(rootElement.getMethodName());
        }

        return erro;
    }

    public static String getFilteredStackTrace(Throwable t) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            writeCleanStackTrace(t, pw);
            return sw.getBuffer().toString();
        } catch (Exception e) {
            e.printStackTrace();
            return e.toString();
        }
    }

    private static void writeCleanStackTrace(Throwable t, PrintWriter s) {
        s.print("Exception: ");
        printExceptionChain(t, s);
        Set<String> skippedPackages = newHashSet();
        int skippedLines = 0;
        boolean shouldFilter = filtersEnabled();
        for (StackTraceElement traceElement : getBottomThrowable(t).getStackTrace()) {
            String forbiddenPackageName = null;
            String forbiddenSuffixName = null;

            if (shouldFilter) {
                forbiddenPackageName = tryGetForbiddenPackageName(traceElement);
                forbiddenSuffixName = tryGetForbiddenSuffixName(traceElement);
            }

            if (forbiddenPackageName == null && forbiddenSuffixName == null) {
                if (skippedPackages.size() > 0) {
                    //37 lines skipped for [org.h2, org.hibernate, sun., java.lang.reflect.Method, $Proxy]
                    s.println(getSkippedPackagesMessage(skippedPackages, skippedLines));
                }
                //at hib.HibExample.test(HibExample.java:18)
                s.println(INDENT + "at " + traceElement);
                skippedPackages.clear();
                skippedLines = 0;
            } else {
                skippedLines++;
                if (forbiddenPackageName != null) {
                    skippedPackages.add(forbiddenPackageName);
                }
                if (forbiddenSuffixName != null) {
                    skippedPackages.add(forbiddenSuffixName);
                }
            }
        }
        if (skippedLines > 0) {
            s.println(getSkippedPackagesMessage(skippedPackages, skippedLines));
        }
    }

    private static String getSkippedPackagesMessage(Set<String> skippedPackages, int skippedLines) {
        return INDENT + skippedLines + " line" + (skippedLines == 1 ? "" : "s") + " skipped for " + skippedPackages;
    }

    private static Throwable getBottomThrowable(Throwable t) {
        while (t.getCause() != null) {
            t = t.getCause();
        }
        return t;
    }

    private static boolean filtersEnabled() {
        return true;
    }

    private static void printExceptionChain(Throwable t, PrintWriter s) {
        s.println(t);
        if (t.getCause() != null) {
            s.print("Caused by: ");
            printExceptionChain(t.getCause(), s);
        }
    }

    private static String tryGetForbiddenPackageName(StackTraceElement traceElement) {
        String classAndMethod = traceElement.getClassName() + "." + traceElement.getMethodName();
        for (String pkg : _suppressedPackages) {
            if (classAndMethod.startsWith(pkg)) {
                return pkg;
            }
        }
        return null;
    }

    private static String tryGetForbiddenSuffixName(StackTraceElement traceElement) {
        String classAndMethod = traceElement.getClassName() + "." + traceElement.getMethodName();
        for (String pkg : _suppressedSuffixes) {
            if (classAndMethod.contains(pkg)) {
                return pkg;
            }
        }
        return null;
    }
}
