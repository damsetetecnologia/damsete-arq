package br.com.damsete.arq.audit.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static br.com.damsete.arq.utils.Strings.join;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by andre on 23/10/2017.
 */
public class Params {

    public static String paramMapToString(Map<String, String[]> params) {
        if (params != null) {
            List<String> paramsList = newArrayList();
            StringBuilder builder;

            try {
                Set<Map.Entry<String, String[]>> paramsEntrySet = params.entrySet();
                for (Map.Entry<String, String[]> entry : paramsEntrySet) {
                    builder = new StringBuilder(5000);

                    if (!entry.getKey().toString().startsWith("jsf_state")
                            && !entry.getKey().toString().startsWith("jsf_tree")
                            && !entry.getKey().toString().startsWith("javax.faces")
                            && !entry.getKey().toString().contains("password")) {
                        builder.append(entry.getKey());
                        builder.append(" = ");
                        builder.append(Arrays.toString(entry.getValue()));

                        paramsList.add(builder.toString());
                    }
                }
            } catch (Exception e) {
                builder = new StringBuilder(500);
                builder.append("PARAM_ERROR = STACK: ");
                builder.append(Arrays.toString(e.getStackTrace()));

                paramsList.add(builder.toString());
            }

            return join(paramsList, "; ");
        }

        return null;
    }
}
