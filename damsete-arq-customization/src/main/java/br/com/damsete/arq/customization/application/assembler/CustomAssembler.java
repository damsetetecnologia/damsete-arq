package br.com.damsete.arq.customization.application.assembler;

import br.com.damsete.arq.customization.application.dto.CustomDto;
import br.com.damsete.arq.customization.domain.model.Custom;
import br.com.damsete.arq.data.application.AbstractAssembler;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CustomAssembler extends AbstractAssembler<CustomDto, Custom> {

    @Override
    public Custom fromDto(CustomDto customDto) {
        ModelMapper modelMapper = new ModelMapper();
        Custom custom = modelMapper.map(customDto, Custom.class);
        return custom;
    }

    @Override
    public CustomDto toDto(Custom custom) {
        ModelMapper modelMapper = new ModelMapper();
        CustomDto customDto = modelMapper.map(custom, CustomDto.class);
        return customDto;
    }
}
