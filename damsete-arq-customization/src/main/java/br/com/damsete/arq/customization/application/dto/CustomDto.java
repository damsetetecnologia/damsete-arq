package br.com.damsete.arq.customization.application.dto;

import br.com.damsete.arq.customization.domain.model.CustomType;
import br.com.damsete.arq.data.application.PersistableDto;
import lombok.*;

/**
 * Created by andre on 28/04/2016.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class CustomDto extends PersistableDto<Long> {

    private Long id = null;
    private String code = null;
    private CustomType type = null;
    private String value = null;
    private String name = null;
}
