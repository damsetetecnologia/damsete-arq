package br.com.damsete.arq.customization.application.service;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.customization.domain.model.Custom;
import br.com.damsete.arq.customization.infrastructure.repository.CustomRepository;
import br.com.damsete.arq.exception.MissingConfigurationException;
import br.com.damsete.arq.exception.WrongConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.collect.Maps.newHashMap;

/**
 * Created by andre on 28/04/2016.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class CustomizationService {

    @Autowired
    private CustomRepository customRepository = null;

    @MethodLoggable
    public boolean getCustomAsBoolean(String code) throws MissingConfigurationException {
        return Boolean.valueOf(getCustom(code));
    }

    @MethodLoggable
    public Date getCustomAsData(String code) throws MissingConfigurationException {
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(getCustom(code));
        } catch (ParseException e) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(new String[]{"custom.wrong.message"}, new Object[]{code}));
        }
    }

    @MethodLoggable
    public double getCustomAsDouble(String code) throws MissingConfigurationException {
        return Double.parseDouble(getCustom(code));
    }

    @MethodLoggable
    public float getCustomAsFloat(String code) throws MissingConfigurationException {
        return Float.parseFloat(getCustom(code));
    }

    @MethodLoggable
    public int getCustomAsInteger(String code) throws MissingConfigurationException {
        return Integer.parseInt(getCustom(code));
    }

    @MethodLoggable
    public List<String> getCustomAsListaString(String code) throws MissingConfigurationException {
        String param = getCustom(code);
        String[] array = StringUtils.split(param, ", ");
        return Arrays.stream(array).map(i -> i).collect(Collectors.toList());
    }

    @MethodLoggable
    public long getCustomAsLong(String code) throws MissingConfigurationException {
        return Long.parseLong(getCustom(code));
    }

    @MethodLoggable
    public Map<String, String> getCustomAsMap(String code) throws MissingConfigurationException {
        Map<String, String> map = newHashMap();
        String param = getCustom(code);
        for (String line : param.split("\n")) {
            String[] parts = line.split("=");
            map.put(parts[0].trim(), parts[1].trim());
        }
        return map;
    }

    @MethodLoggable
    public String getCustomAsString(String code) throws MissingConfigurationException {
        return getCustom(code);
    }

    @MethodLoggable
    private String getCustom(String code) throws MissingConfigurationException {
        Optional<Custom> custom = this.customRepository.findByCode(code);

        if (!custom.isPresent()) {
            throw new MissingConfigurationException(new DefaultMessageSourceResolvable(new String[]{"custom.notfound.message"}, new Object[]{code}));
        }

        return custom.get().getValue();
    }
}
