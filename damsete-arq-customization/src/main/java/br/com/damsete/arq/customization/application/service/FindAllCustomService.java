package br.com.damsete.arq.customization.application.service;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.customization.application.assembler.CustomAssembler;
import br.com.damsete.arq.customization.application.dto.CustomDto;
import br.com.damsete.arq.customization.infrastructure.repository.CustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.List;

import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNumericSpace;

@Service
@Transactional(rollbackFor = Exception.class)
public class FindAllCustomService {

    private CustomRepository customRepository;
    private CustomAssembler customAssembler;

    @Autowired
    public FindAllCustomService(CustomRepository customRepository, CustomAssembler customAssembler) {
        this.customRepository = customRepository;
        this.customAssembler = customAssembler;
    }

    @MethodLoggable
    public Page<CustomDto> findAllCustom(String query, Pageable pageable) {
        Page page = this.customRepository.findAll((r, q, c) -> {
            List<Predicate> predicate = newArrayList();

            ofNullable(query).filter(i -> isNotEmpty(i) && isNumericSpace(i)).ifPresent(i -> {
                predicate.add(c.equal(r.get("code"), Long.parseLong(i)));
            });
            ofNullable(query).filter(i -> isNotEmpty(i) && !isNumericSpace(i)).ifPresent(i -> {
                predicate.add(c.like(c.upper(r.get("name")), "%" + i.toUpperCase() + "%"));
            });

            return q.where(toArray(predicate, Predicate.class)).getRestriction();
        }, pageable);
        List<CustomDto> customDtos = this.customAssembler.toListDto(page.getContent());
        return PageableExecutionUtils.getPage(customDtos, pageable, () -> page.getTotalElements());
    }
}
