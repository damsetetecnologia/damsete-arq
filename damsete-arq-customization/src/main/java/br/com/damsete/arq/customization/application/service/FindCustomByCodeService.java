package br.com.damsete.arq.customization.application.service;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.customization.application.assembler.CustomAssembler;
import br.com.damsete.arq.customization.application.dto.CustomDto;
import br.com.damsete.arq.customization.domain.model.Custom;
import br.com.damsete.arq.customization.infrastructure.repository.CustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Lists.newArrayList;

@Service
@Transactional(rollbackFor = Exception.class)
public class FindCustomByCodeService {

    private CustomRepository customRepository;
    private CustomAssembler customAssembler;

    @Autowired
    public FindCustomByCodeService(CustomRepository customRepository, CustomAssembler customAssembler) {
        this.customRepository = customRepository;
        this.customAssembler = customAssembler;
    }

    @MethodLoggable
    public Optional<CustomDto> findCustomByCode(String code) {
        Optional<Custom> custom = this.customRepository.findOne((r, q, c) -> {
            List<Predicate> predicates = newArrayList();
            predicates.add(c.equal(r.get("code"), code));
            return q.where(toArray(predicates, Predicate.class)).getRestriction();
        });
        return Optional.ofNullable(custom.isPresent() ? this.customAssembler.toDto(custom.get()) : null);
    }
}
