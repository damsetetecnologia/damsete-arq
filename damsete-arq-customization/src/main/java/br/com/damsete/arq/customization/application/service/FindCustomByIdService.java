package br.com.damsete.arq.customization.application.service;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.customization.application.assembler.CustomAssembler;
import br.com.damsete.arq.customization.application.dto.CustomDto;
import br.com.damsete.arq.customization.domain.model.Custom;
import br.com.damsete.arq.customization.infrastructure.repository.CustomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional(rollbackFor = Exception.class)
public class FindCustomByIdService {

    private CustomRepository customRepository;
    private CustomAssembler customAssembler;

    @Autowired
    public FindCustomByIdService(CustomRepository customRepository, CustomAssembler customAssembler) {
        this.customRepository = customRepository;
        this.customAssembler = customAssembler;
    }

    @MethodLoggable
    public Optional<CustomDto> findCustomById(Long id) {
        Optional<Custom> custom = this.customRepository.findById(id);
        return Optional.ofNullable(custom.isPresent() ? this.customAssembler.toDto(custom.get()) : null);
    }
}
