package br.com.damsete.arq.customization.application.service;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.customization.application.command.UpdateCustomCommand;
import br.com.damsete.arq.customization.domain.model.Custom;
import br.com.damsete.arq.customization.infrastructure.repository.CustomRepository;
import br.com.damsete.arq.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class UpdateCustomService {

    private CustomRepository customRepository;

    @Autowired
    public UpdateCustomService(CustomRepository customRepository) {
        this.customRepository = customRepository;
    }

    @MethodLoggable
    public void updateCustom(UpdateCustomCommand command) {
        Custom custom = this.customRepository.findById(command.getId()).orElse(null);

        if (custom == null) {
            throw new BusinessException(new DefaultMessageSourceResolvable("generic.notfound.message"));
        }

        custom.setValue(command.getValue());

        this.customRepository.save(custom);
    }
}
