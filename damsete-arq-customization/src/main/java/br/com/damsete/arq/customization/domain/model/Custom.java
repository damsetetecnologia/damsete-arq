package br.com.damsete.arq.customization.domain.model;

import br.com.damsete.arq.data.infrastructure.listener.AuditListener;
import br.com.damsete.arq.data.domain.base.PersistableEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by andre on 28/04/2016.
 */
@EntityListeners(AuditListener.class)
@Entity
@GenericGenerator(name = "id_generator_custom", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "seq_parametro"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
@Table(name = "parametro")
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Custom extends PersistableEntity<Long> {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "id_generator_custom")
    private Long id = null;

    @Column(name = "codigo")
    private String code = null;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "tipo")
    private CustomType type = null;

    @Column(name = "valor")
    private String value = null;

    @Column(name = "nome")
    private String name = null;
}
