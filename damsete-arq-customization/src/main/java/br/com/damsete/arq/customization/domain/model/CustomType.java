
package br.com.damsete.arq.customization.domain.model;

import java.util.Arrays;

/**
 * Created by andre on 28/04/2016.
 */
public enum CustomType {

   NUMBER, STRING, DATA, BOOL, VALUE, LIST_STRING;

   public static CustomType getTypeCustom(Integer type) {
      return Arrays.stream(values()).filter(i -> type.equals(i.ordinal())).findAny().orElse(null);
   }
}
