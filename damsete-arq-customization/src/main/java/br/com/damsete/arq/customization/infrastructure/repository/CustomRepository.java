package br.com.damsete.arq.customization.infrastructure.repository;

import br.com.damsete.arq.customization.domain.model.Custom;
import br.com.damsete.arq.data.infrastructure.repository.JpaCrudRepository;

import java.util.Optional;

/**
 * Created by andre on 17/10/2016.
 */
public interface CustomRepository extends JpaCrudRepository<Custom, Long> {

    Optional<Custom> findByCode(String code);
}
