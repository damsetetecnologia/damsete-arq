package br.com.damsete.arq.data.application;

import java.io.Serializable;
import java.util.List;

import static com.google.common.collect.Lists.newArrayListWithCapacity;

public abstract class AbstractAssembler<DTO extends Serializable, ENTITY> implements Assembler<DTO, ENTITY> {

    @Override
    public List<ENTITY> fromListDto(List<DTO> dtos) {
        List<ENTITY> entities = newArrayListWithCapacity(dtos.size());
        for (DTO dto : dtos) {
            entities.add(fromDto(dto));
        }
        return entities;
    }

    @Override
    public List<DTO> toListDto(List<ENTITY> entities) {
        List<DTO> dtos = newArrayListWithCapacity(entities.size());
        for (ENTITY entity : entities) {
            dtos.add(toDto(entity));
        }
        return dtos;
    }
}
