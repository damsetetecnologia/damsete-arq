package br.com.damsete.arq.data.application;

import java.io.Serializable;
import java.util.List;

public interface Assembler<DTO extends Serializable, ENTITY> {

    List<ENTITY> fromListDto(List<DTO> dtos);

    ENTITY fromDto(DTO dto);

    List<DTO> toListDto(List<ENTITY> entities);

    DTO toDto(ENTITY entity);
}
