package br.com.damsete.arq.data.application;

import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.data.domain.model.filters.Filters;
import br.com.damsete.arq.data.domain.model.globals.Globals;
import br.com.damsete.arq.data.domain.model.globals.GlobalsValue;
import br.com.damsete.arq.data.domain.model.orders.Orders;
import br.com.damsete.arq.exception.ArqException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface CrudService<T extends Persistable<ID>, ID extends Serializable> {

    Optional<T> findById(ID id);

    Page<T> findAll(Pageable pageable);

    Page<T> findAll(List<Filter> filters, Pageable pageable);

    <G> List<G> findAllProjectedBy(Class<G> clazz);

    <G> List<G> findAllProjectedBy(List<Filter> filters, Class<G> clazz);

    Iterable<Filters> findFilters(String entityName);

    Iterable<Globals> findGlobals(String entityName);

    Iterable<Orders> findOrders(String entityName);

    void delete(T obj) throws ArqException;

    void deleteAll(List<T> objs) throws ArqException;

    void save(T obj) throws ArqException;

    void update(T obj) throws ArqException;

    void global(GlobalsValue globalsValue) throws ArqException;
}
