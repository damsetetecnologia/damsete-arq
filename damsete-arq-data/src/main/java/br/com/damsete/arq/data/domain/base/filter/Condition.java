package br.com.damsete.arq.data.domain.base.filter;

import lombok.Getter;

/**
 * Created by andre on 27/10/2017.
 */
public enum Condition {

    AND("E", "AND"),
    OR("OU", "OR");

    @Getter
    private String name = null;
    @Getter
    private String value = null;

    private Condition(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
