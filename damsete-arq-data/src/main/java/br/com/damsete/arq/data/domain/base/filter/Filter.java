package br.com.damsete.arq.data.domain.base.filter;

import br.com.damsete.arq.types.SerializableObject;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by andre on 26/01/2017.
 */
@Getter
@Setter
public class Filter extends SerializableObject {

    private Condition condition = null;
    private String fieldName = null;
    private Operator operator = null;
    private Object value = null;

    public Filter(String fieldName, Operator operator, Object value) {
        this.fieldName = fieldName;
        this.value = value;
        this.operator = operator;
        this.condition = null;
    }

    public Filter(String fieldName, Operator operator, Object value, Condition condition) {
        this.fieldName = fieldName;
        this.value = value;
        this.operator = operator;
        this.condition = condition;
    }

    public boolean isWhere() {
        return this.condition == null;
    }

    public boolean isAnd() {
        return this.condition == Condition.AND;
    }

    public boolean isOr() {
        return this.condition == Condition.OR;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
