package br.com.damsete.arq.data.domain.base.filter;

import lombok.Getter;

/**
 * Created by andre on 28/01/2017.
 */
public enum Operator {

    EQ("IGUAL", "EQ"),
    NEQ("DIFERENTE", "NEQ"),
    LIKE("CONTÉM", "LIKE"),
    GT("MAIOR", "GT"),
    LT("MENOR", "LT"),
    GTE("MAIOR IGUAL", "GTE"),
    LTE("MENOR IGUAL", "LTE");

    @Getter
    private String name = null;
    @Getter
    private String value = null;

    private Operator(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
