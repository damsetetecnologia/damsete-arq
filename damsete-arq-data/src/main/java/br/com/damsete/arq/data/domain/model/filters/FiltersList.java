package br.com.damsete.arq.data.domain.model.filters;

import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.types.SerializableObject;
import br.com.damsete.arq.utils.Strings;
import lombok.*;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by andre on 28/01/2018.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class FiltersList extends SerializableObject {

    private List<FiltersValue> filtersValueList = newArrayList();

    public void add(FiltersValue filtersValue) {
        if (filtersValue == null) {
            return;
        }

        this.filtersValueList.add(filtersValue);
    }

    public void remove(FiltersValue filtersValue) {
        if (filtersValue == null) {
            return;
        }

        this.filtersValueList.remove(filtersValue);
    }

    public List<Filter> getFilters() {
        List<Filter> filters = newArrayList();
        this.filtersValueList.forEach(i -> filters.add(i.getFilter()));
        return filters;
    }

    @Override
    public String toString() {
        return Strings.join(this.filtersValueList, " ");
    }
}
