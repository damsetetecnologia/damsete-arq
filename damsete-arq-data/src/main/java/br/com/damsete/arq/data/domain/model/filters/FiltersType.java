package br.com.damsete.arq.data.domain.model.filters;

/**
 * Created by andre on 28/01/2018.
 */
public enum FiltersType {

    NUMERIC, TEXT, DATE, BOOLEAN, MONETARY, COMBO, AUTOCOMPLETE;
}
