package br.com.damsete.arq.data.domain.model.filters;

import br.com.damsete.arq.data.domain.base.filter.Condition;
import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.data.domain.base.filter.Operator;
import br.com.damsete.arq.types.SerializableObject;
import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.UUID;

/**
 * Created by andre on 28/01/2018.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class FiltersValue extends SerializableObject {

    private static final long serialVersionUID = 1L;

    private UUID id = UUID.randomUUID();
    private Condition condition = null;
    private Filters filters = null;
    private Operator operator = null;
    private Object value = null;

    public Filter getFilter() {
        return new Filter(this.filters.getColumnName(), this.operator, this.value, this.condition);
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", (this.condition != null ? this.condition.getName() : ""),
                this.filters.getColumnLabel(), this.operator.getName(), this.value);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass().equals(this.getClass())) {
            FiltersValue filtersValue = (FiltersValue) obj;
            return new EqualsBuilder().append(getId(), filtersValue.getId()).isEquals();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getClass()).append(getId()).toHashCode();
    }
}
