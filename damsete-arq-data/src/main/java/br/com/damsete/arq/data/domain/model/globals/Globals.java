package br.com.damsete.arq.data.domain.model.globals;

import br.com.damsete.arq.data.domain.base.PersistableEntity;
import br.com.damsete.arq.data.infrastructure.listener.AuditListener;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by andre on 19/01/2018.
 */
@EntityListeners(AuditListener.class)
@Entity
@GenericGenerator(name = "id_generator_globals", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "seq_globais"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
@Table(name = "globais")
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Globals extends PersistableEntity<Long> {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "id_generator_globals")
    private Long id = null;

    @Column(name = "entidade")
    private String entityName = null;

    @Column(name = "tabela")
    private String tableName = null;

    @Column(name = "nome_coluna")
    private String columnName = null;

    @Column(name = "rotulo_coluna")
    private String columnLabel = null;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "tipo_coluna")
    private GlobalsType type = null;

    @Column(name = "nome_combo")
    private String comboName = null;

    @Column(name = "nome_autocomplete")
    private String autocompleteName = null;

    public boolean isNumeric() {
        return getType().equals(GlobalsType.NUMERIC);
    }

    public boolean isText() {
        return getType().equals(GlobalsType.TEXT);
    }

    public boolean isDate() {
        return getType().equals(GlobalsType.DATE);
    }

    public boolean isBool() {
        return getType().equals(GlobalsType.BOOLEAN);
    }

    public boolean isMonetary() {
        return getType().equals(GlobalsType.MONETARY);
    }

    public boolean isCombo() {
        return getType().equals(GlobalsType.COMBO);
    }

    public boolean isAutoComplete() {
        return getType().equals(GlobalsType.AUTOCOMPLETE);
    }
}
