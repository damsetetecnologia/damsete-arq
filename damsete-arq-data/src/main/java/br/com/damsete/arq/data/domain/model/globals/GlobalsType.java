
package br.com.damsete.arq.data.domain.model.globals;

/**
 * Created by andre on 19/01/2018.
 */
public enum GlobalsType {

    NUMERIC, TEXT, DATE, BOOLEAN, MONETARY, COMBO, AUTOCOMPLETE;
}
