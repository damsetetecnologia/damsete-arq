package br.com.damsete.arq.data.domain.model.globals;

import br.com.damsete.arq.data.infrastructure.projection.IdProjection;
import br.com.damsete.arq.types.SerializableObject;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * Created by andre on 22/01/2018.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class GlobalsValue extends SerializableObject {

    private static final long serialVersionUID = 1L;

    private List<IdProjection> data = null;
    private Globals globals = null;
    private Object value = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
