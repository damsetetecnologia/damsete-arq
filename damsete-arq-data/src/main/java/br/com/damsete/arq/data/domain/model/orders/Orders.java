package br.com.damsete.arq.data.domain.model.orders;

import br.com.damsete.arq.data.domain.base.PersistableEntity;
import br.com.damsete.arq.data.infrastructure.listener.AuditListener;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by andre on 22/01/2018.
 */
@EntityListeners(AuditListener.class)
@Entity
@GenericGenerator(name = "id_generator_orders", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "seq_ordenadores"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
@Table(name = "ordenadores")
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Orders extends PersistableEntity<Long> {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "id_generator_orders")
    private Long id = null;

    @Column(name = "entidade")
    private String entityName = null;

    @Column(name = "nome_coluna")
    private String columnName = null;

    @Column(name = "rotulo_coluna")
    private String columnLabel = null;
}
