package br.com.damsete.arq.data.domain.model.orders;

import br.com.damsete.arq.types.SerializableObject;
import br.com.damsete.arq.utils.Strings;
import lombok.*;
import org.springframework.data.domain.Sort;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by andre on 22/01/2018.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class OrdersList extends SerializableObject {

    private List<OrdersValue> ordersValueList = newArrayList();

    public void add(OrdersValue ordersValue) {
        if (ordersValue == null) {
            return;
        }

        this.ordersValueList.add(ordersValue);
    }

    public List<Sort.Order> getOrders() {
        List<Sort.Order> orders = newArrayList();
        this.ordersValueList.forEach(i -> orders.add(i.getOrder()));
        return orders;
    }

    @Override
    public String toString() {
        return Strings.join(this.ordersValueList, ", ");
    }
}
