package br.com.damsete.arq.data.domain.model.orders;

import br.com.damsete.arq.types.SerializableObject;
import lombok.*;
import org.springframework.data.domain.Sort;

/**
 * Created by andre on 22/01/2018.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class OrdersValue extends SerializableObject {

    private Orders orders = null;
    private String orderType = null;

    @Override
    public String toString() {
        return String.format("%s = %s", this.orders.getColumnLabel(), this.orderType);
    }

    public Sort.Order getOrder() {
        return new Sort.Order(Sort.Direction.fromString(this.orderType), this.orders.getColumnName());
    }
}
