package br.com.damsete.arq.data.infrastructure.audit;

import br.com.damsete.arq.log.core.LogPersistable;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by andre on 05/03/2017.
 */
public class DbBatchUpdate implements LogPersistable {

    private String sql = null;
    private Object[] batchArgs = null;
    private Map<String, ?> mapArgs = null;

    public DbBatchUpdate(String sql) {
        this.sql = sql;
    }

    public DbBatchUpdate(String sql, Object[] batchArgs) {
        this.sql = sql;
        this.batchArgs = Arrays.copyOf(batchArgs, batchArgs.length);
    }

    public DbBatchUpdate(String sql, Map<String, ?> mapArgs) {
        this.sql = sql;
        this.mapArgs = mapArgs;
    }

    @Override
    public String getLog() {
        String params = "";

        if (batchArgs != null) {
            params = Arrays.toString(batchArgs);
        }

        if (mapArgs != null) {
            params = mapArgs.toString();
        }

        return String.format("%s %s %s", "BATCH UPDATE", sql, params);
    }
}
