package br.com.damsete.arq.data.infrastructure.audit;

import br.com.damsete.arq.log.core.LogPersistable;
import lombok.Data;

/**
 * Created by andre on 22/10/2016.
 */
@Data
public class DbJpa implements LogPersistable {

    private String operation = null;
    private Object data = null;

    @Override
    public String getLog() {
        return String.format("%s %s", operation != null ? operation : "",
                data != null ? data.toString() : "");
    }
}
