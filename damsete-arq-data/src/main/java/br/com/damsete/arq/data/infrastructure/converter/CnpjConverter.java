package br.com.damsete.arq.data.infrastructure.converter;

import br.com.damsete.arq.types.Cnpj;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by andre on 09/04/2016.
 */
@Converter(autoApply = true)
public class CnpjConverter implements AttributeConverter<Cnpj, Long> {

    @Override
    public Long convertToDatabaseColumn(Cnpj attribute) {
        return attribute == null ? null : attribute.getRawValue();
    }

    @Override
    public Cnpj convertToEntityAttribute(Long dbData) {
        return dbData == null ? null : new Cnpj(dbData);
    }
}