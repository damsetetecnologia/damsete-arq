package br.com.damsete.arq.data.infrastructure.converter;

import br.com.damsete.arq.types.Cpf;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by andre on 09/04/2016.
 */
@Converter(autoApply = true)
public class CpfConverter implements AttributeConverter<Cpf, Long> {

    @Override
    public Long convertToDatabaseColumn(Cpf attribute) {
        return attribute == null ? null : attribute.getRawValue();
    }

    @Override
    public Cpf convertToEntityAttribute(Long dbData) {
        return dbData == null ? null : new Cpf(dbData);
    }
}