package br.com.damsete.arq.data.infrastructure.converter;

import br.com.damsete.arq.types.Email;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by andre on 05/05/2016.
 */
@Converter(autoApply = true)
public class EmailConverter implements AttributeConverter<Email, String> {

    @Override
    public String convertToDatabaseColumn(Email attribute) {
        return attribute == null ? null : attribute.getRawValue();
    }

    @Override
    public Email convertToEntityAttribute(String dbData) {
        return dbData == null ? null : new Email(dbData);
    }
}