package br.com.damsete.arq.data.infrastructure.converter;

import br.com.damsete.arq.types.NitPis;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by andre on 22/10/2016.
 */
@Converter(autoApply = true)
public class NitPisConverter implements AttributeConverter<NitPis, Long> {

    public Long convertToDatabaseColumn(NitPis attribute) {
        return attribute == null ? null : attribute.getRawValue();
    }

    @Override
    public NitPis convertToEntityAttribute(Long dbData) {
        return dbData == null ? null : new NitPis(dbData);
    }
}