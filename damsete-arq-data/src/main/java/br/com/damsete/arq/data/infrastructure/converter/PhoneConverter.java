package br.com.damsete.arq.data.infrastructure.converter;

import br.com.damsete.arq.types.Phone;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by andre on 05/05/2016.
 */
@Converter(autoApply = true)
public class PhoneConverter implements AttributeConverter<Phone, Long> {

    @Override
    public Long convertToDatabaseColumn(Phone attribute) {
        return attribute == null ? null : attribute.getRawValue();
    }

    @Override
    public Phone convertToEntityAttribute(Long dbData) {
        return dbData == null ? null : new Phone(dbData);
    }
}