package br.com.damsete.arq.data.infrastructure.converter;

import br.com.damsete.arq.types.Rg;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by andre on 22/10/2016.
 */
@Converter(autoApply = true)
public class RgConverter implements AttributeConverter<Rg, Long> {

    public Long convertToDatabaseColumn(Rg attribute) {
        return attribute == null ? null : attribute.getRawValue();
    }

    @Override
    public Rg convertToEntityAttribute(Long dbData) {
        return dbData == null ? null : new Rg(dbData);
    }
}