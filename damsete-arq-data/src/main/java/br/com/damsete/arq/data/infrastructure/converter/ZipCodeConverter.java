package br.com.damsete.arq.data.infrastructure.converter;

import br.com.damsete.arq.types.ZipCode;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 * Created by andre on 05/05/2016.
 */
@Converter(autoApply = true)
public class ZipCodeConverter implements AttributeConverter<ZipCode, Long> {

    @Override
    public Long convertToDatabaseColumn(ZipCode attribute) {
        return attribute == null ? null : attribute.getRawValue();
    }

    @Override
    public ZipCode convertToEntityAttribute(Long dbData) {
        return dbData == null ? null : new ZipCode(dbData);
    }
}