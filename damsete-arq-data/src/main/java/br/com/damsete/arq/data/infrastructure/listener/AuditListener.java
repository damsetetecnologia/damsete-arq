package br.com.damsete.arq.data.infrastructure.listener;

import br.com.damsete.arq.data.infrastructure.audit.DbJpa;
import br.com.damsete.arq.log.core.LoggerAPI;
import org.apache.logging.log4j.Level;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

/**
 * Created by andre on 22/10/2016.
 */
public class AuditListener {

    @PostPersist
    public void onPrePersist(Object object) {
        audit(object, "INSERT");
    }

    @PostUpdate
    public void onPreUpdate(Object object) {
        audit(object, "UPDATE");
    }

    @PostRemove
    public void onPreRemove(Object object) {
        audit(object, "DELETE");
    }

    private void audit(Object object, String operation) {
        DbJpa dbJpa = new DbJpa();
        dbJpa.setOperation(operation);
        dbJpa.setData(object);
        LoggerAPI.log(Level.INFO, dbJpa);
    }
}
