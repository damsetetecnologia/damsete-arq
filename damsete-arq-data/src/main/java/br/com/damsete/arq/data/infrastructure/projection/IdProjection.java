package br.com.damsete.arq.data.infrastructure.projection;

import java.io.Serializable;

/**
 * Created by andre on 22/01/2018.
 */
public interface IdProjection extends Serializable {

    Long getId();
}
