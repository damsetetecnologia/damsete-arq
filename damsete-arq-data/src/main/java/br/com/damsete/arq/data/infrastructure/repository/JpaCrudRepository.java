package br.com.damsete.arq.data.infrastructure.repository;

import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;
import java.util.List;

/**
 * Created by andre on 17/10/2016.
 */
public interface JpaCrudRepository<T extends Persistable<ID>, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

    <G> List<G> findAllProjectedBy(Class<G> clazz);

    <G> List<G> findAllProjectedBy(Specification<T> spec, Class<G> clazz);
}
