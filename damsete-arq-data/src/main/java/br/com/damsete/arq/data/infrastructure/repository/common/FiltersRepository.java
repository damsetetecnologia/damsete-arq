package br.com.damsete.arq.data.infrastructure.repository.common;

import br.com.damsete.arq.data.domain.model.filters.Filters;
import br.com.damsete.arq.data.infrastructure.repository.JpaCrudRepository;

/**
 * Created by andre on 28/01/2018.
 */
public interface FiltersRepository extends JpaCrudRepository<Filters, Long> {

    Iterable<Filters> findByEntityNameOrderByColumnLabel(String entityName);
}
