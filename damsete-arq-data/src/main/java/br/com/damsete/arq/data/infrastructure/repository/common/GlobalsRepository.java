package br.com.damsete.arq.data.infrastructure.repository.common;

import br.com.damsete.arq.data.domain.model.globals.Globals;
import br.com.damsete.arq.data.infrastructure.repository.JpaCrudRepository;

/**
 * Created by andre on 19/01/2018.
 */
public interface GlobalsRepository extends JpaCrudRepository<Globals, Long> {

    Iterable<Globals> findByEntityNameOrderByColumnLabel(String entityName);
}
