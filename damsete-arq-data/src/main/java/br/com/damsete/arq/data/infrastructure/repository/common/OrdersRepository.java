package br.com.damsete.arq.data.infrastructure.repository.common;

import br.com.damsete.arq.data.domain.model.orders.Orders;
import br.com.damsete.arq.data.infrastructure.repository.JpaCrudRepository;

/**
 * Created by andre on 22/01/2018.
 */
public interface OrdersRepository extends JpaCrudRepository<Orders, Long> {

    Iterable<Orders> findByEntityNameOrderByColumnLabel(String entityName);
}
