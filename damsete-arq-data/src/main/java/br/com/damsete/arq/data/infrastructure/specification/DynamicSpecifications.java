package br.com.damsete.arq.data.infrastructure.specification;

import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.data.infrastructure.specification.chain.*;
import br.com.damsete.arq.log.core.LoggerAPI;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.*;
import java.util.List;

/**
 * Created by andre on 26/01/2017.
 */
public class DynamicSpecifications {

    public static <T> Specification<T> getSpecificationByFilter(final List<Filter> filters) {
        return getSpecificationByFilter(filters.toArray(new Filter[filters.size()]));
    }

    public static <T> Specification<T> getSpecificationByFilter(final Filter... filters) {
        Specifications<T> specifications = null;

        if (filters != null) {
            for (Filter filter : filters) {
                Specification<T> specification = convertFilter(filter);
                if (specification != null) {
                    if (filter.isWhere()) {
                        specifications = specifications.where(specification);
                    } else if (filter.isAnd()) {
                        specifications = specifications.and(specification);
                    } else if (filter.isOr()) {
                        specifications = specifications.or(specification);
                    }
                }
            }
        }

        return specifications;
    }

    private static <T> Specification<T> convertFilter(final Filter filter) {
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                PredicateChain predicateChain = new PredicateEqChain();
                predicateChain.setNext(new PredicateNeqChain());
                predicateChain.setNext(new PredicateLikeChain());
                predicateChain.setNext(new PredicateLtChain());
                predicateChain.setNext(new PredicateGtChain());
                predicateChain.setNext(new PredicateLteChain());
                predicateChain.setNext(new PredicateGteChain());

                Predicate predicate = null;

                try {
                    String[] names = StringUtils.split(filter.getFieldName(), ".");
                    Path expression = root.get(names[0]);
                    for (int i = 1; i < names.length; i++) {
                        expression = expression.get(names[i]);
                    }

                    ConversionService conversionService = new DefaultConversionService();

                    Class attributeClass = expression.getJavaType();
                    if (!attributeClass.equals(String.class) && filter.getValue() instanceof String
                            && conversionService.canConvert(String.class, attributeClass)) {
                        filter.setValue(conversionService.convert(filter.getValue(), attributeClass));
                    }

                    predicate = predicateChain.predicate(expression, builder, filter);
                } catch (Exception e) {
                    LoggerAPI.debug(e.getMessage());
                }

                return predicate;
            }
        };
    }
}
