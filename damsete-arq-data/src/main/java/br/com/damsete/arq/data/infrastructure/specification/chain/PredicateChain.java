package br.com.damsete.arq.data.infrastructure.specification.chain;

import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.data.domain.base.filter.Operator;
import br.com.damsete.arq.data.infrastructure.specification.exception.SpecificationNotFoundException;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Created by andre on 15/06/2017.
 */
public abstract class PredicateChain {

    protected PredicateChain next = null;
    protected Operator operator = null;

    public PredicateChain(Operator operator) {
        this.next = null;
        this.operator = operator;
    }

    public void setNext(PredicateChain chain) {
        if (this.next == null) {
            this.next = chain;
        } else {
            this.next.setNext(chain);
        }
    }

    public Predicate predicate(Path expression, CriteriaBuilder builder, Filter filter) {
        if (canPredicateOperator(filter.getOperator())) {
            return predicateOperator(expression, builder, filter);
        } else {
            if (this.next == null) {
                throw new SpecificationNotFoundException(new DefaultMessageSourceResolvable("specification-predicate-exception-not-found"));
            }
            return this.next.predicate(expression, builder, filter);
        }
    }

    private boolean canPredicateOperator(Operator operator) {
        return this.operator.equals(operator);
    }

    protected abstract Predicate predicateOperator(Path expression, CriteriaBuilder builder, Filter filter);
}
