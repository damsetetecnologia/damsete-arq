package br.com.damsete.arq.data.infrastructure.specification.chain;

import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.data.domain.base.filter.Operator;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Created by andre on 15/06/2017.
 */
public class PredicateGteChain extends PredicateChain {

    public PredicateGteChain() {
        super(Operator.GTE);
    }

    @Override
    protected Predicate predicateOperator(Path expression, CriteriaBuilder builder, Filter filter) {
        return builder.greaterThanOrEqualTo(expression, (Comparable) filter.getValue());
    }
}
