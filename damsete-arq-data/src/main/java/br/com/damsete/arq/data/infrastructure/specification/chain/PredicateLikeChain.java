package br.com.damsete.arq.data.infrastructure.specification.chain;

import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.data.domain.base.filter.Operator;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Created by andre on 15/06/2017.
 */
public class PredicateLikeChain extends PredicateChain {

    public PredicateLikeChain() {
        super(Operator.LIKE);
    }

    @Override
    protected Predicate predicateOperator(Path expression, CriteriaBuilder builder, Filter filter) {
        return builder.like(builder.upper(expression), "%" + filter.getValue() + "%");
    }
}
