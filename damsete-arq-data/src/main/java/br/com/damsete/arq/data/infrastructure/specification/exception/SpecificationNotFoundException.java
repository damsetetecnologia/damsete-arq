package br.com.damsete.arq.data.infrastructure.specification.exception;

import lombok.Getter;
import org.springframework.context.MessageSourceResolvable;

/**
 * Created by andre on 19/10/2017.
 */
public class SpecificationNotFoundException extends RuntimeException {

    @Getter
    private MessageSourceResolvable messageSourceResolvable = null;

    public SpecificationNotFoundException(MessageSourceResolvable messageSourceResolvable) {
        this.messageSourceResolvable = messageSourceResolvable;
    }
}
