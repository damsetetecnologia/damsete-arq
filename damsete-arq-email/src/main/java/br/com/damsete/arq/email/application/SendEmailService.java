package br.com.damsete.arq.email.application;

import br.com.damsete.arq.email.domain.model.Email;
import br.com.damsete.arq.log.core.LoggerAPI;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.IOException;

/**
 * Created by andre on 21/04/2016.
 */
@Service
public class SendEmailService {

    @Autowired
    private JavaMailSender mailSender = null;
    @Autowired
    private Configuration freemarkerConfig = null;

    @Async
    public void send(Email message) {
        this.mailSender.send(message.toMimeMessage());
        LoggerAPI.log(Level.INFO, message);
    }

    @Async
    public void send(Iterable<Email> messages) {
        messages.forEach(m -> send(m));
    }

    @Async
    public void send(Email message, String template) {
        try {
            Template t = this.freemarkerConfig.getTemplate(template);
            String content = FreeMarkerTemplateUtils.processTemplateIntoString(t, message.getModel());
            message = message.withContent(content);
            this.mailSender.send(message.toMimeMessage());
            LoggerAPI.log(Level.INFO, message);
        } catch (IOException | TemplateException e) {
            LoggerAPI.log(Level.ERROR, message);
        }
    }

    @Async
    public void send(Iterable<Email> messages, String template) {
        messages.forEach(m -> send(m, template));
    }
}