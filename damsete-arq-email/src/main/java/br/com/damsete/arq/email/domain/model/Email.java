package br.com.damsete.arq.email.domain.model;

import br.com.damsete.arq.exception.WrongConfigurationException;
import br.com.damsete.arq.log.core.LogPersistable;
import com.google.common.collect.Iterables;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;
import java.util.stream.Collectors;

import static br.com.damsete.arq.utils.Networks.newInternetAddress;
import static com.google.common.collect.Iterables.toArray;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Created by andre on 21/04/2016.
 */
public class Email implements LogPersistable {

    private boolean html = true;
    private String fromAddress = null;
    private String fromName = null;
    private String subject = null;
    private String content = null;
    private String confirmationAddress = null;

    private List<EmailRecipient> recipients = new ArrayList<>();
    private List<EmailReplyTo> replyToList = new ArrayList<>();
    private List<EmailAttachment> attachments = new ArrayList<>();
    private List<EmailInline> inlines = new ArrayList<>();
    private Map<String, Object> model = new HashMap<>();

    private Email() {
        super();
    }

    /**
     * Monta um objeto Email para uma menssagem apenas TEXTO.
     *
     * @return Email configurado para apenas TEXTO.
     */
    public static Email asPlainText() {
        Email mm = new Email();
        mm.html = false;
        return mm;
    }

    /**
     * Monta um objeto Email para uma menssagem HTML.
     *
     * @return Email configurado para HTML.
     */
    public static Email asHtml() {
        Email mm = new Email();
        mm.html = true;
        return mm;
    }

    /**
     * Configura o endereço de confirmação da mensagem.
     *
     * @param m MimeMessage com dados da mensagem.
     */
    private void configConfirmation(MimeMessage m) {
        try {
            if (!isEmpty(confirmationAddress)) {
                m.setHeader("Return-Receipt-To", confirmationAddress);
            }
        } catch (MessagingException e) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(""));
        }
    }

    /**
     * Adicionar recipiente (endereço de e-mail) a mensagem.
     *
     * @param m MimeMessage com dados da mensagem.
     * @param r MailRecipient com informações de um recipiente da mensagem.
     */
    private void addRecipient(MimeMessage m, EmailRecipient r) {
        try {
            m.addRecipient(r.getType(), newInternetAddress(r.getToAddress(), r.getToName()));
        } catch (MessagingException e) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(""));
        }
    }

    /**
     * Adiciona anexo a mensagem.
     *
     * @param helper MimeMessageHelper que auxilia a montar a mensagem.
     * @param a      MailAttachment com arquivo anexado a mensagem.
     * @see MimeMessageHelper
     */
    private void addAttachment(MimeMessageHelper helper, EmailAttachment a) {
        try {
            helper.addAttachment(a.getFileName(), a.getSource());
        } catch (MessagingException e) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(""));
        }
    }

    /**
     * Adiciona mensagem in-line no e-mail.
     *
     * @param helper MimeMessageHelper que auxilia a montar a mensagem.
     * @param a      MailInline que será adicionado.
     * @see MimeMessageHelper
     * @see EmailInline
     */
    private void addInline(MimeMessageHelper helper, EmailInline a) {
        try {
            helper.addInline(a.getContentId(), a.getResource());
        } catch (MessagingException e) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(""));
        }
    }

    /**
     * Monta um objeto MailMessage baseado no template.
     *
     * @return Retorna um objeto Email baseado no template.
     */
    public Email useAsTemplate() {
        Email mm = new Email();

        mm.fromAddress = fromAddress;
        mm.fromName = fromName;
        mm.recipients.addAll(this.recipients);
        mm.replyToList.addAll(this.replyToList);
        mm.attachments.addAll(attachments);
        mm.inlines.addAll(inlines);
        mm.subject = subject;
        mm.content = content;
        mm.confirmationAddress = confirmationAddress;

        return mm;
    }

    /**
     * Define remetente.
     *
     * @param fromName    Nome do remetente
     * @param fromAddress E-Mail do remetente
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email from(String fromName, String fromAddress) {
        this.fromName = fromName;
        this.fromAddress = fromAddress;
        return this;
    }

    /**
     * Define o destinatário.
     *
     * @param toName    Nome do destinatário
     * @param toAddress E-Mail do destinatário
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email addTo(String toName, String toAddress) {
        recipients.add(new EmailRecipient(toName, toAddress, Message.RecipientType.TO));
        return this;
    }

    /**
     * Método responsável por montar um objeto MimeMessagePreparator com os dados da mensagem.
     *
     * @return MimeMessagePreparator com todos os dados da mensagem.
     * @see MimeMessagePreparator
     */
    public MimeMessagePreparator toMimeMessage() {
        MimeMessagePreparator mimeMessage = (m) -> {
            MimeMessageHelper helper = new MimeMessageHelper(m, true);

            m.setFrom(new InternetAddress(fromAddress, fromName));
            recipients.forEach(r -> addRecipient(m, r));

            List<InternetAddress> replyTos = replyToList.stream().map(r -> newInternetAddress(r.getReplyToAddress(), r.getReplyToName())).collect(Collectors.toList());
            m.setReplyTo(toArray(replyTos, InternetAddress.class));

            m.setSubject(subject);
            m.setSentDate(new Date());
            helper.setText(content, html);

            configConfirmation(m);


            attachments.forEach(a -> addAttachment(helper, a));
            inlines.forEach(i -> addInline(helper, i));
        };

        return mimeMessage;
    }

    /**
     * Define o destinatário de cópia.
     *
     * @param toName    Nome do destinatário de cópia.
     * @param toAddress E-Mail do destinatário de cópia.
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email addCc(String toName, String toAddress) {
        recipients.add(new EmailRecipient(toName, toAddress, Message.RecipientType.CC));
        return this;
    }

    /**
     * Define o destinatário de cópia oculta.
     *
     * @param toName    Nome do destinatário de cópia oculta.
     * @param toAddress E-Mail do destinatário de cópia oculta.
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email addBcc(String toName, String toAddress) {
        recipients.add(new EmailRecipient(toName, toAddress, Message.RecipientType.BCC));
        return this;
    }

    /**
     * Define o assunto da menssagem.
     *
     * @param subject Assunto da menssagem.
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email withSubject(String subject) {
        this.subject = subject;
        return this;
    }

    /**
     * Define remetente da resposta.
     *
     * @param replyToName    Nome do remetente da resposta
     * @param replyToAddress E-Mail do remetente da resposta.
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email addReplyTo(String replyToName, String replyToAddress) {
        replyToList.add(new EmailReplyTo(replyToName, replyToAddress));
        return this;
    }

    /**
     * Define o endereço de confirmação.
     *
     * @param confirmationAddress Endereço de confirmação.
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email withConfirmationTo(String confirmationAddress) {
        this.confirmationAddress = confirmationAddress;
        return this;
    }

    /**
     * Define o conteúdo da mensagem.
     *
     * @param content Conteúdo da mensagem.
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email withContent(String content) {
        this.content = content;
        return this;
    }

    /**
     * Adiciona anexo a menssagem.
     *
     * @param fileName Nome do arquivo
     * @param source   InputStreamSource do arquivo
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email addAttachment(String fileName, InputStreamSource source) {
        attachments.add(new EmailAttachment(fileName, source));
        return this;
    }

    /**
     * Adiciona um conteúdo in-line.
     *
     * @param contentId Identificador de conteúdo
     * @param resource  Recurso
     * @return Email configurado com os dados informados nos parâmetros.
     */
    public Email addInline(String contentId, Resource resource) {
        inlines.add(new EmailInline(contentId, resource));
        return this;
    }

    /**
     * Adiciona um parâmetro ao model para emails com template.
     *
     * @param key
     * @param value
     * @return
     */
    public Email addModelParam(String key, Object value) {
        this.model.put(key, value);
        return this;
    }

    /**
     * Retorna o model para emails com template.
     *
     * @return
     */
    public Map<String, Object> getModel() {
        return model;
    }

    @Override
    public String getLog() {
        if (isEmpty(this.fromAddress)) {
            this.fromAddress = "";
        }
        if (isEmpty(this.fromName)) {
            this.fromName = "";
        }
        if (isEmpty(this.subject)) {
            this.subject = "";
        }
        if (isEmpty(this.content)) {
            this.content = "";
        }
        if (isEmpty(this.confirmationAddress)) {
            this.confirmationAddress = "";
        }
        if (Iterables.isEmpty(this.recipients)) {
            this.recipients = newArrayList();
        }
        if (Iterables.isEmpty(this.replyToList)) {
            this.replyToList = newArrayList();
        }

        return String.format("%s %s %s %s %s %s %s %s", html, fromAddress,
                fromName, subject, content, confirmationAddress,
                Iterables.toString(this.recipients),
                Iterables.toString(this.replyToList));
    }
}
