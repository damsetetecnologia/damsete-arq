package br.com.damsete.arq.email.service;

import br.com.damsete.arq.email.application.SendEmailService;
import br.com.damsete.arq.email.domain.model.Email;
import br.com.damsete.arq.log.core.LoggerAPI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

/**
 * Created by andre on 29/05/2017.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({LoggerAPI.class})
public class SendEmailServiceTest {

    @Mock
    private JavaMailSender mailSender = null;

    private SendEmailService sendEmailService = new SendEmailService();

    @Before
    public void setup() {
        setField(sendEmailService, "mailSender", mailSender);
    }

    @Test
    public void sendOneEmail() {
        MimeMessagePreparator mimeMessage = mock(MimeMessagePreparator.class);
        Email message = mock(Email.class);

        doReturn(mimeMessage).when(message).toMimeMessage();
        PowerMockito.mockStatic(LoggerAPI.class);

        this.sendEmailService.send(message);

        verify(message, times(1)).toMimeMessage();
        verify(this.mailSender, times(1)).send(mimeMessage);
        verifyNoMoreInteractions(message);
        verifyNoMoreInteractions(this.mailSender);
    }

    @Test
    public void sendManyEmails() {
        MimeMessagePreparator mimeMessage = mock(MimeMessagePreparator.class);
        Email message = mock(Email.class);

        doReturn(mimeMessage).when(message).toMimeMessage();
        PowerMockito.mockStatic(LoggerAPI.class);

        List<Email> messages = newArrayList(message, message, message);

        this.sendEmailService.send(messages);

        verify(message, times(messages.size())).toMimeMessage();
        verify(this.mailSender, times(messages.size())).send(mimeMessage);
        verifyNoMoreInteractions(message);
        verifyNoMoreInteractions(this.mailSender);
    }
}
