package br.com.damsete.arq.files.application;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.files.application.dto.FileDto;
import br.com.damsete.arq.files.domain.model.Content;
import br.com.damsete.arq.files.domain.model.File;
import br.com.damsete.arq.files.domain.model.FileItem;
import br.com.damsete.arq.files.infrastructure.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static org.hibernate.validator.internal.util.CollectionHelper.newArrayList;

/**
 * Created by andre on 08/05/2016.
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class FileService {

    private static final int BUFFER_SIZE = 50 * 1024; // 50 Kb

    @Autowired
    private FileRepository fileRepository = null;

    @MethodLoggable
    public void deleteFile(Long idFile) {
        if (idFile != null) {
            this.fileRepository.deleteById(idFile);
        }
    }

    @MethodLoggable
    public FileDto getFile(Long idFile) {
        Optional<File> file = this.fileRepository.findOne(Example.of(File.builder().id(idFile).build()));
        if (!file.isPresent()) {
            return null;
        }

        FileDto fileDto = new FileDto();
        fileDto.setId(file.get().getId());
        fileDto.setName(file.get().getName());
        fileDto.setSize(file.get().getSize());
        fileDto.setContentType(file.get().getContentType());

        Content content = new Content();
        file.get().getItens().stream().forEach(fileItem -> content.add(file.get().getCompressed() ? uncompress(fileItem.getContent()) : fileItem.getContent()));
        content.close();

        fileDto = fileDto.addContent(content.getContent());

        return fileDto;
    }

    @MethodLoggable
    public FileDto saveFile(FileDto archive) {
        if (archive.getContentType() != null) {
            final String fileName = archive.getName().substring(archive.getName().lastIndexOf("\\") + 1);
            final String contentType = getContentType(fileName, archive.getContentType());
            final boolean compressed = true;

            File file = new File();
            file.setId(archive.getId());
            file.setName(archive.getName());
            file.setCompressed(compressed);
            file.setContentType(contentType);
            file.setSize(archive.getSize());
            file.setRegistrationDate(LocalDateTime.now());
            file.setItens(newArrayList());

            byte[] buffer;
            int fileLength = archive.getContent().length;
            int fragmentNumber = 1;
            int totalFragments = 0;
            byte[] fileContent = archive.getContent();

            while (totalFragments < fileLength) {
                int fragmentSize = BUFFER_SIZE;

                if (fileContent.length - totalFragments >= BUFFER_SIZE) {
                    buffer = new byte[BUFFER_SIZE];
                } else {
                    fragmentSize = fileContent.length - totalFragments;
                    buffer = new byte[fragmentSize];
                }

                System.arraycopy(archive.getContent(), totalFragments, buffer, 0, fragmentSize);

                if (compressed) {
                    buffer = compress(buffer);
                }

                FileItem fileItem = new FileItem();
                fileItem.setSize(Long.valueOf(buffer.length));
                fileItem.setItem(fragmentNumber);
                fileItem.setFile(file);

                fileItem = fileItem.addContent(buffer);

                file.getItens().add(fileItem);
                totalFragments += fragmentSize;
                fragmentNumber++;
            }

            this.fileRepository.save(file);
            archive.setId(file.getId());
        }
        return archive;
    }

    private byte[] compress(byte[] content) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gzipOutputStream.write(content);
            gzipOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            return content;
        }
    }

    private byte[] uncompress(byte[] content) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(content);
            GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int count = 0;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((count = gzipInputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, count);
            }
            gzipInputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            return content;
        }
    }

    private String getContentType(String fileName, String contentType) {
        if (fileName.endsWith(".pdf") && !"application/pdf".equals(contentType)) {
            return "application/pdf";
        } else {
            return contentType;
        }
    }
}
