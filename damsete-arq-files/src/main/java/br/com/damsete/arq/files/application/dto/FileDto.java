package br.com.damsete.arq.files.application.dto;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by andre on 08/05/2016.
 */
public class FileDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Long id = null;
    @Getter
    @Setter
    private Long size = null;
    @Getter
    @Setter
    private String name = null;
    @Getter
    @Setter
    private String contentType = null;

    private byte[] content = null;

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass().equals(this.getClass())) {
            FileDto otherObj = (FileDto) obj;
            return new EqualsBuilder().append(getId(), otherObj.getId()).isEquals();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getClass()).append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public FileDto addContent(byte[] content) {
        this.content = Arrays.copyOf(content, content.length);
        return this;
    }

    public byte[] getContent() {
        return Arrays.copyOf(content, content.length);
    }
}
