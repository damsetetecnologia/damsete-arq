package br.com.damsete.arq.files.domain.model;

import br.com.damsete.arq.log.core.LoggerAPI;
import br.com.damsete.arq.vo.ValueObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 08/05/2016.
 */
public class Content implements ValueObject<Content> {

    private static final long serialVersionUID = 1L;

    private transient ByteArrayOutputStream output = null;

    public Content() {
        this.output = new ByteArrayOutputStream();
    }

    public void add(byte[] byteArray) {
        try {
            this.output.write(byteArray);
        } catch (Exception e) {
            LoggerAPI.error("fileContent.add.error", e);
        }
    }

    public void close() {
        try {
            this.output.close();
        } catch (Exception e) {
            LoggerAPI.error("fileContent.close.error", e);
        }
    }

    public byte[] getContent() {
        if (this.output != null) {
            return this.output.toByteArray();
        } else {
            return null;
        }
    }

    @Override
    public boolean sameValueAs(Content other) {
        return false;
    }
}
