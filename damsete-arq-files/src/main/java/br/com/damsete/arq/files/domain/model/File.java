package br.com.damsete.arq.files.domain.model;

import br.com.damsete.arq.data.infrastructure.listener.AuditListener;
import br.com.damsete.arq.data.domain.base.PersistableEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by andre on 08/05/2016.
 */
@EntityListeners(AuditListener.class)
@Entity
@GenericGenerator(name = "id_generator_file", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "seq_arquivo"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
@Table(name = "arquivo")
@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class File extends PersistableEntity<Long> {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "id_generator_file")
    private Long id = null;

    @Column(name = "nome")
    private String name = null;

    @Column(name = "tamanho")
    private Long size = null;

    @Column(name = "comprimido")
    private Boolean compressed = null;

    @Column(name = "tipo_conteudo")
    private String contentType = null;

    @Column(name = "data_cadastro")
    private LocalDateTime registrationDate = null;

    @OneToMany(mappedBy = "file", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<FileItem> itens = null;
}
