package br.com.damsete.arq.files.domain.model;

import br.com.damsete.arq.data.infrastructure.listener.AuditListener;
import br.com.damsete.arq.data.domain.base.PersistableEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Created by andre on 08/05/2016.
 */
@EntityListeners(AuditListener.class)
@Entity
@GenericGenerator(name = "id_generator_fileitem", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {@org.hibernate.annotations.Parameter(name = "sequence_name", value = "seq_arquivo_item"), @org.hibernate.annotations.Parameter(name = "initial_value", value = "1"), @org.hibernate.annotations.Parameter(name = "increment_size", value = "1")})
@Table(name = "arquivo_item")
public class FileItem extends PersistableEntity<Long> {

    @Getter
    @Setter
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "id_generator_fileitem")
    private Long id = null;

    @Getter
    @Setter
    @Fetch(FetchMode.JOIN)
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_arquivo")
    private File file = null;

    @Getter
    @Setter
    @Column(name = "tamanho")
    private Long size = null;

    @Getter
    @Setter
    @Column(name = "item")
    private Integer item = null;

    @Column(name = "conteudo")
    private byte[] content = null;

    public FileItem addContent(byte[] content) {
        this.content = Arrays.copyOf(content, content.length);
        return this;
    }

    public byte[] getContent() {
        return Arrays.copyOf(content, content.length);
    }
}
