package br.com.damsete.arq.files.infrastructure.repository;

import br.com.damsete.arq.data.infrastructure.repository.JpaCrudRepository;
import br.com.damsete.arq.files.domain.model.File;

/**
 * Created by andre on 17/10/2016.
 */
public interface FileRepository extends JpaCrudRepository<File, Long> {
}
