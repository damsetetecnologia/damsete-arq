package br.com.damsete.arq.jsf.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

public class CustomBooleanConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
        if (value == null || value.trim().equals("")) {
            value = "N\u00E3o";
        }
        if (value.equals("Sim")) {
            return true;
        }
        return false;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
        Boolean valor = (Boolean) value;
        if (valor.equals(Boolean.TRUE)) {
            return "Sim";
        }
        return "N\u00E3o";
    }
}
