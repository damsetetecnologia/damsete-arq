package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.jsf.utils.ConverterUtils;
import br.com.damsete.arq.types.Cnpj;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by andre on 04/05/2016.
 */
public class CustomCnpjConverter implements Converter {

    private static final String MESSAGE_CONVERTER_ERROR = "arq.CustomCnpjConverter";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Cnpj cnpj = null;
        if (StringUtils.isNotEmpty(value)) {
            try {
                cnpj = new Cnpj(Long.parseLong(value.replace(".", "").replace("-", "").replace("/", "")));
                if (!cnpj.isValid()) {
                    ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
                }
            } catch (Exception e) {
                ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
            }
        }
        return cnpj;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String cnpj = null;
        if (value != null) {
            Cnpj cnpjValue = (Cnpj) value;
            cnpj = cnpjValue.format();
        }
        return cnpj;
    }
}
