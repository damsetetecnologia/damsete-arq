package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.jsf.utils.ConverterUtils;
import br.com.damsete.arq.types.Cpf;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by andre on 04/05/2016.
 */
public class CustomCpfConverter implements Converter {

    private static final String MESSAGE_CONVERTER_ERROR = "arq.CustomCpfConverter";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Cpf cpf = null;
        if (StringUtils.isNotEmpty(value)) {
            try {
                cpf = new Cpf(Long.parseLong(value.replace(".", "").replace("-", "").replace("/", "")));
                if (!cpf.isValid()) {
                    ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
                }
            } catch (Exception e) {
                ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
            }
        }
        return cpf;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String cpf = null;
        if (value != null) {
            Cpf cpfValue = (Cpf) value;
            cpf = cpfValue.format();
        }
        return cpf;
    }
}
