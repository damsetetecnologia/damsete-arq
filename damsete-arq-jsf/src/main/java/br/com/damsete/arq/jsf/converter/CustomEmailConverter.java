package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.types.Email;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by andre on 05/05/2016.
 */
public class CustomEmailConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Email email = null;
        if (StringUtils.isNotEmpty(value)) {
            email = new Email(value);
        }
        return email;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String email = null;
        if (value != null) {
            Email emailValue = (Email) value;
            email = emailValue.getRawValue();
        }
        return email;
    }
}
