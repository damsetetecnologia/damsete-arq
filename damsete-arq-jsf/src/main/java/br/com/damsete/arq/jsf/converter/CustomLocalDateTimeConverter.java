package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.jsf.utils.ConverterUtils;
import br.com.damsete.arq.utils.Dates;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.regex.Pattern;

public class CustomLocalDateTimeConverter implements Converter {

    private static final String MESSAGE_CONVERTER_ERROR = "arq.CustomLocalDateTimeConverter";
    private static final Pattern REGEX = Pattern.compile("^\\d{2}/\\d{2}/\\d{4}$");
    private static final String DATE_TIMER_PATTERN = "dd/MM/yyyy 'as' HH:mm:ss";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        LocalDate data = null;
        if (StringUtils.isNotEmpty(value)) {
            if (REGEX.matcher(value).matches()) {
                try {
                    data = Dates.parseLocalDate(value, DATE_TIMER_PATTERN);
                } catch (Exception e) {
                    ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
                }
            } else {
                ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
            }
        }
        return data;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String formatedDate = Dates.formatLocalDate((LocalDateTime) value, DATE_TIMER_PATTERN);
        return formatedDate;
    }
}
