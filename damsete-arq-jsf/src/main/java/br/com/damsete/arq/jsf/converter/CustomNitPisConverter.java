package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.jsf.utils.ConverterUtils;
import br.com.damsete.arq.types.NitPis;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by andre on 23/10/2016.
 */
public class CustomNitPisConverter implements Converter {

    private static final String MESSAGE_CONVERTER_ERROR = "arq.CustomNitPisConverter";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        NitPis nitPis = null;
        if (StringUtils.isNotEmpty(value)) {
            try {
                nitPis = new NitPis(Long.parseLong(value.replace(".", "").replace("-", "")));
                if (!nitPis.isValid()) {
                    ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
                }
            } catch (Exception e) {
                ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
            }
        }
        return nitPis;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String nitPis = null;
        if (value != null) {
            NitPis nitPisValue = (NitPis) value;
            nitPis = nitPisValue.format();
        }
        return nitPis;
    }
}
