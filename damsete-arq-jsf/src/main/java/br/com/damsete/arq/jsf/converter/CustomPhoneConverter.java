package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.jsf.utils.ConverterUtils;
import br.com.damsete.arq.types.Phone;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by andre on 05/05/2016.
 */
public class CustomPhoneConverter implements Converter {

    private static final String MESSAGE_CONVERTER_ERROR = "arq.CustomPhoneConverter";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Phone phone = null;
        if (StringUtils.isNotEmpty(value)) {
            try {
                phone = new Phone(Long.parseLong(value.replace("(", "").replace(")", "").replace("-", "")));
            } catch (Exception e) {
                ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
            }
        }
        return phone;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String phone = null;
        if (value != null) {
            Phone phoneValue = (Phone) value;
            phone = phoneValue.format();
        }
        return phone;
    }
}
