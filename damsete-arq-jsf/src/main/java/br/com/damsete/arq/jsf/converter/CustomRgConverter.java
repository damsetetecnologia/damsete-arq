package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.jsf.utils.ConverterUtils;
import br.com.damsete.arq.types.Rg;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by andre on 23/10/2016.
 */
public class CustomRgConverter implements Converter {

    private static final String MESSAGE_CONVERTER_ERROR = "arq.CustomRgConverter";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Rg rg = null;
        if (StringUtils.isNotEmpty(value)) {
            try {
                rg = new Rg(Long.parseLong(value.replace(".", "")));
            } catch (Exception e) {
                ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
            }
        }
        return rg;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String rg = null;
        if (value != null) {
            Rg rgValue = (Rg) value;
            rg = rgValue.format();
        }
        return rg;
    }
}
