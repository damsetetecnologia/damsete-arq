package br.com.damsete.arq.jsf.converter;

import br.com.damsete.arq.jsf.utils.ConverterUtils;
import br.com.damsete.arq.types.ZipCode;
import org.apache.commons.lang3.StringUtils;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Created by andre on 05/05/2016.
 */
public class CustomZipCodeConverter implements Converter {

    private static final String MESSAGE_CONVERTER_ERROR = "arq.CustomZipCodeConverter";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        ZipCode zipCode = null;
        if (StringUtils.isNotEmpty(value)) {
            try {
                zipCode = new ZipCode(Long.parseLong(value.replace("-", "")));
            } catch (Exception e) {
                ConverterUtils.addMessageErrorConverter(MESSAGE_CONVERTER_ERROR, component);
            }
        }
        return zipCode;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String zipCode = null;
        if (value != null) {
            ZipCode zipCodeValue = (ZipCode) value;
            zipCode = zipCodeValue.format();
        }
        return zipCode;
    }
}
