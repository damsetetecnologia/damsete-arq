package br.com.damsete.arq.jsf.exception;

import br.com.damsete.arq.jsf.exception.converter.factory.ExceptionMessageConverter;
import br.com.damsete.arq.jsf.exception.converter.factory.ExceptionMessageConverterFactory;
import br.com.damsete.arq.log.core.LoggerAPI;
import org.omnifaces.config.WebXml;
import org.omnifaces.context.OmniPartialViewContext;
import org.omnifaces.util.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import javax.el.ELException;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.PhaseId;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by andre on 05/11/2016.
 */
public class ArqExceptionHandler extends ExceptionHandlerWrapper {

    private static final Set<Class<? extends Throwable>> STANDARD_TYPES_TO_UNWRAP = Utils.unmodifiableSet(FacesException.class, ELException.class);

    private static final String ERROR_DEFAULT_LOCATION_MISSING =
            "Either HTTP 500 or java.lang.Throwable error page is required in web.xml or web-fragment.xml."
                    + " Neither was found.";
    private static final String LOG_EXCEPTION_HANDLED =
            "ArqExceptionHandler: An exception occurred during processing JSF ajax request."
                    + " Error page '%s' will be shown.";
    private static final String LOG_RENDER_EXCEPTION_HANDLED =
            "ArqExceptionHandler: An exception occurred during rendering JSF ajax response."
                    + " Error page '%s' will be shown.";
    private static final String LOG_RENDER_EXCEPTION_UNHANDLED =
            "ArqExceptionHandler: An exception occurred during rendering JSF ajax response."
                    + " Error page '%s' CANNOT be shown as response is already committed."
                    + " Consider increasing 'javax.faces.FACELETS_BUFFER_SIZE' if it really needs to be handled.";
    private static final String LOG_ERROR_PAGE_ERROR =
            "ArqExceptionHandler: Well, another exception occurred during rendering error page '%s'."
                    + " Trying to render a hardcoded error page now.";
    private static final String ERROR_PAGE_ERROR =
            "<?xml version='1.0' encoding='UTF-8'?><partial-response id='error'><changes><update id='javax.faces.ViewRoot'>"
                    + "<![CDATA[<html lang='en'><head><title>Error in error</title></head><body><section><h2>Oops!</h2>"
                    + "<p>A problem occurred during processing the ajax request. Subsequently, another problem occurred during"
                    + " processing the error page which should inform you about that problem.</p><p>If you are the responsible"
                    + " web developer, it's time to read the server logs about the bug in the error page itself.</p></section>"
                    + "</body></html>]]></update></changes></partial-response>";

    private static final String ATTRIBUTE_ERROR_EXCEPTION = "javax.servlet.error.exception";
    private static final String ATTRIBUTE_ERROR_EXCEPTION_LOG = "javax.servlet.error.exception.log";
    private static final String ATTRIBUTE_ERROR_EXCEPTION_TYPE = "javax.servlet.error.exception_type";
    private static final String ATTRIBUTE_ERROR_MESSAGE = "javax.servlet.error.message";
    private static final String ATTRIBUTE_ERROR_REQUEST_URI = "javax.servlet.error.request_uri";
    private static final String ATTRIBUTE_ERROR_STATUS_CODE = "javax.servlet.error.status_code";

    private Class<? extends Throwable>[] exceptionTypesToUnwrap = null;
    private ExceptionHandler wrapped = null;

    public ArqExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
        exceptionTypesToUnwrap = getExceptionTypesToUnwrap();
    }

    @Override
    public void handle() throws FacesException {
        handleException(Faces.getContext());
        this.wrapped.handle();
    }

    private void handleException(FacesContext context) {
        if (context == null) {
            return;
        }

        Iterator<ExceptionQueuedEvent> unhandledExceptionQueuedEvents = getUnhandledExceptionQueuedEvents().iterator();
        if (!unhandledExceptionQueuedEvents.hasNext()) {
            return;
        }

        Throwable exception = unhandledExceptionQueuedEvents.next().getContext().getException();
        if (exception instanceof AbortProcessingException) {
            return;
        }

        unhandledExceptionQueuedEvents.remove();
        exception = findExceptionRootCause(exception);
        if (shouldHandleExceptionRootCause(context, exception)) {
            String errorPageLocation = findErrorPageLocation(exception);
            if (errorPageLocation == null) {
                throw new IllegalArgumentException(ERROR_DEFAULT_LOCATION_MISSING);
            }

            if (!canRenderErrorPageView(context, exception, errorPageLocation)) {
                return;
            }

            HttpServletRequest request = FacesLocal.getRequest(context);
            request.setAttribute(ATTRIBUTE_ERROR_EXCEPTION, exception);
            request.setAttribute(ATTRIBUTE_ERROR_EXCEPTION_LOG, exception);
            request.setAttribute(ATTRIBUTE_ERROR_EXCEPTION_TYPE, exception.getClass());
            request.setAttribute(ATTRIBUTE_ERROR_MESSAGE, exception.getMessage());
            request.setAttribute(ATTRIBUTE_ERROR_REQUEST_URI, request.getRequestURI());
            request.setAttribute(ATTRIBUTE_ERROR_STATUS_CODE, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            verifyAjaxRequest(context, exception);

            NavigationHandler navigationHandler = context.getApplication().getNavigationHandler();
            navigationHandler.handleNavigation(context, null, errorPageLocation);
            context.renderResponse();

        }
        while (unhandledExceptionQueuedEvents.hasNext()) {
            unhandledExceptionQueuedEvents.next();
            unhandledExceptionQueuedEvents.remove();
        }
    }

    private Throwable findExceptionRootCause(Throwable exception) {
        return Exceptions.unwrap(exception, exceptionTypesToUnwrap);
    }

    private boolean shouldHandleExceptionRootCause(FacesContext context, Throwable exception) {
        boolean shouldHandle = true;

        WebApplicationContext applicationContext = FacesContextUtils.getRequiredWebApplicationContext(context);
        ExceptionMessageConverterFactory exceptionMessageConverterFactory = (ExceptionMessageConverterFactory) applicationContext.getBean("exceptionMessageConverterFactory");
        ExceptionMessageConverter<Throwable> converter = exceptionMessageConverterFactory.getMessageExceptionConverter(exception);
        if (converter != null) {
            FacesMessage facesMessage = converter.convert(exception);
            if (facesMessage != null) {
                Messages.addGlobal(facesMessage);
                shouldHandle = false;
            }
        }

        return shouldHandle;
    }

    private String findErrorPageLocation(Throwable exception) {
        return WebXml.INSTANCE.findErrorPageLocation(exception);
    }

    private void logException(Throwable exception, String location, String message) {
        LoggerAPI.error(String.format(message, location), exception);
    }

    private void verifyAjaxRequest(FacesContext context, Throwable exception) {
        if (!context.getPartialViewContext().isAjaxRequest()) {
            throw new FacesException(exception); // Not an ajax request, let default web.xml error page mechanism do its job.
        }
    }

    private boolean canRenderErrorPageView(FacesContext context, Throwable exception, String errorPageLocation) {
        if (context.getCurrentPhaseId() != PhaseId.RENDER_RESPONSE) {
            logException(exception, errorPageLocation, LOG_EXCEPTION_HANDLED);
            return true;
        } else if (!context.getExternalContext().isResponseCommitted()) {
            logException(exception, errorPageLocation, LOG_RENDER_EXCEPTION_HANDLED);
            resetResponse(context);
            return true;
        } else {
            logException(exception, errorPageLocation, LOG_RENDER_EXCEPTION_UNHANDLED);
            OmniPartialViewContext.getCurrentInstance(context).closePartialResponse();
            return false;
        }
    }

    private void resetResponse(FacesContext context) {
        ExternalContext externalContext = context.getExternalContext();
        String contentType = externalContext.getResponseContentType();
        String characterEncoding = externalContext.getResponseCharacterEncoding();
        externalContext.responseReset();
        OmniPartialViewContext.getCurrentInstance(context).resetPartialResponse();
        externalContext.setResponseContentType(contentType);
        externalContext.setResponseCharacterEncoding(characterEncoding);
    }

    private Class<? extends Throwable>[] getExceptionTypesToUnwrap() {
        Set<Class<? extends Throwable>> typesToUnwrap = new HashSet<>(STANDARD_TYPES_TO_UNWRAP);
        return typesToUnwrap.toArray(new Class[typesToUnwrap.size()]);
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }
}
