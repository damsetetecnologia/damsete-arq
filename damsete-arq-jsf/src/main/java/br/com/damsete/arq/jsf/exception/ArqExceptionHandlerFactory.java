package br.com.damsete.arq.jsf.exception;

import org.omnifaces.exceptionhandler.DefaultExceptionHandlerFactory;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * Created by andre on 05/11/2016.
 */
public class ArqExceptionHandlerFactory extends DefaultExceptionHandlerFactory {

    public ArqExceptionHandlerFactory(ExceptionHandlerFactory wrapped) {
        super(wrapped);
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        return new ArqExceptionHandler(getWrapped().getExceptionHandler());
    }
}
