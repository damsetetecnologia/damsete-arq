package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.exception.ApiException;
import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import org.springframework.stereotype.Component;

/**
 * Created by andre on 07/06/2018.
 */
@Component(value = "exceptionMessageConverter.br.com.damsete.arq.exception.ApiException")
public class ApiExceptionMessageConverter extends AbstractExceptionMessageConverter<ApiException> {

    @Override
    protected String getSummary(ApiException exception) {
        return exception.getLocalizedMessage();
    }
}
