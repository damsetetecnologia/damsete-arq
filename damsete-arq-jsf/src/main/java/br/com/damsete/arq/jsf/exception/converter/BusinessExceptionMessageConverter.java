package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.exception.BusinessException;
import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import br.com.damsete.arq.message.ArqMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by andre on 23/04/2016.
 */
@Component(value = "exceptionMessageConverter.br.com.damsete.arq.exception.BusinessException")
public class BusinessExceptionMessageConverter extends AbstractExceptionMessageConverter<BusinessException> {

    @Autowired
    protected ArqMessageSource messageSource = null;

    @Override
    protected String getSummary(BusinessException exception) {
        return this.messageSource.getMessage(exception.getMessageSourceResolvable(), Locale.getDefault());
    }
}
