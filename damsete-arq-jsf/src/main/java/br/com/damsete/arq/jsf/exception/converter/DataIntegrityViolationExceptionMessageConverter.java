package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import br.com.damsete.arq.jsf.exception.converter.factory.ExceptionMessageConverter;
import br.com.damsete.arq.jsf.exception.converter.factory.ExceptionMessageConverterFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;

/**
 * Created by andre on 10/11/2016.
 */
@Component("exceptionMessageConverter.org.springframework.dao.DataIntegrityViolationException")
public class DataIntegrityViolationExceptionMessageConverter extends AbstractExceptionMessageConverter<DataIntegrityViolationException> {

    @Autowired
    private ExceptionMessageConverterFactory exceptionMessageConverterFactory = null;

    @Override
    public FacesMessage convert(DataIntegrityViolationException exception) {
        Throwable nested = null;

        if (exception.getCause() != null) {
            nested = exception.getCause();
        }

        if (nested != null) {
            ExceptionMessageConverter<Throwable> exceptionMessageConverter = this.exceptionMessageConverterFactory.getMessageExceptionConverter(nested);
            return exceptionMessageConverter.convert(nested);
        }

        return super.convert(exception);
    }

    protected String getSummary(DataIntegrityViolationException exception) {
        return null;
    }
}
