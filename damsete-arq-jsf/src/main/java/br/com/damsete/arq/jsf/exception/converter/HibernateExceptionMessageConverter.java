package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import br.com.damsete.arq.message.ArqMessageSource;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.StaleObjectStateException;
import org.hibernate.StaleStateException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

/**
 * Created by andre on 10/11/2016.
 */
@Component(value = "exceptionMessageConverter.org.hibernate.HibernateException")
public class HibernateExceptionMessageConverter extends AbstractExceptionMessageConverter<HibernateException> {

    // O elemento selecionado não se encontra mais no banco de dados.
    private static final String ERROR_STALE_STATE = "hibernate.stalestate.message";

    // O elemento selecionado foi modificado por outra pessoa exception não pode mais ser utilizado. Por favor, reinicie a operação.
    private static final String ERROR_STALE_OBJECT_STATE = "hibernate.staleobjectstate.message";

    // O elemento selecionado não pode ser removido porque ainda está sendo referenciado pelo sistema.
    private static final String ERROR_FOREIGN_KEY = "hibernate.foreignkey.message";

    // Caro usuário, apenas um resultado era esperado na operação realizada porém, o banco de dados retornou {0}. Isto pode ser um
    // problema de migração ou inconsistência de dados. Entre em contato com o Administrador do Sistema.
    private static final String ERROR_NON_UNIQUE_RESULT = "hibernate.nonuniqueresult.message";

    // Ocorreu um erro de restrição de unicidade.
    private static final String ERROR_UNIQUE = "hibernate.unique.message";

    @Autowired
    protected ArqMessageSource messageSource = null;

    @Override
    protected String getSummary(HibernateException exception) {
        if (exception instanceof StaleStateException) {
            return this.messageSource.getMessage(ERROR_STALE_STATE);
        }

        if (exception instanceof StaleObjectStateException) {
            return this.messageSource.getMessage(ERROR_STALE_OBJECT_STATE);
        }

        if (exception instanceof ConstraintViolationException) {
            if (exception.getCause() instanceof SQLException) {
                SQLException bue = (SQLException) exception.getCause();
                if (bue.getSQLState().equals("23505")) {
                    return this.messageSource.getMessage(ERROR_UNIQUE);
                }
            }
            return this.messageSource.getMessage(ERROR_FOREIGN_KEY);
        }

        if (exception instanceof NonUniqueResultException) {
            return this.messageSource.getMessage(ERROR_NON_UNIQUE_RESULT);
        }

        return null;
    }
}
