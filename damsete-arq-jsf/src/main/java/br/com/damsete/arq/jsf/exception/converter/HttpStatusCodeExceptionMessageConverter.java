package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

/**
 * Created by andre on 07/06/2018.
 */
@Component(value = "exceptionMessageConverter.org.springframework.web.client.HttpStatusCodeException")
public class HttpStatusCodeExceptionMessageConverter extends AbstractExceptionMessageConverter<HttpStatusCodeException> {

    @Override
    protected String getSummary(HttpStatusCodeException exception) {
        return exception.getLocalizedMessage();
    }
}
