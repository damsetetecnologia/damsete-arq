package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.exception.MissingConfigurationException;
import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import br.com.damsete.arq.message.ArqMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by andre on 06/04/2016.
 */
@Component(value = "exceptionMessageConverter.br.com.damsete.arq.exception.MissingConfigurationException")
public class MissingConfigurationExceptionMessageConverter extends AbstractExceptionMessageConverter<MissingConfigurationException> {

    @Autowired
    protected ArqMessageSource messageSource = null;

    @Override
    protected String getSummary(MissingConfigurationException exception) {
        return this.messageSource.getMessage(exception.getMessageSourceResolvable(), Locale.getDefault());
    }
}
