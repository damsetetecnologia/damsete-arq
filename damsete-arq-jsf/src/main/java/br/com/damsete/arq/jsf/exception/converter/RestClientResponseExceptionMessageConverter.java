package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.api.ApiError;
import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;

/**
 * Created by andre on 07/06/2018.
 */
@Component(value = "exceptionMessageConverter.org.springframework.web.client.RestClientResponseException")
public class RestClientResponseExceptionMessageConverter extends AbstractExceptionMessageConverter<RestClientResponseException> {

    @Override
    protected String getSummary(RestClientResponseException exception) {
        ApiError apiError = ApiError.getApiError(exception);
        return apiError.getMessage();
    }
}
