package br.com.damsete.arq.jsf.exception.converter;

import br.com.damsete.arq.jsf.exception.converter.factory.AbstractExceptionMessageConverter;
import org.springframework.stereotype.Component;

/**
 * Created by andre on 10/11/2016.
 */
@Component(value = "exceptionMessageConverter.java.lang.Throwable")
public class ThrowableExceptionMessageConverter extends AbstractExceptionMessageConverter<Throwable> {

    @Override
    protected String getSummary(Throwable exception) {
        return null;
    }
}
