package br.com.damsete.arq.jsf.exception.converter.factory;

import javax.faces.application.FacesMessage;

/**
 * Created by andre on 06/04/2016.
 */
public abstract class AbstractExceptionMessageConverter<T extends Throwable> implements ExceptionMessageConverter<T> {

    @Override
    public FacesMessage convert(T exception) {
        String summary = getSummary(exception);
        if (summary != null) {
            return new FacesMessage(getSeverity(exception), summary, this.getDetail(exception));
        } else {
            return null;
        }
    }

    protected String getDetail(T exception) {
        return exception.getLocalizedMessage();
    }

    protected FacesMessage.Severity getSeverity(T exception) {
        return FacesMessage.SEVERITY_ERROR;
    }

    protected abstract String getSummary(T exception);
}
