package br.com.damsete.arq.jsf.exception.converter.factory;

import javax.faces.application.FacesMessage;

/**
 * Created by andre on 06/04/2016.
 */
public interface ExceptionMessageConverter<T extends Throwable> {

    FacesMessage convert(T exception);
}
