package br.com.damsete.arq.jsf.exception.converter.factory;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by andre on 06/04/2016.
 */
@Component
public class ExceptionMessageConverterFactory {

    @Autowired
    private BeanFactory beanFactory = null;

    public ExceptionMessageConverter<Throwable> getMessageExceptionConverter(Throwable t) {
        return getMessageExceptionConverter(t.getClass());
    }

    private ExceptionMessageConverter<Throwable> getMessageExceptionConverter(Class<? extends Throwable> clazz) {
        Class c = clazz;
        while (!c.isAssignableFrom(Object.class)) {
            String id = this.getMessageExceptionConverterSpringId(c.getCanonicalName());
            if (this.beanFactory.containsBean(id)) {
                return (ExceptionMessageConverter<Throwable>) this.beanFactory.getBean(id);
            }
            c = c.getSuperclass();
        }
        return null;
    }

    private String getMessageExceptionConverterSpringId(String className) {
        return "exceptionMessageConverter." + className;
    }
}
