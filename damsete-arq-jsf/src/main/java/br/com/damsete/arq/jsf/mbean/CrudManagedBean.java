package br.com.damsete.arq.jsf.mbean;

import br.com.damsete.arq.data.application.CrudService;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;

/**
 * Created by andre on 13/07/2016.
 */
public interface CrudManagedBean<T extends Persistable<ID>, ID extends Serializable> extends Serializable {

    T getEntity();

    void setService(CrudService<T, ID> service);
}