package br.com.damsete.arq.jsf.message;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MultiPageMessagesSupport implements PhaseListener {

    private static final String TOKEN = "br.com.damsete.arq.jsf.message.SAVED_FACES_MESSAGES";

    @Override
    public void afterPhase(PhaseEvent event) {
        if (!PhaseId.RENDER_RESPONSE.equals(event.getPhaseId())) {
            FacesContext facesContext = event.getFacesContext();
            saveMessages(facesContext, facesContext.getExternalContext().getSessionMap());
        }
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        FacesContext facesContext = event.getFacesContext();
        saveMessages(facesContext, facesContext.getExternalContext().getSessionMap());
        if (PhaseId.RENDER_RESPONSE.equals(event.getPhaseId()) && !facesContext.getResponseComplete()) {
            restoreMessages(facesContext, facesContext.getExternalContext().getSessionMap());
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }

    private int restoreMessages(FacesContext facesContext, Map<String, Object> source) {
        int restoredCount = 0;
        if (FacesContext.getCurrentInstance() != null) {
            List messages = (List) source.remove(TOKEN);
            if (messages == null) {
                return 0;
            }
            restoredCount = messages.size();
            for (Object element : messages) {
                facesContext.addMessage(null, (FacesMessage) element);
            }
        }
        return restoredCount;
    }

    private int saveMessages(FacesContext facesContext, Map<String, Object> destination) {
        int restoredCount = 0;
        if (FacesContext.getCurrentInstance() != null) {
            List messages = new ArrayList();
            for (Iterator iter = facesContext.getMessages(null); iter.hasNext(); ) {
                messages.add(iter.next());
                iter.remove();
            }
            if (messages.size() > 0) {
                List existingMessages = (List) destination.get(TOKEN);
                if (existingMessages != null) {
                    existingMessages.addAll(messages);
                } else {
                    destination.put(TOKEN, messages);
                }
                restoredCount = messages.size();
            }
        }
        return restoredCount;
    }
}