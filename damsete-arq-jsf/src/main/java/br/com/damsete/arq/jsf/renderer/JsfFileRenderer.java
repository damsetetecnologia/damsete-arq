package br.com.damsete.arq.jsf.renderer;

import br.com.damsete.arq.log.core.LoggerAPI;
import br.com.damsete.arq.renderer.FileRenderer;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;
import org.springframework.stereotype.Component;

import java.io.*;

/**
 * Created by andre on 01/06/2016.
 */
@Component
public class JsfFileRenderer implements FileRenderer {

    @Override
    public void render(String content, ContentType contentType, String fileName, boolean forceDownload) {
        try {
            Faces.getResponse().setContentType(contentType.getContentType());

            String forceDownloadCommand = forceDownload ? "attachment; " : "";
            Faces.getResponse().setHeader("Content-Disposition", forceDownloadCommand + "filename=\"" + fileName + "\"");

            Faces.getResponse().getWriter().write(content);
            Faces.getResponse().getWriter().flush();
            Faces.getResponse().getWriter().close();
            Faces.getResponse().flushBuffer();
        } catch (IOException e) {
            LoggerAPI.error(e.getMessage(), e);
            Messages.addGlobalError(e.getMessage());
        }
        Faces.getContext().responseComplete();
    }

    @Override
    public void render(byte[] byteArray, ContentType contentType, String fileName, boolean forceDownload) {
        try {
            Faces.getResponse().setContentType(contentType.getContentType());
            Faces.getResponse().setContentLength(byteArray.length);

            String forceDownloadCommand = forceDownload ? "attachment; " : "";
            Faces.getResponse().setHeader("Content-Disposition", forceDownloadCommand + "filename=\"" + fileName + "\"");

            Faces.getResponse().getOutputStream().write(byteArray, 0, byteArray.length);
            Faces.getResponse().getOutputStream().flush();
            Faces.getResponse().getOutputStream().close();
        } catch (IOException e) {
            LoggerAPI.error(e.getMessage(), e);
            Messages.addGlobalError(e.getMessage());
        }
        Faces.getContext().responseComplete();
    }

    @Override
    public void render(byte[] byteArray, ContentType contentType, String fileName) {
        render(byteArray, contentType, fileName, false);
    }

    @Override
    public void render(InputStream stream, ContentType contentType, String fileName) {
        render(stream, contentType, fileName, false);
    }

    @Override
    public void render(InputStream stream, ContentType contentType, String fileName, boolean forceDownload) {
        render(getBytes(stream), contentType, fileName, forceDownload);
    }

    @Override
    public void render(File file, ContentType contentType, String fileName) {
        render(file, contentType, fileName, false);
    }

    @Override
    public void render(File file, ContentType contentType, String fileName, boolean forceDownload) {
        InputStream stream = null;
        try {
            stream = new FileInputStream(file);
            render(stream, contentType, fileName, forceDownload);
        } catch (FileNotFoundException e) {
            LoggerAPI.error(e.getMessage(), e);
            Messages.addGlobalError(e.getMessage());
        } finally {
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (IOException e) {
                LoggerAPI.error(e.getMessage(), e);
                Messages.addGlobalError(e.getMessage());
            }
        }
    }

    private byte[] getBytes(InputStream stream) {
        byte[] byteArray = null;
        try {
            int thisLine;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            while ((thisLine = stream.read()) != -1) {
                bos.write(thisLine);
            }
            bos.flush();
            byteArray = bos.toByteArray();

            if (bos != null) {
                bos.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return byteArray;
    }
}
