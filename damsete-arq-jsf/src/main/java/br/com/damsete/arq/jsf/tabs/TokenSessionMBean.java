package br.com.damsete.arq.jsf.tabs;

import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.jsf.FacesContextUtils;

import java.io.Serializable;
import java.util.UUID;

import static org.omnifaces.util.Faces.getContext;

/**
 * Created by andre on 19/04/2018.
 */
@Controller
@Scope("session")
@Data
public class TokenSessionMBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String token = UUID.randomUUID().toString();

    public void store(String token) {
        this.token = token;
    }

    public static TokenSessionMBean getInstance() {
        return (TokenSessionMBean) FacesContextUtils.getWebApplicationContext(getContext()).getBean("tokenSessionMBean");
    }
}
