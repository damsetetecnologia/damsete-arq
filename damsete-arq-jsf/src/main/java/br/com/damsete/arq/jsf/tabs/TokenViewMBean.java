package br.com.damsete.arq.jsf.tabs;

import br.com.damsete.arq.exception.BusinessException;
import lombok.Data;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Controller;
import org.springframework.web.jsf.FacesContextUtils;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.UUID;

import static org.omnifaces.util.Faces.getContext;

/**
 * Created by andre on 19/04/2018.
 */
@Controller
@Scope("view")
@Data
public class TokenViewMBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String token = UUID.randomUUID().toString();

    @PostConstruct
    public void init() {
        TokenSessionMBean.getInstance().store(this.token);
    }

    public void verifyTabs() throws BusinessException {
        String tokenSession = TokenSessionMBean.getInstance().getToken();
        String tokenView = this.token;

        if (!tokenSession.equals(tokenView)) {
            throw new BusinessException(new DefaultMessageSourceResolvable(new String[]{"user.pageinactive.message"}));
        }
    }

    public static TokenViewMBean getInstance() {
        return (TokenViewMBean) FacesContextUtils.getWebApplicationContext(getContext()).getBean("tokenViewMBean");
    }
}
