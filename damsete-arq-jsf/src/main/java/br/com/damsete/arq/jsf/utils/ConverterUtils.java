package br.com.damsete.arq.jsf.utils;

import org.apache.commons.lang3.StringUtils;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import javax.faces.component.UIComponent;
import javax.faces.convert.ConverterException;
import java.util.ResourceBundle;

/**
 * Created by andre on 06/05/2016.
 */
public class ConverterUtils {

    public static void addMessageErrorConverter(String msgKey, UIComponent component) {
        if (StringUtils.isNotEmpty(msgKey)) {
            String messageBundle = Faces.getApplication().getMessageBundle();
            ResourceBundle resourceBundle = ResourceBundle.getBundle(messageBundle);
            String fieldName = component.getId();
            String msg = resourceBundle.getString(msgKey + "." + fieldName);
            throw new ConverterException(Messages.createError(msg));
        }
    }
}
