package br.com.damsete.arq.jsf.utils;

import br.com.damsete.arq.exception.BusinessException;
import br.com.damsete.arq.jsf.tabs.TokenViewMBean;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.omnifaces.util.Faces.getSession;

/**
 * Created by andre on 29/01/2017.
 */
public class Operations {

    private static final String USER_OPERATIONS = "user_operations";
    //private static final String PAGE_OPERATION = "page_operation";

    public static void prepareOperation(String operation) throws BusinessException {
        if (operation == null) {
            return;
        }
        List<String> userOperations = (List<String>) getSession().getAttribute(USER_OPERATIONS);
        if (userOperations == null) {
            userOperations = newArrayList();
        }
        if (userOperations.contains(operation)) {
            throw new BusinessException(new DefaultMessageSourceResolvable(new String[]{"user.operationactive.message"}));
        }
        userOperations.add(operation);
        getSession().setAttribute(USER_OPERATIONS, userOperations);
    }

    public static void removeOperation(String operation) {
        if (operation == null) {
            return;
        }
        List<String> userOperations = (List<String>) getSession().getAttribute(USER_OPERATIONS);
        if (userOperations == null) {
            userOperations = newArrayList();
        }
        userOperations.remove(operation);
        getSession().setAttribute(USER_OPERATIONS, userOperations);
    }

    public static void checkActivePage() throws BusinessException {
        TokenViewMBean tokenViewMBean = TokenViewMBean.getInstance();
        tokenViewMBean.verifyTabs();
    }
}
