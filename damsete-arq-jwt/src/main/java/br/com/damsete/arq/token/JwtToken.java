package br.com.damsete.arq.token;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class JwtToken implements Serializable {

    private static final long serialVersionUID = 1L;

    private String applicationName = null;
    private String username = null;
    private Long tenantId = null;

    public JwtToken(String info) {
        String[] data = info.split("-");
        this.username = data[0];
        this.applicationName = data[1];
        this.tenantId = Long.parseLong(data[2]);
    }

    public String getInfo() {
        return String.format("%s-%s-%s", this.username, this.applicationName, this.tenantId);
    }
}
