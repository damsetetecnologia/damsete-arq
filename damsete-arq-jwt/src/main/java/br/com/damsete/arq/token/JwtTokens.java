package br.com.damsete.arq.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@Component
public class JwtTokens implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";

    @Value("${damsete.jwt.secret}")
    private String secret = null;
    @Value("${damsete.jwt.access-token-validity-seconds}")
    private int accessTokenValiditySeconds = 0;
    @Value("${damsete.jwt.refresh-token-validity-seconds}")
    private int refreshTokenValiditySeconds = 0;

    public String generateToken(JwtToken jwtToken) {
        Map<String, Object> claims = newHashMap();

        claims.put(CLAIM_KEY_USERNAME, jwtToken.getInfo());

        final Date createdDate = new Date();
        claims.put(CLAIM_KEY_CREATED, createdDate);

        return doGenerateToken(claims);
    }

    public Boolean canTokenBeRefreshed(String token) {
        return !isTokenExpired(token);
    }

    public String refreshToken(String token) {
        String refreshedToken;
        try {
            final Claims claims = getClaimsFromToken(token);
            claims.put(CLAIM_KEY_CREATED, new Date());
            refreshedToken = doGenerateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public Boolean validateToken(String token, JwtToken jwtToken) {
        final JwtToken jwtTokenAux = getUsernameFromToken(token);
        return jwtTokenAux.getUsername().equals(jwtToken.getUsername()) && !isTokenExpired(token);
    }

    public JwtToken getUsernameFromToken(String token) {
        String info;
        try {
            final Claims claims = getClaimsFromToken(token);
            info = claims.getSubject();
        } catch (Exception e) {
            return null;
        }
        return new JwtToken(info);
    }

    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(this.secret)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private String doGenerateToken(Map<String, Object> claims) {
        final Date createdDate = (Date) claims.get(CLAIM_KEY_CREATED);
        final Date expirationDate = new Date(createdDate.getTime() + this.accessTokenValiditySeconds * 1000);

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, this.secret)
                .compact();
    }
}
