package br.com.damsete.arq.log;

import br.com.damsete.arq.log.core.Logger;
import br.com.damsete.arq.log.core.LoggerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogConfig {

    @Bean
    public Logger logger() {
        return new LoggerBuilder().forLogger("br.com.damsete").create();
    }
}
