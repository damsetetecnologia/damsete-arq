package br.com.damsete.arq.log.core;

import java.util.Date;

public class LogKeyGenerator {

    public String generateKey(String system) {

        // Formato:
        // nome do sistema  = fopag
        // data completa    = 9999999999999

        if (system == null) {
            throw new IllegalArgumentException("unidentified system");
        }

        StringBuilder key = new StringBuilder();
        long time = new Date().getTime();
        key.append(system).append(time);

        return key.toString();
    }

}
