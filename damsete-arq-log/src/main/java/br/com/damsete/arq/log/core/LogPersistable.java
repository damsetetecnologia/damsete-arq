package br.com.damsete.arq.log.core;

public interface LogPersistable {

    String getLog();
}
