package br.com.damsete.arq.log.core;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;

public class Logger {

    private org.apache.logging.log4j.Logger logger;

    public Logger() {
        this.logger = LogManager.getRootLogger();
    }

    public Logger(String name) {
        this.logger = LogManager.getLogger(name);
    }

    public Logger(Class<?> clazz) {
        this.logger = LogManager.getLogger(clazz);
    }

    public void log(Level level, LogPersistable logPersistable) {
        this.logger.log(level, logPersistable.getLog());
    }

    public void log(Level level, String log) {
        this.logger.log(level, log);
    }

    public void debug(String log) {
        this.logger.debug(log);
    }

    public void info(String log) {
        this.logger.info(log);
    }

    public void warn(String log) {
        this.logger.warn(log);
    }

    public void error(String log) {
        this.logger.error(log);
    }

    public void debug(LogPersistable log) {
        this.logger.debug(log.getLog());
    }

    public void info(LogPersistable log) {
        this.logger.info(log.getLog());
    }

    public void warn(LogPersistable log) {
        this.logger.warn(log.getLog());
    }

    public void error(LogPersistable log) {
        this.logger.error(log.getLog());
    }

    public void error(LogPersistable log, Throwable throwable) {
        this.logger.error(log.getLog(), throwable);
    }

    public void error(String error, Throwable throwable) {
        this.logger.error(error, throwable);
    }
}
