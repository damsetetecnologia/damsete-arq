package br.com.damsete.arq.log.core;

import org.apache.logging.log4j.Level;

public class LoggerAPI {

    public static void log(Level level, LogPersistable logPersistable) {
        Logger logger = new Logger(logPersistable.getClass().getName());
        logger.log(level, logPersistable.getLog());
    }

    public static void log(Class<?> clazz, Level level, String log) {
        Logger logger = new Logger(clazz);
        logger.log(level, log);
    }

    public static void debug(String log) {
        Logger logger = new Logger();
        logger.debug(log);
    }

    public static void debug(LogPersistable log) {
        Logger logger = new Logger(log.getClass().getName());
        logger.debug(log);
    }

    public static void info(String log) {
        Logger logger = new Logger();
        logger.info(log);
    }

    public static void info(LogPersistable log) {
        Logger logger = new Logger(log.getClass().getName());
        logger.info(log);
    }

    public static void warn(String log) {
        Logger logger = new Logger();
        logger.warn(log);
    }

    public static void warn(LogPersistable log) {
        Logger logger = new Logger(log.getClass().getName());
        logger.warn(log);
    }

    public static void error(String log) {
        Logger logger = new Logger();
        logger.error(log);
    }

    public static void error(LogPersistable log) {
        Logger logger = new Logger(log.getClass().getName());
        logger.error(log);
    }

    public static void error(LogPersistable log, Throwable t) {
        Logger logger = new Logger(log.getClass().getName());
        logger.error(log, t);
    }

    public static void error(String error, Throwable throwable) {
        Logger logger = new Logger();
        logger.error(error, throwable);
    }
}
