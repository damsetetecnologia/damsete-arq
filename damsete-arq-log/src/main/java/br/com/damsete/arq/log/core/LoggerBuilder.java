package br.com.damsete.arq.log.core;

public class LoggerBuilder {

    private Logger logger;

    public LoggerBuilder() {
        this.logger = new Logger();
    }

    public LoggerBuilder forLogger(String name) {
        this.logger = new Logger(name);
        return this;
    }

    public LoggerBuilder forLogger(Class<?> clazz) {
        this.logger = new Logger(clazz);
        return this;
    }

    public Logger create() {
        return logger;
    }
}
