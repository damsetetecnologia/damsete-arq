package br.com.damsete.arq.api;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
@Setter
public class ApiBody {

    private Object response;
    private String message;

    public ApiBody() {
        super();
    }

    public ApiBody(Object response) {
        this.response = response;
    }

    public ApiBody(String message) {
        this.message = message;
    }

    public ApiBody(Object response, String message) {
        this.response = response;
        this.message = message;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
