package br.com.damsete.arq.api;

import br.com.damsete.arq.log.core.LoggerAPI;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClientResponseException;

import java.io.IOException;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ApiError implements Serializable {

    private static final long serialVersionUID = 1L;

    private HttpStatus status;
    private String debugMessage;
    private String message;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public static ApiError getApiError(RestClientResponseException e) {
        try {
            return new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).readValue(e.getResponseBodyAsString(), ApiError.class);
        } catch (IOException ioe) {
            LoggerAPI.error(ioe.getMessage(), ioe);
        }
        return ApiError.unknown();
    }

    public static ApiError unknown() {
        return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "unknown", "unknown");
    }
}
