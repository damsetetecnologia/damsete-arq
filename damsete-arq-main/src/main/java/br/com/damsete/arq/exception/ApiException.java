package br.com.damsete.arq.exception;

public class ApiException extends RuntimeException {

    public ApiException(String message) {
        super(message);
    }
}
