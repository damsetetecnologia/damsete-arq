package br.com.damsete.arq.exception;

import lombok.Getter;
import org.springframework.context.MessageSourceResolvable;

/**
 * Created by andre on 16/05/2016.
 */
public class BusinessException extends ArqException {

    @Getter
    private MessageSourceResolvable messageSourceResolvable = null;

    public BusinessException(MessageSourceResolvable messageSourceResolvable) {
        this.messageSourceResolvable = messageSourceResolvable;
    }

}
