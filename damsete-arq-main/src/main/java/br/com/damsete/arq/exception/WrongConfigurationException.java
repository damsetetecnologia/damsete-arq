package br.com.damsete.arq.exception;

import lombok.Getter;
import org.springframework.context.MessageSourceResolvable;

/**
 * Created by andre on 16/05/2016.
 */
public class WrongConfigurationException extends RuntimeException {

    @Getter
    private MessageSourceResolvable messageSourceResolvable = null;

    public WrongConfigurationException(MessageSourceResolvable messageSourceResolvable) {
        this.messageSourceResolvable = messageSourceResolvable;
    }
}