package br.com.damsete.arq.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.util.Collection;

/**
 * Created by andre on 07/04/2017.
 */
public class Jsons {

    public static String toJson(Object value) {
        String json = null;
        try {
            json = new ObjectMapper().writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }
        return json;
    }

    public static <T> T fromJson(Class<? extends Collection> collectionClass, Class<?> elementClass, String value) {
        T list = null;
        try {
            CollectionType typeReference = TypeFactory.defaultInstance().constructCollectionType(collectionClass, elementClass);

            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

            list = mapper.readValue(value, typeReference);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return list;
    }

    public static <T> T fromJson(Class<?> elementClass, String value) {
        T object = null;
        try {
            JavaType typeReference = TypeFactory.defaultInstance().constructType(elementClass);

            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
            mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

            object = mapper.readValue(value, typeReference);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return object;
    }
}
