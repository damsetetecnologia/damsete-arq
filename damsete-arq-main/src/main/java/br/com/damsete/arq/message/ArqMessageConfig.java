package br.com.damsete.arq.message;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Created by andre on 21/06/2017.
 */
@Configuration
public class ArqMessageConfig {

    private static final String[] MESSAGES_SOURCE = new String[]{"classpath:i18n/messages",
            "classpath:i18n/messages_generic", "classpath:i18n/messages_customization",
            "classpath:i18n/messages_hibernate", "classpath:i18n/messages_jsf",
            "classpath:i18n/messages_security", "classpath:i18n/messages_report",
            "classpath:i18n/messages_files"};

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(MESSAGES_SOURCE);
        messageSource.setUseCodeAsDefaultMessage(true);
        return messageSource;
    }
}
