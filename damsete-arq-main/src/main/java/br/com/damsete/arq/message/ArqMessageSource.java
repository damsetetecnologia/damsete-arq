package br.com.damsete.arq.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

import java.util.Locale;

/**
 * Created by andre on 21/07/2016.
 */
@Component
public class ArqMessageSource implements MessageSource {

    @Autowired
    private MessageSource messageSource = null;

    @Override
    public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
        return this.messageSource.getMessage(code, args, defaultMessage, locale);
    }

    public String getMessage(String code, Object... args) {
        return this.messageSource.getMessage(code, args, Locale.getDefault());
    }

    public String getMessage(FieldError args) {
        return this.messageSource.getMessage(args, Locale.getDefault());
    }

    @Override
    public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
        return this.messageSource.getMessage(code, args, locale);
    }

    @Override
    public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
        return this.messageSource.getMessage(resolvable, locale);
    }

    public String getMessage(MessageSourceResolvable resolvable) throws NoSuchMessageException {
        return this.messageSource.getMessage(resolvable, Locale.getDefault());
    }
}