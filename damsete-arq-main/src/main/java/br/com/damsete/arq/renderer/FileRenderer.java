package br.com.damsete.arq.renderer;

import java.io.File;
import java.io.InputStream;

/**
 * Created by andre on 01/06/2016.
 */
public interface FileRenderer {

    void render(String content, ContentType contentType, String fileName, boolean forceDownload);

    void render(final byte[] byteArray, final ContentType contentType, final String fileName, boolean forceDownload);

    void render(final byte[] byteArray, final ContentType contentType, final String fileName);

    void render(final InputStream stream, final ContentType contentType, final String fileName);

    void render(final InputStream stream, final ContentType contentType, final String fileName, boolean forceDownload);

    void render(final File file, final ContentType contentType, final String fileName);

    void render(final File file, final ContentType contentType, final String fileName, boolean forceDownload);

    public enum ContentType {
        CSV("text/plain"),
        HTML("text/html"),
        ODT("application/vnd.oasis.opendocument.text"),
        PDF("application/pdf"),
        RTF("application/rtf"),
        TXT("text/plain"),
        XLS("application/vnd.ms-excel");

        private String contentType = null;

        private ContentType(String contentType) {
            this.contentType = contentType;
        }

        public String getContentType() {
            return contentType;
        }
    }
}
