package br.com.damsete.arq.types;

import br.com.damsete.arq.utils.Digits;

import static br.com.damsete.arq.utils.Strings.hasAllRepeatedDigits;
import static org.apache.commons.lang3.StringUtils.isNumeric;
import static org.apache.commons.lang3.StringUtils.leftPad;

/**
 * Created by andre on 09/04/2016.
 */
public class Cnpj implements Type<Long> {

    private static final long serialVersionUID = 1L;

    private Long cnpj;

    public Cnpj() {
        super();
    }

    public Cnpj(Long cnpj) {
        this.cnpj = cnpj;
    }

    @Override
    public String toString() {
        return format();
    }

    public Long getRawValue() {
        return cnpj;
    }

    public String format() {
        if (cnpj == null) {
            return null;
        }

        String cnpjPadded = leftPad(cnpj.toString(), 14, '0');
        return String.format("%s.%s.%s/%s-%s", cnpjPadded.substring(0, 2), cnpjPadded.substring(2, 5), cnpjPadded.substring(5, 8), cnpjPadded.substring(8, 12), cnpjPadded.substring(12, 14));
    }

    public String unformat() {
        if (cnpj == null || format().trim().equals("")) {
            return null;
        }

        return format().replace(".", "").replace("-", "").replace("/", "");
    }

    public boolean isValid() {
        return isValid(false);
    }

    public boolean isValid(boolean nullable) {
        if (!nullable && cnpj == null) {
            return false;
        }

        if (nullable && cnpj == null) {
            return true;
        }

        if (cnpj != null && !isValid(unformat())) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isValid(String cnpj) {
        if (!isNumeric(cnpj)) {
            return false;
        }

        if (cnpj.trim().length() != 14) {
            return false;
        }

        if (hasAllRepeatedDigits(cnpj)) {
            return false;
        }

        String cnpjWithoutDigit = cnpj.substring(0, cnpj.length() - 2);
        String digits = cnpj.substring(cnpj.length() - 2);

        String calculatedDigits = calculateDigits(cnpjWithoutDigit);

        if (!digits.equals(calculatedDigits)) {
            return false;
        }

        return true;
    }

    private String calculateDigits(String cnpjWithoutDigit) {
        Digits digits = new Digits(cnpjWithoutDigit);
        digits.complementModule().swapIfFind("0", 10, 11).mod(11);

        String digit1 = digits.calculate();
        digits.addDigit(digit1);
        String digit2 = digits.calculate();

        return digit1 + digit2;
    }
}