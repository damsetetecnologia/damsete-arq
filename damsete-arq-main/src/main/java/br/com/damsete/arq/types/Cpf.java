package br.com.damsete.arq.types;

import br.com.damsete.arq.utils.Digits;

import static br.com.damsete.arq.utils.Strings.hasAllRepeatedDigits;
import static org.apache.commons.lang3.StringUtils.isNumeric;
import static org.apache.commons.lang3.StringUtils.leftPad;

/**
 * Created by andre on 09/04/2016.
 */
public class Cpf implements Type<Long> {

    private static final long serialVersionUID = 1L;

    private Long cpf;

    public Cpf() {
        super();
    }

    public Cpf(Long cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return format();
    }

    public Long getRawValue() {
        return cpf;
    }

    public boolean isValid() {
        return isValid(false);
    }

    public boolean isValid(boolean nullable) {
        if (!nullable && cpf == null) {
            return false;
        }

        if (nullable && cpf == null) {
            return true;
        }

        if (cpf != null && !isValid(unformat())) {
            return false;
        } else {
            return true;
        }
    }

    public String format() {
        if (cpf == null) {
            return null;
        }

        String cpfPadded = leftPad(cpf.toString(), 11, '0');
        return String.format("%s.%s.%s-%s", cpfPadded.substring(0, 3), cpfPadded.substring(3, 6), cpfPadded.substring(6, 9), cpfPadded.substring(9, 11));
    }

    public String unformat() {
        if (cpf == null || format().trim().equals("")) {
            return null;
        }

        return format().replace(".", "").replace("-", "").replace("/", "");
    }

    private boolean isValid(String cpf) {
        if (!isNumeric(cpf)) {
            return false;
        }

        if (cpf.trim().length() != 11) {
            return false;
        }

        if (hasAllRepeatedDigits(cpf)) {
            return false;
        }

        String cpfWithoutDigit = cpf.substring(0, cpf.length() - 2);
        String digits = cpf.substring(cpf.length() - 2);

        String calculatedDigits = calculateDigits(cpfWithoutDigit);

        if (!digits.equals(calculatedDigits)) {
            return false;
        }

        return true;
    }

    private String calculateDigits(String cpfWithoutDigit) {
        Digits digits = new Digits(cpfWithoutDigit);
        digits.withMultipliers(2, 11).complementModule().swapIfFind("0", 10, 11).mod(11);

        String digit1 = digits.calculate();
        digits.addDigit(digit1);
        String digit2 = digits.calculate();

        return digit1 + digit2;
    }
}