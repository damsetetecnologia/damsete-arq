package br.com.damsete.arq.types;

/**
 * Created by andre on 05/05/2016.
 */
public class Email implements Type<String> {

    private static final long serialVersionUID = 1L;

    private String email;

    public Email() {
        super();
    }

    public Email(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return email;
    }

    public String getRawValue() {
        return email;
    }
}
