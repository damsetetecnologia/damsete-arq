package br.com.damsete.arq.types;

/**
 * Created by andre on 09/04/2016.
 */
public enum Gender {

    M("Masculino"),

    F("Feminino");

    private final String name;

    private Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return name();
    }
}