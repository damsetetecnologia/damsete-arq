package br.com.damsete.arq.types;

import static org.apache.commons.lang3.StringUtils.leftPad;

/**
 * Created by andre on 22/10/2016.
 */
public class NitPis implements Type<Long> {

    private static final long serialVersionUID = 1L;

    private Long nitPis;

    public NitPis() {
        super();
    }

    public NitPis(Long nitPis) {
        this.nitPis = nitPis;
    }

    @Override
    public String toString() {
        return format();
    }

    public Long getRawValue() {
        return nitPis;
    }

    public boolean isValid() {
        return isValid(false);
    }

    public boolean isValid(boolean nullable) {
        if (!nullable && nitPis == null) {
            return false;
        }

        if (nullable && nitPis == null) {
            return true;
        }

        if (nitPis != null && !isValid(unformat())) {
            return false;
        } else {
            return true;
        }
    }

    public String format() {
        if (nitPis == null) {
            return null;
        }

        String nitPisPadded = leftPad(nitPis.toString(), 11, '0');
        return String.format("%s.%s.%s-%s", nitPisPadded.substring(0, 3), nitPisPadded.substring(3, 7), nitPisPadded.substring(7, 10), nitPisPadded.substring(10));
    }

    public String unformat() {
        if (nitPis == null || format().trim().equals("")) {
            return null;
        }

        return format().replace(".", "").replace("-", "");
    }

    private boolean isValid(String nitPis) {
        if (nitPis.trim().length() != 11) {
            return false;
        }

        return true;
    }
}