package br.com.damsete.arq.types;

import static org.apache.commons.lang3.StringUtils.leftPad;

/**
 * Created by andre on 05/05/2016.
 */
public class Phone implements Type<Long> {

    private static final long serialVersionUID = 1L;

    private Long phone;

    public Phone() {
        super();
    }

    public Phone(Long phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return format();
    }

    public Long getRawValue() {
        return phone;
    }

    public String format() {
        if (phone == null) {
            return null;
        }

        String phonePadded = leftPad(phone.toString(), 11, '0');
        return String.format("(%s)%s-%s", phonePadded.substring(0, 2), phonePadded.substring(2, 7), phonePadded.substring(7, 11));
    }

    public String unformat() {
        if (phone == null || format().trim().equals("")) {
            return null;
        }

        return format().replace("(", "").replace(")", "").replace("-", "");
    }
}
