package br.com.damsete.arq.types;

import static org.apache.commons.lang3.StringUtils.leftPad;

/**
 * Created by andre on 22/10/2016.
 */
public class Rg implements Type<Long> {

    private static final long serialVersionUID = 1L;

    private Long rg;

    public Rg() {
        super();
    }

    public Rg(Long rg) {
        this.rg = rg;
    }

    @Override
    public String toString() {
        return format();
    }

    public Long getRawValue() {
        return rg;
    }

    public String format() {
        if (rg == null) {
            return null;
        }

        String rgPadded = leftPad(rg.toString(), 9, '0');
        return String.format("%s.%s.%s", rgPadded.substring(0, 3), rgPadded.substring(3, 6), rgPadded.substring(6, 9));
    }

    public String unformat() {
        if (rg == null || format().trim().equals("")) {
            return null;
        }

        return format().replace(".", "");
    }
}