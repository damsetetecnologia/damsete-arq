package br.com.damsete.arq.types;

import java.io.Serializable;

/**
 * Created by andre on 05/01/2018.
 */
public interface Type<T> extends Serializable {

    T getRawValue();
}
