package br.com.damsete.arq.types;

import static org.apache.commons.lang3.StringUtils.leftPad;

/**
 * Created by andre on 05/05/2016.
 */
public class ZipCode implements Type<Long> {

    private static final long serialVersionUID = 1L;

    private Long zipCode;

    public ZipCode() {
        super();
    }

    public ZipCode(Long zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return format();
    }

    public Long getRawValue() {
        return zipCode;
    }

    public String format() {
        if (zipCode == null) {
            return null;
        }

        String zipCodePadded = leftPad(zipCode.toString(), 8, '0');
        return String.format("%s-%s", zipCodePadded.substring(0, 5), zipCodePadded.substring(5, 8));
    }

    public String unformat() {
        if (zipCode == null || format().trim().equals("")) {
            return null;
        }

        return format().replace("-", "");
    }
}
