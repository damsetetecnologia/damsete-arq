package br.com.damsete.arq.utils;

import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

/**
 * Created by andre on 13/03/2017.
 */
public class Dates {

    public static String formatLocalDate(LocalDate localDate, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDate == null || StringUtils.isEmpty(pattern) ? null : formatter.format(localDate);
    }

    public static String formatLocalDate(LocalDateTime localDateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDateTime == null || StringUtils.isEmpty(pattern) ? null : formatter.format(localDateTime);
    }

    public static LocalDate parseLocalDate(String localDate, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return StringUtils.isEmpty(localDate) || StringUtils.isEmpty(pattern) ? null : LocalDate.parse(localDate, formatter);
    }

    public static int getCurrentSecond() {
        return LocalDateTime.now().getSecond();
    }

    public static int getCurrentMinute() {
        return LocalDateTime.now().getMinute();
    }

    public static int getCurrentHour() {
        return LocalDateTime.now().getHour();
    }

    public static int getCurrentDay() {
        return LocalDate.now().getDayOfMonth();
    }

    public static DayOfWeek getCurrentDayOfWeek() {
        return LocalDate.now().getDayOfWeek();
    }

    public static Month getCurrentMonth() {
        return LocalDate.now().getMonth();
    }

    public static int getCurrentYear() {
        return LocalDate.now().getYear();
    }

    public static LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    public static LocalDateTime getCurrentDateTime() {
        return LocalDateTime.now();
    }

    public static LocalDate firstMonthDate(Month month, int year) {
        return LocalDate.of(year, month, 1);
    }

    public static LocalDate lastMonthDate(Month month, int year) {
        return LocalDate.of(year, month, 1).with(TemporalAdjusters.lastDayOfMonth());
    }

    public static boolean isAfter(LocalDate a, LocalDate b) {
        if (a == null || b == null) {
            return false;
        }
        return a.isAfter(b);
    }

    public static boolean isBefore(LocalDate a, LocalDate b) {
        if (a == null || b == null) {
            return false;
        }
        return a.isBefore(b);
    }

    public static boolean isEqual(LocalDate a, LocalDate b) {
        if (a == null || b == null) {
            return false;
        }
        return a.isEqual(b);
    }

    public static Period getPeriod(LocalDate a, LocalDate b) {
        if (a == null || b == null) {
            return Period.ZERO;
        }
        return Period.between(a, b);
    }

    public static Duration getDuration(LocalDate a, LocalDate b) {
        if (a == null || b == null) {
            return Duration.ZERO;
        }
        return Duration.between(a, b);
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date asDate(LocalTime localTime) {
        if (localTime == null) {
            return null;
        }

        Instant instant = localTime.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant();
        BigInteger milis = BigInteger.valueOf(instant.getEpochSecond()).multiply(BigInteger.valueOf(1000));
        milis = milis.add(BigInteger.valueOf(instant.getNano()).divide(BigInteger.valueOf(1_000_000)));
        return new Date(milis.longValue());
    }

    public static Date asDate(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }

        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        if (date == null) {
            return null;
        }

        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime asLocalDateTime(Date date) {
        if (date == null) {
            return null;
        }

        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
