package br.com.damsete.arq.utils;

import java.util.Random;

/**
 * Created by andre on 19/06/2017.
 */
public class DigitGenerator {

    private static final Random RANDOM = new Random();

    public String generate(int amount) {
        final StringBuilder digits = new StringBuilder();
        for (int i = 0; i < amount; i++) {
            digits.append(RANDOM.nextInt(10));
        }
        return digits.toString();
    }
}
