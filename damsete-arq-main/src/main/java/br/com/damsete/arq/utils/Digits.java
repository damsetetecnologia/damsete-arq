package br.com.damsete.arq.utils;

import java.util.*;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.newLinkedList;
import static com.google.common.collect.Maps.newHashMap;

/**
 * Created by andre on 19/06/2017.
 */
public class Digits {

    private Map<Integer, String> substitutions = null;
    private List<Integer> multipliers = null;
    private List<Integer> numbers = null;
    private boolean sumIndividually;
    private boolean complement;
    private int module;

    public Digits(String patch) {
        this.multipliers = newArrayList();
        this.substitutions = newHashMap();
        this.numbers = newLinkedList();

        withMultipliers(2, 9);
        mod(11);

        char[] digits = patch.toCharArray();
        for (char digit : digits) {
            this.numbers.add(Character.getNumericValue(digit));
        }
        Collections.reverse(this.numbers);
    }

    public Digits withMultipliers(int begin, int end) {
        this.multipliers.clear();
        for (int i = begin; i <= end; i++) {
            this.multipliers.add(i);
        }
        return this;
    }

    public Digits withMultipliers(Integer... multipliersOrdered) {
        this.multipliers = Arrays.asList(multipliersOrdered);
        return this;
    }

    public Digits complementModule() {
        this.complement = true;
        return this;
    }

    public Digits swapIfFind(String substituto, Integer... i) {
        for (Integer integer : i) {
            this.substitutions.put(integer, substituto);
        }
        return this;
    }

    public Digits mod(int module) {
        this.module = module;
        return this;
    }

    public Digits sumIndividually() {
        this.sumIndividually = true;
        return this;
    }

    public String calculate() {
        int sum = 0;
        int nextMultiplier = 0;
        for (int digit : this.numbers) {
            int multiplier = multipliers.get(nextMultiplier);
            int total = digit * multiplier;
            sum += sumIndividually ? sumDigits(total) : total;
            nextMultiplier = nextMultiplier(nextMultiplier);
        }

        int result = sum % module;

        if (this.complement) {
            result = module - result;
        }

        if (this.substitutions.containsKey(result)) {
            return this.substitutions.get(result);
        }

        return String.valueOf(result);
    }


    private int sumDigits(int total) {
        return total / 10 + total % 10;
    }

    private int nextMultiplier(int multiplier) {
        multiplier++;
        if (multiplier == this.multipliers.size()) {
            multiplier = 0;
        }
        return multiplier;
    }

    public Digits addDigit(String digit) {
        ((LinkedList) this.numbers).addFirst(Integer.valueOf(digit));
        return this;
    }
}
