package br.com.damsete.arq.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

public class Filters {

    public static boolean isExtensionDiscarded(HttpServletRequest request, String extensionsDiscarded) {
        if (extensionsDiscarded != null) {
            List<String> extensions = Arrays.asList(extensionsDiscarded.split(","));
            String uri = request.getRequestURI();
            for (String extension : extensions) {
                if (uri.endsWith(extension)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isDirectoryDiscarded(HttpServletRequest request, String directoriesDiscarded) {
        if (directoriesDiscarded != null) {
            List<String> directories = Arrays.asList(directoriesDiscarded.split(","));
            String uri = request.getServletPath();
            for (String dir : directories) {
                if (uri.startsWith(dir)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isUrlDiscarded(HttpServletRequest request, String urlsDiscarded) {
        if (urlsDiscarded != null) {
            List<String> urls = Arrays.asList(urlsDiscarded.split(","));
            String uri = request.getRequestURL().toString();
            for (String url : urls) {
                if (uri.contains(url)) {
                    return true;
                }
            }
        }
        return false;
    }
}
