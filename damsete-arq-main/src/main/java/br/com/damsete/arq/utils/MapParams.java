package br.com.damsete.arq.utils;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapParams implements Map<String, Object>, Serializable {

    private static final long serialVersionUID = 1L;

    private Map<String, Object> paramsAsMap = null;

    public MapParams() {
        super();
        if (this.paramsAsMap == null) {
            this.paramsAsMap = new HashMap<String, Object>();
        }
    }

    public MapParams(Object... parametros) {
        insereParametros(parametros);
    }

    @Override
    public void clear() {
        this.paramsAsMap.clear();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.paramsAsMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.paramsAsMap.containsValue(value);
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return this.paramsAsMap.entrySet();
    }

    @Override
    public Object get(Object key) {
        return this.paramsAsMap.get(key);
    }

    public MapParams insereParametros(Object... parametros) {
        if (parametros.length % 2 != 0) {
            throw new IllegalArgumentException("Espera-se n\u00FAmero par de objetos: " + parametros.length);
        }

        if (this.paramsAsMap == null) {
            this.paramsAsMap = new HashMap<String, Object>(parametros.length / 2);
        }

        for (int i = 0; i < parametros.length; i += 2) {
            Object key = parametros[i];
            if (!(key instanceof String)) {
                throw new IllegalArgumentException("O par\u00E2metro '" + i + "' deveria ser uma String.");
            }
            Object value = (i + 1 < parametros.length ? parametros[i + 1] : null);
            this.paramsAsMap.put(key.toString(), value);
        }

        return this;
    }

    @Override
    public boolean isEmpty() {
        return this.paramsAsMap.isEmpty();
    }

    @Override
    public Set<String> keySet() {
        return this.paramsAsMap.keySet();
    }

    @Override
    public Object put(String key, Object value) {
        return this.paramsAsMap.put(key, value);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        this.paramsAsMap.putAll(m);
    }

    @Override
    public Object remove(Object key) {
        return this.paramsAsMap.remove(key);
    }

    @Override
    public int size() {
        return this.paramsAsMap.size();
    }

    @Override
    public String toString() {
        return this.paramsAsMap.toString();
    }

    @Override
    public Collection<Object> values() {
        return this.paramsAsMap.values();
    }
}
