package br.com.damsete.arq.utils;

import br.com.damsete.arq.exception.WrongConfigurationException;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * Created by andre on 16/04/2016.
 */
public class Networks {

    public static String getLocalMacAddress() {
        try {
            return getMacAddress(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            return null;
        }
    }

    public static String getMacAddress(InetAddress address) {
        try {
            String ip = address.getHostAddress();
            String loopBack = ip.substring(0, 3);
            if (loopBack.equals("127")) {
                ip = getFirstIP();
            }

            InetAddress localHost = InetAddress.getByName(ip);
            NetworkInterface netInter = NetworkInterface.getByInetAddress(localHost);
            byte[] macAddressBytes = netInter.getHardwareAddress();

            String macAddress = String.format("%1$02x-%2$02x-%3$02x-%4$02x-%5$02x-%6$02x",
                    macAddressBytes[0], macAddressBytes[1], macAddressBytes[2],
                    macAddressBytes[3], macAddressBytes[4], macAddressBytes[5])
                    .toUpperCase();

            return macAddress;
        } catch (SocketException e) {
            return null;
        } catch (UnknownHostException e) {
            return null;
        }
    }

    public static String getFirstIP() {
        Enumeration<NetworkInterface> ifaces = null;

        try {
            ifaces = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            return null;
        }

        if (ifaces != null) {
            while (ifaces.hasMoreElements()) {
                Enumeration<InetAddress> addrs = ifaces.nextElement().getInetAddresses();
                while (addrs.hasMoreElements()) {
                    InetAddress addr = addrs.nextElement();
                    if (!addr.isLoopbackAddress() && !(addr instanceof java.net.Inet6Address)) {
                        return addr.getHostAddress();
                    }
                }
            }
        }

        return "localhost";
    }

    public static String getLocalName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            return null;
        }
    }

    public static String getLocalAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            return null;
        }
    }

    public static InternetAddress newInternetAddress(String address, String name) {
        try {
            return new InternetAddress(address, name);
        } catch (UnsupportedEncodingException e) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable("network-exception-unsupported-encoding"));
        }
    }
}
