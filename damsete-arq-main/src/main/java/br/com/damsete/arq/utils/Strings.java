package br.com.damsete.arq.utils;

/**
 * Created by andre on 15/03/2017.
 */
public class Strings extends org.apache.commons.lang3.StringUtils {

    public static boolean hasAllRepeatedDigits(String cpf) {
        for (int i = 1; i < cpf.length(); i++) {
            if (cpf.charAt(i) != cpf.charAt(0)) {
                return false;
            }
        }
        return true;
    }
}
