package br.com.damsete.arq.vo;

import java.io.Serializable;

/**
 * Created by andre on 01/05/2018.
 */
public interface ValueObject<T> extends Serializable {

    boolean sameValueAs(T other);
}
