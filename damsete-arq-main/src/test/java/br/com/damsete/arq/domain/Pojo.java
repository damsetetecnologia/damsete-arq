package br.com.damsete.arq.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by andre on 24/06/2017.
 */
@Data
@Builder
public class Pojo {

    private Integer id = null;
    private int anInt = 0;
    private long aLong = 0;
    private double aDouble = 0;
    private String aString = null;
    private boolean aBoolean = true;
    private BigDecimal bigDecimal = null;
}
