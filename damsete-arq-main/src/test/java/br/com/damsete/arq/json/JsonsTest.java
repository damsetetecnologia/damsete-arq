package br.com.damsete.arq.json;

import br.com.damsete.arq.domain.Pojo;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * Created by andre on 23/06/2017.
 */
public class JsonsTest {

    @Test
    public void testJson() {
        Pojo pojoActual = Pojo.builder().id(1).aBoolean(true).aDouble(1).aLong(1).anInt(1).bigDecimal(BigDecimal.ONE).aString("string").build();

        String json = Jsons.toJson(pojoActual);

        Pojo pojoExpected = Jsons.fromJson(Pojo.class, json);

        Assert.assertEquals(pojoExpected, pojoActual);
    }
}
