package br.com.damsete.arq.types;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by andre on 15/06/2017.
 */
public class CnpjTest {

    @Test
    public void testToString() {
        Cnpj cnpj = new Cnpj(0L);
        Assert.assertTrue(cnpj.toString().length() > 0);
    }

    @Test
    public void testGetRawValue() {
        Cnpj cnpj = new Cnpj(0L);
        Assert.assertTrue(cnpj.getRawValue() == 0);
    }

    @Test
    public void testFormat() {
        Cnpj cnpj = new Cnpj(0L);
        Assert.assertTrue(cnpj.format().contains("."));
    }

    @Test
    public void testFormatNull() {
        Cnpj cnpj = new Cnpj(null);
        Assert.assertNull(cnpj.format());
    }

    @Test
    public void testUnformat() {
        Cnpj cnpj = new Cnpj(0L);
        Assert.assertTrue(!cnpj.unformat().contains("."));
    }

    @Test
    public void testUnformatNull() {
        Cnpj cnpj = new Cnpj(null);
        Assert.assertNull(cnpj.unformat());
    }

    @Test
    public void testIsValid() {
        Cnpj cnpj = new Cnpj(Long.valueOf("25094249000134"));
        Assert.assertTrue(cnpj.isValid());
    }

    @Test
    public void testIsInvalid() {
        Cnpj cnpj = new Cnpj(Long.valueOf("25094249000255"));
        Assert.assertTrue(!cnpj.isValid());
    }

    @Test
    public void testIsValidNullable() {
        Cnpj cnpj = new Cnpj(null);
        Assert.assertTrue(cnpj.isValid(true));
    }

    @Test
    public void testIsInvalidNullable() {
        Cnpj cnpj = new Cnpj(null);
        Assert.assertTrue(!cnpj.isValid(false));
    }

    @Test
    public void testIsInvalidNumber() {
        Cnpj cnpj = new Cnpj(99999999999999L);
        Assert.assertTrue(!cnpj.isValid());
    }

    @Test
    public void testIsInvalidNumberLength() {
        Cnpj cnpj = new Cnpj(999999999876543219L);
        Assert.assertTrue(!cnpj.isValid());
    }
}