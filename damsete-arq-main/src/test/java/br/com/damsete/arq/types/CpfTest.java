package br.com.damsete.arq.types;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by andre on 15/06/2017.
 */
public class CpfTest {

    @Test
    public void testToString() {
        Cpf cpf = new Cpf(0L);
        Assert.assertTrue(cpf.toString().length() > 0);
    }

    @Test
    public void testGetRawValue() {
        Cpf cpf = new Cpf(0L);
        Assert.assertTrue(cpf.getRawValue() == 0);
    }

    @Test
    public void testFormat() {
        Cpf cpf = new Cpf(0L);
        Assert.assertTrue(cpf.format().contains("."));
    }

    @Test
    public void testFormatNull() {
        Cpf cpf = new Cpf(null);
        Assert.assertNull(cpf.format());
    }

    @Test
    public void testUnformat() {
        Cpf cpf = new Cpf(0L);
        Assert.assertTrue(!cpf.unformat().contains("."));
    }

    @Test
    public void testUnformatNull() {
        Cpf cpf = new Cpf(null);
        Assert.assertNull(cpf.unformat());
    }

    @Test
    public void testIsValid() {
        Cpf cpf = new Cpf(Long.valueOf("05870773440"));
        Assert.assertTrue(cpf.isValid());
    }

    @Test
    public void testIsInvalid() {
        Cpf cpf = new Cpf(Long.valueOf("04568745988"));
        Assert.assertTrue(!cpf.isValid());
    }

    @Test
    public void testIsValidNullable() {
        Cpf cpf = new Cpf(null);
        Assert.assertTrue(cpf.isValid(true));
    }

    @Test
    public void testIsInvalidNullable() {
        Cpf cpf = new Cpf(null);
        Assert.assertTrue(!cpf.isValid(false));
    }

    @Test
    public void testIsInvalidNumber() {
        Cpf cpf = new Cpf(99999999999L);
        Assert.assertTrue(!cpf.isValid());
    }

    @Test
    public void testIsInvalidNumberLength() {
        Cpf cpf = new Cpf(999999999876543210L);
        Assert.assertTrue(!cpf.isValid());
    }
}
