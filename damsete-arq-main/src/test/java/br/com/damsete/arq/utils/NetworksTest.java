package br.com.damsete.arq.utils;

import org.junit.Assert;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by andre on 23/06/2017.
 */
public class NetworksTest {

    @Test
    public void testGetLocalMacAddress() {
        String mac = Networks.getLocalMacAddress();

        Assert.assertNotNull(mac);
        Assert.assertTrue(mac.contains("-"));
    }

    @Test
    public void testGetMacAddress() throws UnknownHostException {
        String localhostMac = Networks.getMacAddress(InetAddress.getLocalHost());

        Assert.assertNotNull(localhostMac);
        Assert.assertTrue(localhostMac.contains("-"));
    }

    @Test
    public void testGetFirstIP() {
        String firstIP = Networks.getFirstIP();

        Assert.assertNotNull(firstIP);
        Assert.assertTrue(firstIP.contains("."));
    }

    @Test
    public void testGetLocalName() {
        String localName = Networks.getLocalName();

        Assert.assertNotNull(localName);
    }

    @Test
    public void testGetLocalAddress() {
        String localAddress = Networks.getLocalAddress();

        Assert.assertNotNull(localAddress);
        Assert.assertTrue(localAddress.contains("."));
    }

    @Test
    public void testNewInternetAddress() {
        String ipAddress = Networks.getLocalAddress();
        InternetAddress internetAddress = Networks.newInternetAddress(ipAddress, "damsete");

        Assert.assertNotNull(internetAddress);
        Assert.assertEquals(internetAddress.getAddress(), ipAddress);
    }
}