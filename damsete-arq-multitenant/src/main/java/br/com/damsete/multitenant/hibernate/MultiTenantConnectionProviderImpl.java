package br.com.damsete.multitenant.hibernate;

import br.com.damsete.arq.log.core.Logger;
import br.com.damsete.multitenant.TenantContext;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class MultiTenantConnectionProviderImpl implements MultiTenantConnectionProvider {

    private static final long serialVersionUID = 1L;

    private transient DataSource dataSource;
    private transient Logger logger;

    @Autowired
    public MultiTenantConnectionProviderImpl(DataSource dataSource,
                                             Logger logger) {
        this.dataSource = dataSource;
        this.logger = logger;
    }

    @Override
    public Connection getAnyConnection() throws SQLException {
        return this.dataSource.getConnection();
    }

    @Override
    public void releaseAnyConnection(Connection connection) throws SQLException {
        connection.close();
    }

    @Override
    public Connection getConnection(String tenantIdentifier) throws SQLException {
        Connection connection = getAnyConnection();
        try (Statement statement = connection.createStatement()) {
            statement.execute(String.format("SET search_path TO %s;", tenantIdentifier));

            this.logger.debug("connection.getConnection >> " + tenantIdentifier);

            return connection;
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
            throw new SQLException("Could not alter JDBC connection to specified schema [" + tenantIdentifier + "]", e);
        }
    }

    @Override
    public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
        try (
                Connection connectionAux = connection;
                Statement statement = connectionAux.createStatement();
        ) {
            statement.execute(String.format("SET search_path TO %s;", TenantContext.DEFAULT_TENANT));

            this.logger.debug("connection.releaseConnection >> " + TenantContext.DEFAULT_TENANT);
        } catch (Exception e) {
            this.logger.error(e.getMessage(), e);
            throw new SQLException("Could not alter JDBC connection to specified schema [" + tenantIdentifier + "]", e);
        }
    }

    @Override
    public boolean isUnwrappableAs(Class unwrapType) {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> unwrapType) {
        return null;
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return true;
    }
}
