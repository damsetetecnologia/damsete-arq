package br.com.damsete.multitenant.interceptor;

import br.com.damsete.arq.utils.Filters;
import br.com.damsete.arq.utils.Strings;
import br.com.damsete.multitenant.TenantContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TenantInterceptor extends HandlerInterceptorAdapter {

    private static final String TENANT_HEADER = "X-TenantID";

    private String directoriesDiscarded;
    private String extensionsDiscarded;
    private String urlsDiscarded;
    private String urlTenant;

    public TenantInterceptor(String directoriesDiscarded,
                             String extensionsDiscarded,
                             String urlsDiscarded,
                             String urlTenant) {
        this.directoriesDiscarded = directoriesDiscarded;
        this.extensionsDiscarded = extensionsDiscarded;
        this.urlsDiscarded = urlsDiscarded;
        this.urlTenant = urlTenant;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean tenantSet = true;

        if (!Filters.isUrlDiscarded(request, this.urlsDiscarded)
                && !Filters.isDirectoryDiscarded(request, this.directoriesDiscarded)
                && !Filters.isExtensionDiscarded(request, this.extensionsDiscarded)) {
            String tenant = request.getHeader(TENANT_HEADER);
            if (Strings.isEmpty(tenant)) {
                tenant = (String) request.getSession().getAttribute(TENANT_HEADER);
            }

            if (Strings.isEmpty(tenant)) {
                tenantSet = false;

                response.sendRedirect(request.getContextPath() + this.urlTenant);
            } else {
                TenantContext.setCurrentTenant(tenant);
            }
        }

        return tenantSet;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        TenantContext.clear();
    }
}
