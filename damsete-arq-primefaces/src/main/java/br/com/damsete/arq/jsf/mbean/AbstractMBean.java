package br.com.damsete.arq.jsf.mbean;

import br.com.damsete.arq.log.core.Logger;
import br.com.damsete.arq.message.ArqMessageSource;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

public abstract class AbstractMBean implements Serializable {

    @Autowired
    protected transient ArqMessageSource messageSource = null;
    @Autowired
    protected transient Logger logger = null;

    protected String getOperationName(String operation) {
        return this.getClass().getSimpleName() + "." + operation;
    }
}
