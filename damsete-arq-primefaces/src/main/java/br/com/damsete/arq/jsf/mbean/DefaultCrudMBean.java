package br.com.damsete.arq.jsf.mbean;

import br.com.damsete.arq.data.application.CrudService;
import br.com.damsete.arq.data.domain.base.filter.Condition;
import br.com.damsete.arq.data.domain.base.filter.Filter;
import br.com.damsete.arq.data.domain.base.filter.Operator;
import br.com.damsete.arq.data.domain.model.filters.Filters;
import br.com.damsete.arq.data.domain.model.filters.FiltersList;
import br.com.damsete.arq.data.domain.model.filters.FiltersValue;
import br.com.damsete.arq.data.domain.model.globals.Globals;
import br.com.damsete.arq.data.domain.model.globals.GlobalsValue;
import br.com.damsete.arq.data.domain.model.orders.Orders;
import br.com.damsete.arq.data.domain.model.orders.OrdersList;
import br.com.damsete.arq.data.domain.model.orders.OrdersValue;
import br.com.damsete.arq.data.infrastructure.projection.IdProjection;
import br.com.damsete.arq.exception.ArqException;
import br.com.damsete.arq.exception.BusinessException;
import br.com.damsete.arq.exception.WrongConfigurationException;
import br.com.damsete.arq.jsf.table.AbstractLazyModel;
import br.com.damsete.arq.types.BrazilProvinces;
import br.com.damsete.arq.utils.Strings;
import com.google.common.collect.Iterables;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Persistable;
import org.springframework.data.domain.Sort;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static br.com.damsete.arq.jsf.utils.Operations.*;
import static br.com.damsete.arq.jsf.utils.Primefaces.*;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by andre on 13/07/2016.
 */
@Getter
@Setter
public class DefaultCrudMBean<T extends Persistable<ID>, ID extends Serializable> extends AbstractMBean implements CrudManagedBean<T, ID> {

    private static final long serialVersionUID = 1L;

    protected transient CrudService<T, ID> service = null;

    protected FiltersList filterList = new FiltersList();
    protected OrdersList orderList = new OrdersList();
    protected String searchCriteriaField = null;
    protected FiltersValue filtersValue = null;
    protected GlobalsValue globalsValue = null;
    protected OrdersValue ordersValue = null;
    protected LazyDataModel<T> data = null;
    protected List<Filter> filters = null;
    protected List<T> selectedData = null;
    protected ViewState viewState = null;
    protected T entity = null;

    public void initializeListing() throws ArqException {
        this.viewState = ViewState.LISTING;
        this.selectedData = newArrayList();
        populateList();
    }

    public void initializeForm(Long entityId) throws ArqException {
        if (entityId == null) {
            this.viewState = ViewState.ADDING;
            this.entity = newInstance();
        } else {
            this.viewState = ViewState.EDITING;
            Optional<T> obj = this.service.findById((ID) entityId);
            if (obj.isPresent()) {
                this.entity = obj.get();
            } else {
                this.viewState = ViewState.ADDING;
                this.entity = newInstance();
            }
        }
        populateForm();
    }

    public String changeToAdd() throws BusinessException {
        boolean error = true;
        try {
            checkActivePage();
            this.viewState = ViewState.ADDING;
            error = false;
            return "form" + getEntityName() + ".jsf?faces-redirect=true";
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public void changeToAddPopup() throws ArqException {
        boolean error = true;
        try {
            setEntity(newInstance());
            error = false;
            updateAndOpenDialog("addPopup" + getEntityName() + "Dialog", "dialogAddPopup" + getEntityName());
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public String changeToListing() {
        this.viewState = ViewState.LISTING;
        return "list" + getEntityName() + ".jsf?faces-redirect=true";
    }

    public String changeToEdit(ID entityId) throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            this.viewState = ViewState.EDITING;
            error = false;
            onBeforeChangeToEdit();
            return "form" + getEntityName() + ".jsf?faces-redirect=true&entityId=" + entityId;
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public void changeToDelete(ID entityId) throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            this.viewState = ViewState.DELETING;
            Optional<T> obj = this.service.findById(entityId);
            error = false;
            if (obj.isPresent()) {
                this.entity = obj.get();
                updateAndOpenDialog("delete" + getEntityName() + "Dialog", "dialogDelete" + getEntityName());
            } else {
                addInfo(updateDefaultMessagesGeneral(), this.messageSource.getMessage("generic.notfound.message"));
            }
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public void changeToDeleteAll() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            this.viewState = ViewState.DELETING;
            error = false;
            if (this.selectedData.isEmpty()) {
                addInfo(updateDefaultMessagesGeneral(), this.messageSource.getMessage("generic.notfound.message"));
            } else {
                updateAndOpenDialog("deleteAll" + getEntityName() + "Dialog", "dialogDeleteAll" + getEntityName());
            }
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public void changeToFilter() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            newFilterInstance();
            error = false;
            updateAndOpenDialog("filter" + getEntityName() + "Dialog", "dialogFilter" + getEntityName());
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public void changeToGlobal() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            newGlobalInstance();
            error = false;
            updateAndOpenDialog("global" + getEntityName() + "Dialog", "dialogGlobal" + getEntityName());
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public void changeToOrder() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            newOrderInstance();
            error = false;
            updateAndOpenDialog("order" + getEntityName() + "Dialog", "dialogOrder" + getEntityName());
        } finally {
            if (error) {
                updateDefaultMessages();
            }
        }
    }

    public String doCancel() {
        return "list" + getEntityName() + ".jsf?faces-redirect=true";
    }

    public void delete() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            prepareOperation(getOperationName("delete"));
            onBeforeDelete(this.entity);
            this.service.delete(this.entity);
            onAfterDelete(this.entity);
            addInfo(updateDefaultMessagesToDelete(), this.messageSource.getMessage("generic.deleted.message"));
            this.entity = newInstance();
            error = false;
        } finally {
            updateComponent(StringUtils.uncapitalize(getEntityName()) + "Form:" + StringUtils.uncapitalize(getEntityName()) + "List");
            closeDialog("dialogDelete" + getEntityName());
            if (error) {
                updateDefaultMessages();
            }
            removeOperation(getOperationName("delete"));
        }
    }

    public void deleteAll() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            prepareOperation(getOperationName("deleteAll"));
            this.service.deleteAll(this.selectedData);
            addInfo(updateDefaultMessagesToDelete(), this.messageSource.getMessage("generic.deleted.message"));
            this.entity = newInstance();
            this.selectedData = newArrayList();
            error = false;
        } finally {
            updateComponent(StringUtils.uncapitalize(getEntityName()) + "Form:" + StringUtils.uncapitalize(getEntityName()) + "List");
            closeDialog("dialogDeleteAll" + getEntityName());
            if (error) {
                updateDefaultMessages();
            }
            removeOperation(getOperationName("deleteAll"));
        }
    }

    public void save() throws ArqException {
        try {
            checkActivePage();
            prepareOperation(getOperationName("save"));
            onBeforeSave(this.entity);
            this.service.save(this.entity);
            onAfterSave(this.entity);
            addInfo(updateDefaultMessagesToSave(), getDefaultSaveMessage());
            this.entity = newInstance();
        } finally {
            removeOperation(getOperationName("save"));
            populateForm();
        }
    }

    public void savePopup() throws ArqException {
        try {
            prepareOperation(getOperationName("savePopup"));
            onBeforeSave(this.entity);
            this.service.save(this.entity);
            onAfterSave(this.entity);
            addInfo(true, getDefaultSaveMessage());
            this.entity = newInstance();
        } finally {
            removeOperation(getOperationName("savePopup"));
        }
    }

    public void update() throws ArqException {
        try {
            checkActivePage();
            prepareOperation(getOperationName("update"));
            Optional<T> obj = this.service.findById(this.entity.getId());
            if (obj.isPresent()) {
                onBeforeUpdate(this.entity);
                this.service.update(this.entity);
                onAfterUpdate(this.entity);
                addInfo(updateDefaultMessagesToUpdate(), this.messageSource.getMessage("generic.updated.message"));
            } else {
                addError(updateDefaultMessagesToUpdate(), this.messageSource.getMessage("generic.notfound.message"));
            }
        } finally {
            removeOperation(getOperationName("update"));
            populateForm();
        }
    }

    public void global() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            prepareOperation(getOperationName("global"));
            if (this.filters.isEmpty()) {
                this.globalsValue.setData(newArrayList(this.service.findAllProjectedBy(IdProjection.class)));
            } else {
                this.globalsValue.setData(newArrayList(this.service.findAllProjectedBy(this.filters, IdProjection.class)));
            }
            this.service.global(this.globalsValue);
            addInfo(true, this.messageSource.getMessage("generic.global.message"));
            error = false;
            newGlobalInstance();
        } finally {
            closeDialog("dialogGlobal" + Strings.capitalize(getEntityName()));
            if (error) {
                updateDefaultMessages();
            }
            removeOperation(getOperationName("global"));
        }
    }

    public void order() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            prepareOperation(getOperationName("order"));
            this.orderList.add(this.ordersValue);
            addInfo(true, this.messageSource.getMessage("generic.order.message"));
            error = false;
            newOrderInstance();
        } finally {
            if (error) {
                updateDefaultMessages();
            }
            removeOperation(getOperationName("order"));
        }
    }

    public void addFilter() throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            prepareOperation(getOperationName("addFilter"));
            this.filterList.add(this.filtersValue);
            addInfo(true, this.messageSource.getMessage("generic.filter-add.message"));
            updateComponent(StringUtils.uncapitalize(getEntityName()) + "Form:" + StringUtils.uncapitalize(getEntityName()) + "List");
            error = false;
            newFilterInstance();
        } finally {
            if (error) {
                updateDefaultMessages();
            }
            removeOperation(getOperationName("addFilter"));
        }
    }

    public void deleteFilter(FiltersValue filter) throws ArqException {
        boolean error = true;
        try {
            checkActivePage();
            prepareOperation(getOperationName("deleteFilter"));
            this.filterList.remove(filter);
            addInfo(true, this.messageSource.getMessage("generic.filter-delete.message"));
            updateComponent(StringUtils.uncapitalize(getEntityName()) + "Form:" + StringUtils.uncapitalize(getEntityName()) + "List");
            error = false;
            newFilterInstance();
        } finally {
            if (error) {
                updateDefaultMessages();
            }
            removeOperation(getOperationName("deleteFilter"));
        }
    }

    public void clearSearchCriteriaFilters() {
        setSearchCriteriaField(null);
        setOrderList(new OrdersList());
        setFilterList(new FiltersList());
        updateComponent("controlsForm");
        updateComponent(StringUtils.uncapitalize(getEntityName()) + "Form:" + StringUtils.uncapitalize(getEntityName()) + "List");
    }

    public List<SelectItem> getBrazilPronvinces() {
        List<SelectItem> items = newArrayList();
        Arrays.asList(BrazilProvinces.values()).forEach(i -> items.add(new SelectItem(i.getCode(), i.getCode())));
        items.sort((i1, i2) -> i1.getLabel().compareTo(i2.getLabel()));
        return items;
    }

    public Iterable<Operator> getOperators() {
        return Arrays.asList(Operator.values());
    }

    public Iterable<Condition> getConditions() {
        return Arrays.asList(Condition.values());
    }

    public Iterable<Filters> getFiltersList() {
        return this.service.findFilters(getEntityName());
    }

    public Iterable<Globals> getGlobalsList() {
        return this.service.findGlobals(getEntityName());
    }

    public Iterable<Orders> getOrdersList() {
        return this.service.findOrders(getEntityName());
    }

    @Override
    public void setService(CrudService<T, ID> service) {
        this.service = service;
    }

    protected String getDefaultSaveMessage() {
        return this.messageSource.getMessage("generic.created.message");
    }

    protected boolean updateDefaultMessagesToSave() {
        return updateDefaultMessagesGeneral();
    }

    protected boolean updateDefaultMessagesToUpdate() {
        return updateDefaultMessagesGeneral();
    }

    protected boolean updateDefaultMessagesToDelete() {
        return updateDefaultMessagesGeneral();
    }

    protected boolean updateDefaultMessagesGeneral() {
        return true;
    }

    protected void populateForm() throws ArqException {
    }

    protected void populateList() throws ArqException {
        this.data = new AbstractLazyModel<T, ID>() {
            @Override
            public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                Page<T> page = getPage(first, pageSize, sortField, sortOrder);
                Long totalSize = page.getTotalElements();
                List<T> data = page.getContent();
                setRowCount(totalSize.intValue());
                return data;
            }
        };
    }

    protected List<Filter> getSearchCriteriaFields() {
        return null;
    }

    protected String[] getSortField() {
        return null;
    }

    protected void onBeforeChangeToEdit() {
    }

    protected void onBeforeDelete(T entity) throws ArqException {
    }

    protected void onAfterDelete(T entity) throws ArqException {
    }

    protected void onBeforeSave(T entity) throws ArqException {
    }

    protected void onAfterSave(T entity) throws ArqException {
    }

    protected void onBeforeUpdate(T entity) throws ArqException {
    }

    protected void onAfterUpdate(T entity) throws ArqException {
    }

    protected String getEntityName() {
        return getEntityClass().getSimpleName().replace("Dto", "");
    }

    private Page<T> getPage(int first, int pageSize, String sortField, SortOrder sortOrder) {
        PageRequest pageRequest = null;

        if (Iterables.isEmpty(this.orderList.getOrdersValueList())) {
            String[] sortFieldFinal = getSortFieldFinal(sortField);
            if (sortFieldFinal != null) {
                pageRequest = PageRequest.of(first / pageSize, pageSize, getDirection(sortOrder), sortFieldFinal);
            } else {
                pageRequest = PageRequest.of(first / pageSize, pageSize);
            }
        } else {
            pageRequest = PageRequest.of(first / pageSize, pageSize, Sort.by(this.orderList.getOrders()));
        }

        this.filters = createSearchCriteriaFilters();
        return this.filters.isEmpty() ? service.findAll(pageRequest) : service.findAll(this.filters, pageRequest);
    }

    private Sort.Direction getDirection(SortOrder sortOrder) {
        return sortOrder.ordinal() == Sort.Direction.DESC.ordinal() ? Sort.Direction.DESC : Sort.Direction.ASC;
    }

    private String[] getSortFieldFinal(String sortField) {
        return getSortField() != null ? getSortField() : Strings.isNotEmpty(sortField) ? new String[]{sortField} : null;
    }

    private List<Filter> createSearchCriteriaFilters() {
        List<Filter> filters = newArrayList();

        if (getSearchCriteriaFields() != null) {
            filters.addAll(getSearchCriteriaFields());
        }

        if (!Iterables.isEmpty(this.filterList.getFilters())) {
            filters.addAll(this.filterList.getFilters());
        }

        return filters;
    }

    private void newFilterInstance() {
        this.filtersValue = new FiltersValue();
    }

    private void newGlobalInstance() {
        this.globalsValue = new GlobalsValue();
    }

    private void newOrderInstance() {
        this.ordersValue = new OrdersValue();
    }

    private T newInstance() throws WrongConfigurationException {
        try {
            return getEntityClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(new String[]{"exception.crudmbean.newinstance"}, new Object[]{getClass().getName()}));
        }
    }

    private Class<T> getEntityClass() {
        Type genericSuperclass = getClass().getGenericSuperclass();

        if (!(genericSuperclass instanceof ParameterizedType)) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(new String[]{"exception.crudmbean.invalidparameter"}, new Object[]{getClass().getName()}));
        }

        return (Class<T>) ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
    }

    protected enum ViewState {
        ADDING,
        LISTING,
        EDITING,
        DELETING;
    }
}