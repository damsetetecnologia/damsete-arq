package br.com.damsete.arq.jsf.mbean;

import br.com.damsete.arq.files.application.FileService;
import br.com.damsete.arq.files.application.dto.FileDto;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Faces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;

/**
 * Created by andre on 11/01/2018.
 */
@Controller
@Scope("request")
@Getter
@Setter
public class ImageMBean extends AbstractMBean {

    private static final long serialVersionUID = 1L;

    @Autowired
    private transient FileService fileService = null;

    public StreamedContent getImage() {
        if (Faces.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        } else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            String id = Faces.getExternalContext().getRequestParameterMap().get("idFileImage");

            FileDto image = this.fileService.getFile(Long.valueOf(id));
            if (image == null) {
                return new DefaultStreamedContent();
            }

            return new DefaultStreamedContent(new ByteArrayInputStream(image.getContent()));
        }
    }
}
