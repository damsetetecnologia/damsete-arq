package br.com.damsete.arq.jsf.mbean;

public enum ViewState {

    ADDING,
    LISTING,
    EDITING,
    DELETING;
}
