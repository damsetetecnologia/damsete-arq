package br.com.damsete.arq.jsf.table;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by andre on 04/04/2016.
 */
public class AbstractLazyModel<T extends Persistable<PK>, PK extends Serializable> extends LazyDataModel<T> {

    @Override
    public List<T> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        throw new IllegalStateException("Lazy loading not implemented");
    }

    @Override
    public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        throw new IllegalStateException("Lazy loading not implemented");
    }

    @Override
    public Object getRowKey(T object) {
        return object.getId();
    }

    @Override
    public T getRowData(String rowKey) {
        final Long key = Long.parseLong(rowKey);

        for (T t : getModelSource()) {
            if (t.getId().equals(key)) {
                return t;
            }
        }

        return null;
    }

    private List<T> getModelSource() {
        return (List<T>) this.getWrappedData();
    }
}
