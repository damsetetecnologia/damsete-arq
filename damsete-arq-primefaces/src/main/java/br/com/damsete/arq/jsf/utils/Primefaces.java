package br.com.damsete.arq.jsf.utils;

import br.com.damsete.arq.message.ArqMessageSource;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Sort;
import org.springframework.validation.FieldError;

import java.util.List;

/**
 * Created by andre on 28/07/2016.
 */
public class Primefaces {

    public static void updateComponent(String componentId) {
        PrimeFaces.current().ajax().update(componentId);
    }

    public static void executeScript(String script) {
        PrimeFaces.current().executeScript(script);
    }

    public static void openDialog(String widgetVar) {
        executeScript("PF('" + widgetVar + "').show()");
    }

    public static void updateAndOpenDialog(String id, String widgetVar) {
        updateComponent(id);
        executeScript("PF('" + widgetVar + "').show()");
    }

    public static void closeDialog(String widgetVar) {
        executeScript("PF('" + widgetVar + "').hide()");
    }

    public static void addInfo(String message, Object... parameters) {
        addInfo(true, message, parameters);
    }

    public static void addInfo(boolean updateDefault, String message, Object... parameters) {
        Messages.addInfo(null, message, parameters);
        if (updateDefault) {
            updateDefaultMessages();
        }
    }

    public static void addInfo(String componentId, String message, Object... parameters) {
        addInfo(true, componentId, message, parameters);
    }

    public static void addInfo(boolean updateDefault, String componentId, String message, Object... parameters) {
        Messages.addInfo(null, message, parameters);
        if (updateDefault) {
            updateMessages(componentId);
        }
    }

    public static void addError(String message, Object... parameters) {
        addError(true, message, parameters);
    }

    public static void addError(boolean updateDefault, String message, Object... parameters) {
        Messages.addError(null, message, parameters);
        if (updateDefault) {
            updateDefaultMessages();
        }
    }

    public static void addError(String componentId, String message, Object... parameters) {
        addError(true, componentId, message, parameters);
    }

    public static void addError(boolean updateDefault, String componentId, String message, Object... parameters) {
        Messages.addError(null, message, parameters);
        if (updateDefault) {
            updateMessages(componentId);
        }
    }

    public static void addError(List<FieldError> errors, ArqMessageSource messageSource) {
        addError(true, errors, messageSource);
    }

    public static void addError(boolean updateDefault, List<FieldError> errors, ArqMessageSource messageSource) {
        for (FieldError error : errors) {
            String message = error.getCodes()[error.getCodes().length - 1];
            String parameters = error.getDefaultMessage();
            Messages.addError(null, messageSource.getMessage(message), parameters);
        }
        if (updateDefault) {
            updateDefaultMessages();
        }
    }

    public static void addError(String componentId, List<FieldError> errors, ArqMessageSource messageSource) {
        addError(true, componentId, errors, messageSource);
    }

    public static void addError(boolean updateDefault, String componentId, List<FieldError> errors, ArqMessageSource messageSource) {
        for (FieldError error : errors) {
            String message = error.getCodes()[error.getCodes().length - 1];
            String parameters = error.getDefaultMessage();
            Messages.addError(null, messageSource.getMessage(message), parameters);
        }
        if (updateDefault) {
            updateMessages(componentId);
        }
    }

    public static void addWarning(String message, Object... parameters) {
        addWarning(true, message, parameters);
    }

    public static void addWarning(boolean updateDefault, String message, Object... parameters) {
        Messages.addWarn(null, message, parameters);
        if (updateDefault) {
            updateDefaultMessages();
        }
    }

    public static void addWarning(String componentId, String message, Object... parameters) {
        addWarning(true, componentId, message, parameters);
    }

    public static void addWarning(boolean updateDefault, String componentId, String message, Object... parameters) {
        Messages.addWarn(null, message, parameters);
        if (updateDefault) {
            updateMessages(componentId);
        }
    }

    public static void updateDefaultMessages() {
        if (getDefaultMessagesComponentId() != null && !getDefaultMessagesComponentId().isEmpty()) {
            temporizeHiding(getDefaultMessagesComponentId());
        }
    }

    public static void updateMessages(String componentId) {
        if (componentId != null && !componentId.isEmpty()) {
            temporizeHiding(componentId);
        }
    }

    public static void temporizeHiding(String componentId) {
        updateComponent(componentId);
        executeScript("setTimeout(\"$(\'#" + componentId + "\').slideUp(300)\", 8000)");
    }

    public static String getDefaultMessagesComponentId() {
        return "messages";
    }

    public static Sort.Direction getDirection(SortOrder sortOrder) {
        return sortOrder.ordinal() == Sort.Direction.DESC.ordinal() ? Sort.Direction.DESC : Sort.Direction.ASC;
    }
}
