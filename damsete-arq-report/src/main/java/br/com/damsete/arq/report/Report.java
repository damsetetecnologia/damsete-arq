package br.com.damsete.arq.report;

import br.com.damsete.arq.exception.MissingConfigurationException;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * Created by andre on 01/06/2016.
 */
public interface Report extends Serializable {

    Object getSource();

    Object getReportObject();

    String getPath();

    void prepare(Collection<?> dataSource, Map<String, Object> param);

    byte[] export(Collection<?> dataSource, Map<String, Object> param, TypeReport type) throws MissingConfigurationException;

    byte[] export(TypeReport type) throws MissingConfigurationException;
}
