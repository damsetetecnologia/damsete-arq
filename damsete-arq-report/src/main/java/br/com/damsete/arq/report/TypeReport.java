package br.com.damsete.arq.report;

import br.com.damsete.arq.exception.WrongConfigurationException;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import static br.com.damsete.arq.utils.Strings.isEmpty;

/**
 * Created by andre on 01/06/2016.
 */
public enum TypeReport {

    CSV, HTML, ODT, PDF, RTF, TXT, XLS, ODS;

    public static TypeReport valueTo(String alias) {
        if (isEmpty(alias)) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable("report-exception-report-type-null"));
        }
        TypeReport[] types = TypeReport.values();
        for (TypeReport type : types) {
            if (type.name().equalsIgnoreCase(alias.toUpperCase())) {
                return type;
            }
        }
        throw new WrongConfigurationException(new DefaultMessageSourceResolvable(new String[]{"report-exception-report-type-not-supported"}, new Object[]{alias}));
    }
}
