package br.com.damsete.arq.report.annotation;

import java.lang.annotation.*;

/**
 * Created by andre on 01/06/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Inherited
public @interface Path {

    String value();
}
