package br.com.damsete.arq.report.exporter;

import br.com.damsete.arq.exception.MissingConfigurationException;
import br.com.damsete.arq.report.TypeReport;
import br.com.damsete.arq.report.exporter.chain.*;
import net.sf.jasperreports.engine.JasperPrint;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 01/06/2016.
 */
public class JasperReportsExporter {

    public static final String NON_COMPILED_REPORT_EXTENSION = ".jrxml";

    public static final String COMPILED_REPORT_EXTENSION = ".jasper";

    public static ByteArrayOutputStream export(TypeReport type, JasperPrint print) throws MissingConfigurationException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        JRExporterChain exporterChain = new JRPdfExporterChain();
        exporterChain.setNext(new JRXlsExporterChain());
        exporterChain.setNext(new JRCsvExporterChain());
        exporterChain.setNext(new JRTextExporterChain());
        exporterChain.setNext(new JRHtmlExporterChain());
        exporterChain.setNext(new JROdsExporterChain());
        exporterChain.setNext(new JROdtExporterChain());
        exporterChain.setNext(new JRRtfExporterChain());

        exporterChain.exportReport(type, print, outputStream);

        return outputStream;
    }

}
