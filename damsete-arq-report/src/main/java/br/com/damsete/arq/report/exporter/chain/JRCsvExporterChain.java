package br.com.damsete.arq.report.exporter.chain;

import br.com.damsete.arq.report.TypeReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 13/06/2017.
 */
public class JRCsvExporterChain extends JRExporterChain {

    public JRCsvExporterChain() {
        super(TypeReport.CSV);
    }

    @Override
    protected void exportReport(JasperPrint print, ByteArrayOutputStream outputStream) throws JRException {
        JRCsvExporter exporterCSV = new JRCsvExporter();
        exporterCSV.setExporterInput(new SimpleExporterInput(print));
        exporterCSV.setExporterOutput(new SimpleWriterExporterOutput(outputStream));
        exporterCSV.exportReport();
    }
}
