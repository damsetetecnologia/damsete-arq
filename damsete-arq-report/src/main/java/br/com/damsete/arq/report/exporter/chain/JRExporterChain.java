package br.com.damsete.arq.report.exporter.chain;

import br.com.damsete.arq.exception.MissingConfigurationException;
import br.com.damsete.arq.log.core.LoggerAPI;
import br.com.damsete.arq.report.TypeReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 13/06/2017.
 */
public abstract class JRExporterChain {

    protected JRExporterChain next = null;
    protected TypeReport typeReport = null;

    public JRExporterChain(TypeReport typeReport) {
        this.next = null;
        this.typeReport = typeReport;
    }

    public void setNext(JRExporterChain chain) {
        if (this.next == null) {
            this.next = chain;
        } else {
            this.next.setNext(chain);
        }
    }

    public void exportReport(TypeReport typeReport, JasperPrint print, ByteArrayOutputStream outputStream) throws MissingConfigurationException {
        try {
            if (canExportReport(typeReport)) {
                exportReport(print, outputStream);
            } else {
                if (this.next == null) {
                    throw new MissingConfigurationException(new DefaultMessageSourceResolvable(new String[]{"report-exception-reportimpl-not-found"}, new Object[]{typeReport.name()}));
                }
                this.next.exportReport(typeReport, print, outputStream);
            }
        } catch (JRException e) {
            LoggerAPI.error(e.getMessage(), e);
            throw new MissingConfigurationException(new DefaultMessageSourceResolvable(new String[]{"report-exception-generating-report"}, new Object[]{typeReport.name()}));
        }
    }

    private boolean canExportReport(TypeReport typeReport) {
        return this.typeReport.equals(typeReport);
    }

    protected abstract void exportReport(JasperPrint print, ByteArrayOutputStream outputStream) throws JRException;
}
