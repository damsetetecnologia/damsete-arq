package br.com.damsete.arq.report.exporter.chain;

import br.com.damsete.arq.report.TypeReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 13/06/2017.
 */
public class JRHtmlExporterChain extends JRExporterChain {

    public JRHtmlExporterChain() {
        super(TypeReport.HTML);
    }

    @Override
    protected void exportReport(JasperPrint print, ByteArrayOutputStream outputStream) throws JRException {
        HtmlExporter exporterHTML = new HtmlExporter();
        exporterHTML.setExporterInput(new SimpleExporterInput(print));
        exporterHTML.setExporterOutput(new SimpleHtmlExporterOutput(outputStream));
        exporterHTML.exportReport();
    }
}
