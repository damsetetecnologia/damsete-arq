package br.com.damsete.arq.report.exporter.chain;

import br.com.damsete.arq.report.TypeReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 13/06/2017.
 */
public class JROdtExporterChain extends JRExporterChain {

    public JROdtExporterChain() {
        super(TypeReport.ODT);
    }

    @Override
    protected void exportReport(JasperPrint print, ByteArrayOutputStream outputStream) throws JRException {
        JROdtExporter exporterODT = new JROdtExporter();
        exporterODT.setExporterInput(new SimpleExporterInput(print));
        exporterODT.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
        exporterODT.exportReport();
    }
}
