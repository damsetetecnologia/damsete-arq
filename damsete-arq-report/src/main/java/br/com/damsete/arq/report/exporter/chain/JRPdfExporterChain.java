package br.com.damsete.arq.report.exporter.chain;

import br.com.damsete.arq.report.TypeReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 13/06/2017.
 */
public class JRPdfExporterChain extends JRExporterChain {

    public JRPdfExporterChain() {
        super(TypeReport.PDF);
    }

    @Override
    protected void exportReport(JasperPrint print, ByteArrayOutputStream outputStream) throws JRException {
        JRPdfExporter exporterPDF = new JRPdfExporter();
        exporterPDF.setExporterInput(new SimpleExporterInput(print));
        exporterPDF.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
        exporterPDF.exportReport();
    }
}
