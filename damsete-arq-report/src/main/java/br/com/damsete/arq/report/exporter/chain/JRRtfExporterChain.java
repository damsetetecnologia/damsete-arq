package br.com.damsete.arq.report.exporter.chain;

import br.com.damsete.arq.report.TypeReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 13/06/2017.
 */
public class JRRtfExporterChain extends JRExporterChain {

    public JRRtfExporterChain() {
        super(TypeReport.RTF);
    }

    @Override
    protected void exportReport(JasperPrint print, ByteArrayOutputStream outputStream) throws JRException {
        JRRtfExporter exporterRTF = new JRRtfExporter();
        exporterRTF.setExporterInput(new SimpleExporterInput(print));
        exporterRTF.setExporterOutput(new SimpleWriterExporterOutput(outputStream));
        exporterRTF.exportReport();
    }
}
