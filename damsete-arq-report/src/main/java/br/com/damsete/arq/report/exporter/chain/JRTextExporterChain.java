package br.com.damsete.arq.report.exporter.chain;

import br.com.damsete.arq.report.TypeReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

import java.io.ByteArrayOutputStream;

/**
 * Created by andre on 13/06/2017.
 */
public class JRTextExporterChain extends JRExporterChain {

    public JRTextExporterChain() {
        super(TypeReport.TXT);
    }

    @Override
    protected void exportReport(JasperPrint print, ByteArrayOutputStream outputStream) throws JRException {
        JRTextExporter exporterTXT = new JRTextExporter();
        exporterTXT.setExporterInput(new SimpleExporterInput(print));
        exporterTXT.setExporterOutput(new SimpleWriterExporterOutput(outputStream));
        exporterTXT.exportReport();
    }
}
