package br.com.damsete.arq.report.impl;

import br.com.damsete.arq.exception.MissingConfigurationException;
import br.com.damsete.arq.exception.WrongConfigurationException;
import br.com.damsete.arq.log.core.LoggerAPI;
import br.com.damsete.arq.report.Report;
import br.com.damsete.arq.report.TypeReport;
import br.com.damsete.arq.report.exporter.JasperReportsExporter;
import br.com.damsete.arq.report.utils.Reflections;
import br.com.damsete.arq.utils.Strings;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.context.support.DefaultMessageSourceResolvable;

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

/**
 * Created by andre on 01/06/2016.
 */
public class ReportImpl implements Report {

    private JasperReport jasper = null;
    private JasperPrint print = null;
    private String path = null;

    public ReportImpl(String path) {
        this.path = path;
    }

    @Override
    public Object getSource() {
        loadReport();
        return this.jasper;
    }

    @Override
    public Object getReportObject() {
        return print;
    }

    private InputStream getReportStream(String relativePath) {
        InputStream reportStream;

        try {
            ClassLoader classLoader = Reflections.getClassLoaderForResource(relativePath);
            reportStream = classLoader.getResourceAsStream(relativePath);

        } catch (Exception cause) {
            reportStream = null;
        }

        if (reportStream == null) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable("report-exception-file-not-found"));
        }

        return reportStream;
    }

    private void loadJRXML(String relativePath) {
        try {
            this.jasper = JasperCompileManager.compileReport(getReportStream(relativePath));
        } catch (Throwable e) {
            LoggerAPI.error(e.getMessage(), e);
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(new String[]{"report-exception-compiling-jrxml-file"}, new Object[]{this.path}));
        }
    }

    private void loadJasper(String relativePath) {
        try {
            this.jasper = (JasperReport) JRLoader.loadObject(getReportStream(relativePath));
        } catch (JRException e) {
            LoggerAPI.error(e.getMessage(), e);
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable(new String[]{"report-exception-loading-jasper-file"}, new Object[]{this.path}));
        }
    }

    private final void loadReport() {
        if (Strings.isEmpty(this.path)) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable("report-exception-file-not-found"));
        }

        if (this.jasper == null) {
            if (this.path.endsWith(JasperReportsExporter.COMPILED_REPORT_EXTENSION)) {
                loadJasper(this.path);

            } else if (this.path.endsWith(JasperReportsExporter.NON_COMPILED_REPORT_EXTENSION)) {
                try {
                    String jasperPath = this.path.replaceAll(JasperReportsExporter.NON_COMPILED_REPORT_EXTENSION,
                            JasperReportsExporter.COMPILED_REPORT_EXTENSION);
                    loadJasper(jasperPath);
                } catch (Exception e) {
                    loadJRXML(this.path);
                }
            } else {
                throw new WrongConfigurationException(new DefaultMessageSourceResolvable(new String[]{"report-exception-extension-not-valid"}, new Object[]{this.path}));
            }
        }
    }

    @Override
    public void prepare(Collection<?> dataSource, Map<String, Object> param) {
        loadReport();
        try {
            this.print = JasperFillManager.fillReport(this.jasper, param, new JRBeanCollectionDataSource(dataSource));
        } catch (JRException e) {
            LoggerAPI.error(e.getMessage(), e);
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable("report-exception-filling-report-problem"));
        }
    }

    public Boolean isFilled() {
        return print != null;
    }

    public byte[] export(Collection<?> dataSource, Map<String, Object> param, TypeReport type) throws MissingConfigurationException {
        prepare(dataSource, param);
        return export(type);
    }

    public byte[] export(TypeReport type) throws MissingConfigurationException {
        if (!isFilled()) {
            throw new WrongConfigurationException(new DefaultMessageSourceResolvable("report-exception-report-not-filled"));
        }
        return JasperReportsExporter.export(type, this.print).toByteArray();
    }

    @Override
    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
