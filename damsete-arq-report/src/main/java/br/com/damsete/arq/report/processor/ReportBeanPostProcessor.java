package br.com.damsete.arq.report.processor;

import br.com.damsete.arq.report.annotation.Path;
import br.com.damsete.arq.report.exporter.JasperReportsExporter;
import br.com.damsete.arq.report.impl.ReportImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * Created by andre on 01/06/2016.
 */
@Component
public class ReportBeanPostProcessor implements MergedBeanDefinitionPostProcessor {

    private static final String DEFAULT_PATH = "reports/";

    @Override
    public void postProcessMergedBeanDefinition(RootBeanDefinition rootBeanDefinition, Class beanType, String beanName) {
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.getAnnotation(Path.class) != null) {
                String path = field.getAnnotation(Path.class).value();
                if (path == null) {
                    path = DEFAULT_PATH + field.getName() + JasperReportsExporter.COMPILED_REPORT_EXTENSION;
                }
                createReport(bean, field, path);
            }
        }
        return bean;
    }

    private void createReport(Object bean, Field field, String path) {
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, bean, new ReportImpl(path));
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
