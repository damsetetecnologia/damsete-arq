package br.com.damsete.arq.report.utils;

import java.net.URL;

/**
 * Created by andre on 01/06/2016.
 */
public class Reflections {

    public Reflections() {
        throw new UnsupportedOperationException();
    }

    public static ClassLoader getClassLoaderForClass(final String canonicalName) {
        return Reflections.getClassLoaderForResource(canonicalName.replaceAll("\\.", "/") + ".class");
    }

    public static ClassLoader getClassLoaderForResource(final String resource) {
        final String stripped = resource.charAt(0) == '/' ? resource.substring(1) : resource;

        URL url = null;
        ClassLoader result = Thread.currentThread().getContextClassLoader();

        if (result != null) {
            url = result.getResource(stripped);
        }

        if (url == null) {
            result = Reflections.class.getClassLoader();
            url = Reflections.class.getClassLoader().getResource(stripped);
        }

        if (url == null) {
            result = null;
        }

        return result;
    }
}
