package br.com.damsete.arq.report;

import br.com.damsete.arq.exception.WrongConfigurationException;
import br.com.damsete.arq.report.exporter.JasperReportsExporter;
import br.com.damsete.arq.report.impl.ReportImpl;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.util.JRLoader;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by andre on 24/06/2017.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({JRLoader.class, JasperFillManager.class, JasperReportsExporter.class, JasperCompileManager.class})
public class ReportImplTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private Report report = null;

    @Test(expected = WrongConfigurationException.class)
    public void testReportWithWrongExtension() {
        this.report = new ReportImpl("readme.txt");
        this.report.getSource();

        this.exception.expect(WrongConfigurationException.class);
    }
}
