package br.com.damsete.arq.report;

import br.com.damsete.arq.exception.MissingConfigurationException;
import br.com.damsete.arq.report.impl.ReportImpl;
import br.com.damsete.arq.report.service.PersonService;
import org.junit.Assert;
import org.junit.Test;

import static com.google.common.collect.Maps.newHashMap;

/**
 * Created by andre on 24/06/2017.
 */
public class ReportIntegrationTest {

    private PersonService personService = new PersonService();

    private Report report = new ReportImpl("report/Person.jrxml");

    @Test
    public void testIntegration() throws MissingConfigurationException {
        byte[] relbytes = this.report.export(this.personService.findAll(), newHashMap(), TypeReport.PDF);
        Assert.assertTrue(relbytes != null);
        Assert.assertTrue(relbytes.length > 0);
    }
}
