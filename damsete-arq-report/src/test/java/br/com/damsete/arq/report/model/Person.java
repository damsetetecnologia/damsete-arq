package br.com.damsete.arq.report.model;

import lombok.Data;

/**
 * Created by andre on 24/06/2017.
 */
@Data
public class Person {

    private String name = null;
    private int age = 0;
    private String address = null;
    private String email = null;
    private String cpf = null;
    private String rg = null;
}
