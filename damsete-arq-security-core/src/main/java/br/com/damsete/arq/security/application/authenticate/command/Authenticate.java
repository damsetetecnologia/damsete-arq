package br.com.damsete.arq.security.application.authenticate.command;

import br.com.damsete.arq.security.application.authenticate.dto.MultiTenant;
import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Authenticate implements Serializable {

    private static final long serialVersionUID = 1L;

    private String applicationName = null;
    private MultiTenant tenant = null;
    private String username = null;
    private String password = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
