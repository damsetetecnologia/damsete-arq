package br.com.damsete.arq.security.application.authenticate.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by andre on 17/10/2017.
 */
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Authorities implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Authority> authorities = null;
}
