package br.com.damsete.arq.security.application.authenticate.dto;

import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * Created by andre on 25/04/2016.
 */
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Credential implements UserDetails {

    private static final long serialVersionUID = 1L;

    private Long id = null;
    private Long idPhoto = null;
    private String name = null;
    private String token = null;
    private String username = null;
    private String password = null;
    private String email = null;
    private boolean enabled = true;
    private boolean denied = false;
    private MultiTenant multiTenant = null;
    private List<Authority> authorities = null;

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass().equals(this.getClass())) {
            Credential otherObj = (Credential) obj;
            return new EqualsBuilder().append(getId(), otherObj.getId()).isEquals();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getClass()).append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public boolean isWithPhoto() {
        return this.idPhoto != null && this.idPhoto > 0;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
}
