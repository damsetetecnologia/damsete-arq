package br.com.damsete.arq.security.application.authenticate.dto;

import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Created by andre on 16/10/2017.
 */
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MultiTenant implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id = null;
    private String applicationName = null;
    private String name = null;
    private String schema = null;
    private boolean enabled = true;

    public String getLabel() {
        return String.format("%s - %s", getApplicationName(), getName());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj.getClass().equals(this.getClass())) {
            MultiTenant otherObj = (MultiTenant) obj;
            return new EqualsBuilder().append(getId(), otherObj.getId()).isEquals();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getClass()).append(getId()).toHashCode();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
