package br.com.damsete.arq.security.application.authenticate.dto;

import lombok.*;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by andre on 16/10/2017.
 */
@Data
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MultiTenants implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<MultiTenant> multiTenants = null;

    public boolean isUnique() {
        return CollectionUtils.size(this.multiTenants) == 1;
    }
}
