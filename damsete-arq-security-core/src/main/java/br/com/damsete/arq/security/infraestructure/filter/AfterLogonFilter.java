package br.com.damsete.arq.security.infraestructure.filter;

import org.springframework.security.core.Authentication;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface AfterLogonFilter {

    void processAfterLogon(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException;

    int getOrder();
}