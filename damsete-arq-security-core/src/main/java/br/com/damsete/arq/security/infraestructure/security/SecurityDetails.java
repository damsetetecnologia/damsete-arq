package br.com.damsete.arq.security.infraestructure.security;

import br.com.damsete.arq.security.application.authenticate.dto.Credential;
import br.com.damsete.arq.security.application.authenticate.dto.MultiTenant;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityDetails {

    private static final String ANONYMOUS_USER = "anonymousUser";

    public Credential getCredential() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && !authentication.getPrincipal().equals(ANONYMOUS_USER)) {
            return (Credential) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        return null;
    }

    public boolean isCredentialed() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null || !authentication.getPrincipal().equals(ANONYMOUS_USER);
    }

    public MultiTenant getTenant() {
        return getCredential() != null ? getCredential().getMultiTenant() : null;
    }

    public boolean isWithTenant() {
        return getTenant() != null;
    }
}
