package br.com.damsete.arq.security.application;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.api.ApiError;
import br.com.damsete.arq.security.application.authenticate.command.Authenticate;
import br.com.damsete.arq.security.application.authenticate.dto.Credential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional(rollbackFor = Exception.class)
public class AuthenticateService {

    @Value("${damsete.authentication.api-url}")
    private String apiUrl = null;
    private RestTemplate restTemplate;

    @Autowired
    public AuthenticateService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @MethodLoggable
    public Credential authenticate(Authenticate authenticate) {
        try {
            String resourceUrl = this.apiUrl + "/authenticate";
            return this.restTemplate.postForObject(resourceUrl, authenticate, Credential.class);
        } catch (RestClientResponseException e) {
            ApiError apiError = ApiError.getApiError(e);
            throw new AuthenticationServiceException(apiError.getMessage());
        }
    }
}
