package br.com.damsete.arq.security.application;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.api.ApiError;
import br.com.damsete.arq.security.application.authenticate.command.Authenticate;
import br.com.damsete.arq.security.application.authenticate.dto.Credential;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional(rollbackFor = Exception.class)
public class AuthenticateTenantService {

    @Value("${damsete.authentication-tenant.api-url}")
    private String apiUrl = null;
    private RestTemplate restTemplate;

    @Autowired
    public AuthenticateTenantService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @MethodLoggable
    public Credential authenticateByTenant(Authenticate authenticate) {
        try {
            String resourceUrl = this.apiUrl + "/authenticate-tenant";


            HttpHeaders httpHeaders = new HttpHeaders() {{
                set("Authorization", authenticate.getPassword());
            }};

            HttpEntity<Authenticate> requestEntity = new HttpEntity<>(authenticate, httpHeaders);

            HttpEntity<Credential> response = this.restTemplate.exchange(resourceUrl, HttpMethod.POST, requestEntity, Credential.class);

            return response.getBody();
        } catch (RestClientResponseException e) {
            ApiError apiError = ApiError.getApiError(e);
            throw new AuthenticationServiceException(apiError.getMessage());
        }
    }
}
