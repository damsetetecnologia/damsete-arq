package br.com.damsete.arq.security.application;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.security.application.profile.command.UpdateProfile;
import br.com.damsete.arq.utils.MapParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
@Transactional(rollbackFor = Exception.class)
public class ProfileService {

    @Value("${damsete.profile.api-url}")
    private String apiUrl = null;
    private RestTemplate restTemplate;

    @Autowired
    public ProfileService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @MethodLoggable
    public void update(UpdateProfile updateProfile) {
        String resourceUrl = this.apiUrl + "/profile/{id}";

        HttpHeaders httpHeaders = new HttpHeaders() {{
            set("Authorization", updateProfile.getToken());
        }};

        HttpEntity<UpdateProfile> requestEntity = new HttpEntity<>(updateProfile, httpHeaders);

        this.restTemplate.exchange(resourceUrl, HttpMethod.PUT, requestEntity, UpdateProfile.class, new MapParams("id", updateProfile.getId()));
    }
}
