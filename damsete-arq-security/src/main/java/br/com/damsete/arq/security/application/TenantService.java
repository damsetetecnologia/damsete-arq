package br.com.damsete.arq.security.application;

import br.com.damsete.arq.annotation.MethodLoggable;
import br.com.damsete.arq.security.application.authenticate.dto.MultiTenant;
import br.com.damsete.arq.security.application.authenticate.dto.MultiTenants;
import br.com.damsete.arq.utils.MapParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.stream.Collectors;

import static com.google.common.collect.Maps.newHashMap;

@Service
@Transactional(rollbackFor = Exception.class)
public class TenantService {

    @Value("${damsete.tenant.api-url}")
    private String apiUrl = null;
    private RestTemplate restTemplate;

    @Autowired
    public TenantService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @MethodLoggable
    public MultiTenants getMultiTenants(String applicationName, String username, String token) {
        String resourceUrl = this.apiUrl + "/tenant?%s";

        Map<String, Object> params = newHashMap();
        params.put("applicationName", applicationName);
        params.put("username", username);

        String parametrizedArgs = params.keySet().stream().map(k -> String.format("%s={%s}", k, k)).collect(Collectors.joining("&"));

        HttpHeaders httpHeaders = new HttpHeaders() {{
            set("Authorization", token);
        }};

        HttpEntity requestEntity = new HttpEntity<>(httpHeaders);

        HttpEntity<MultiTenants> response = this.restTemplate.exchange(String.format(resourceUrl, parametrizedArgs), HttpMethod.GET, requestEntity,
                MultiTenants.class, params);

        return response.getBody();
    }

    @MethodLoggable
    public MultiTenant findTenantById(Long tenantId, String token) {
        String resourceUrl = this.apiUrl + "/tenant/{id}";

        HttpHeaders httpHeaders = new HttpHeaders() {{
            set("Authorization", token);
        }};

        HttpEntity requestEntity = new HttpEntity<>(httpHeaders);

        HttpEntity<MultiTenant> response = this.restTemplate.exchange(resourceUrl, HttpMethod.GET, requestEntity,
                MultiTenant.class, new MapParams("id", tenantId));

        return response.getBody();
    }
}
