package br.com.damsete.arq.security.infraestructure.audit;

import br.com.damsete.arq.security.application.authenticate.dto.Credential;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class CredentialAuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        Credential credential = (Credential) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (credential == null) {
            return Optional.of("public");
        }

        return Optional.of(credential.getUsername());
    }
}
