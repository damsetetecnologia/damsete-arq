package br.com.damsete.arq.security.infraestructure.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

@Component
public class AfterLogonFilters extends SavedRequestAwareAuthenticationSuccessHandler {

    private List<AfterLogonFilter> filters;

    @Autowired(required = false)
    public AfterLogonFilters(List<AfterLogonFilter> filters) {
        this.filters = filters;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        if (!isEmpty(this.filters)) {
            this.filters.sort(Comparator.comparingInt(AfterLogonFilter::getOrder));

            for (AfterLogonFilter it : this.filters) {
                it.processAfterLogon(request, response, authentication);
            }
        }

        super.onAuthenticationSuccess(request, response, authentication);
    }
}
