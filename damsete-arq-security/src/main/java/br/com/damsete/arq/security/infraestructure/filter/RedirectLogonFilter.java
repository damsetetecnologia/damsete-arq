package br.com.damsete.arq.security.infraestructure.filter;

import br.com.damsete.arq.security.application.authenticate.dto.Credential;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RedirectLogonFilter implements EnvironmentAware, AfterLogonFilter {

    private static final String TENANT_HEADER = "X-TenantID";

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private Environment environment = null;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void processAfterLogon(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        if (response.isCommitted()) {
            return;
        }

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute("SPRING_SECURITY_SAVED_REQUEST");
        }

        determineTenant(authentication, session);

        String targetUrl = determineTargetUrl(authentication);
        this.redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    @Override
    public int getOrder() {
        return -1;
    }

    private void determineTenant(Authentication authentication, HttpSession session) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<String> roles = authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        if (!isTenant(roles)) {
            Credential credential = (Credential) authentication.getPrincipal();
            if (Optional.ofNullable(credential.getMultiTenant()).isPresent()) {
                session.setAttribute(TENANT_HEADER, credential.getMultiTenant().getSchema());
            }
        }
    }

    private String determineTargetUrl(Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<String> roles = authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        String url = "";
        if (isTenant(roles)) {
            url = this.environment.getProperty("damsete.security.tenant-url");
        } else {
            url = this.environment.getProperty("damsete.security.dashboard-url");
        }
        return url;
    }

    private boolean isTenant(List<String> roles) {
        return roles.stream().anyMatch(i -> i.equals("ROLE_TENANT"));
    }
}
