package br.com.damsete.arq.security.provider;

import br.com.damsete.arq.log.core.Logger;
import br.com.damsete.arq.message.ArqMessageSource;
import br.com.damsete.arq.security.application.AuthenticateService;
import br.com.damsete.arq.security.application.AuthenticateTenantService;
import br.com.damsete.arq.security.application.authenticate.command.Authenticate;
import br.com.damsete.arq.security.application.authenticate.dto.Credential;
import br.com.damsete.arq.security.application.authenticate.dto.MultiTenant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

@Component
public class MyAuthenticationProvider implements AuthenticationProvider {

    @Value("${damsete.authentication.from}")
    private String authenticationFrom = null;

    private AuthenticateTenantService authenticateTenantService;
    private AuthenticateService authenticateService;
    private ArqMessageSource messageSource;
    private Logger logger;

    @Autowired
    public MyAuthenticationProvider(AuthenticateTenantService authenticateTenantService,
                                    AuthenticateService authenticateService,
                                    ArqMessageSource messageSource,
                                    Logger logger) {
        this.authenticateTenantService = authenticateTenantService;
        this.authenticateService = authenticateService;
        this.messageSource = messageSource;
        this.logger = logger;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        this.logger.debug("authenticationProvider.authenticate.from >> " + this.authenticationFrom);
        this.logger.debug("authenticationProvider.authentication >> " + authentication);

        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        try {
            Authenticate authenticate = Authenticate.builder().
                    applicationName(this.authenticationFrom).
                    username(username).
                    password(password).
                    build();

            Credential credential = this.authenticateService.authenticate(authenticate);
            this.logger.debug("authenticationProvider.authenticate.credential >> " + credential);

            return new UsernamePasswordAuthenticationToken(credential, password, credential.getAuthorities());
        } catch (ResourceAccessException e) {
            throw new AuthenticationServiceException(this.messageSource.getMessage("access.token.request.error"));
        }
    }

    public Authentication authenticateByTenant(Authentication authentication) throws AuthenticationException {
        this.logger.debug("authenticationProvider.authenticateByTenant.from >> " + this.authenticationFrom);
        this.logger.debug("authenticationProvider.authenticateByTenant >> " + authentication);

        try {
            MultiTenant multiTenant = ((Credential) authentication.getPrincipal()).getMultiTenant();
            String username = ((Credential) authentication.getPrincipal()).getUsername();
            String password = ((Credential) authentication.getPrincipal()).getPassword();
            String token = ((Credential) authentication.getPrincipal()).getToken();

            Authenticate authenticate = Authenticate.builder().
                    applicationName(this.authenticationFrom).
                    tenant(multiTenant).
                    username(username).
                    password(token).
                    build();

            Credential credential = this.authenticateTenantService.authenticateByTenant(authenticate);
            this.logger.debug("authenticationProvider.authenticateByTenant.credential >> " + credential);

            return new UsernamePasswordAuthenticationToken(credential, password, credential.getAuthorities());
        } catch (ResourceAccessException e) {
            throw new AuthenticationServiceException(this.messageSource.getMessage("access.token.request.error"));
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
