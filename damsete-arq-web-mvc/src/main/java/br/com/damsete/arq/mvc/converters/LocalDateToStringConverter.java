package br.com.damsete.arq.mvc.converters;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateToStringConverter implements Converter<LocalDate, String> {

    @Override
    public String convert(LocalDate source) {
        if (source != null) {
            return DateTimeFormatter.ofPattern("dd/MM/yyyy").format(source);
        } else {
            return null;
        }
    }
}
