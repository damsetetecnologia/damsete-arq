package br.com.damsete.arq.mvc.exceptionhandler;

import br.com.damsete.arq.api.ApiError;
import br.com.damsete.arq.exception.BusinessException;
import br.com.damsete.arq.exception.MissingConfigurationException;
import br.com.damsete.arq.exception.WrongConfigurationException;
import br.com.damsete.arq.message.ArqMessageSource;
import br.com.damsete.arq.utils.Strings;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private ArqMessageSource messageSource;

    @Autowired
    public GlobalExceptionHandler(ArqMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = createErrors(ex.getBindingResult());
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, Strings.join(errors, "\r\n"));
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String message = this.messageSource.getMessage("resource.missing-servlet-request-parameter.message", ex.getParameterName());
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, message);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String message = this.messageSource.getMessage("resource.no-handler-found.message", ex.getHttpMethod(), ex.getRequestURL());
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, debugMessage, message);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String supportedMediaTypes = Strings.join(ex.getSupportedMediaTypes(), ", ");
        String message = this.messageSource.getMessage("resource.http-media-type-not-supported.message", ex.getContentType(), supportedMediaTypes);
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.UNSUPPORTED_MEDIA_TYPE, debugMessage, message);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String supportedHttpMethods = Strings.join(ex.getSupportedHttpMethods(), ", ");
        String message = this.messageSource.getMessage("resource.http-request-method-not-supported.message", ex.getMethod(), supportedHttpMethods);
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, debugMessage, message);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String message = this.messageSource.getMessage("resource.http-message-not-readable.message");
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, message);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @ExceptionHandler({EmptyResultDataAccessException.class})
    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
        String message = this.messageSource.getMessage("resource.empty-result-data-access.message");
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, debugMessage, message);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request) {
        String message = this.messageSource.getMessage("resource.data-integrity-violation.message");
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, message);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
        List<String> errors = newArrayList();
        ex.getConstraintViolations().forEach(violation -> errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " + violation.getMessage()));
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, Strings.join(errors, "\r\n"));
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Object> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex, WebRequest request) {
        String message = this.messageSource.getMessage("resource.method-argument-type-mismatch.message", ex.getName(), ex.getRequiredType().getName());
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, message);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity<Object> handleBusinessException(BusinessException ex, WebRequest request) {
        String message = this.messageSource.getMessage(ex.getMessageSourceResolvable());
        String debugMessage = "BusinessException";
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, message);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({MissingConfigurationException.class})
    public ResponseEntity<Object> handleMissingConfigurationException(MissingConfigurationException ex, WebRequest request) {
        String message = this.messageSource.getMessage(ex.getMessageSourceResolvable());
        String debugMessage = "MissingConfigurationException";
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, message);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({WrongConfigurationException.class})
    public ResponseEntity<Object> handleWrongConfigurationException(WrongConfigurationException ex, WebRequest request) {
        String message = this.messageSource.getMessage(ex.getMessageSourceResolvable());
        String debugMessage = "WrongConfigurationException";
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, debugMessage, message);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        String message = this.messageSource.getMessage("resource.internal-server-error.message");
        String debugMessage = ExceptionUtils.getRootCauseMessage(ex);
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, debugMessage, message);
        return handleExceptionInternal(ex, apiError, new HttpHeaders(), apiError.getStatus(), request);
    }

    private List<String> createErrors(BindingResult bindingResult) {
        List<String> errors = newArrayList();

        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errors.add(this.messageSource.getMessage(fieldError));
        }

        for (ObjectError objectError : bindingResult.getGlobalErrors()) {
            errors.add(this.messageSource.getMessage(objectError));
        }

        return errors;
    }
}
