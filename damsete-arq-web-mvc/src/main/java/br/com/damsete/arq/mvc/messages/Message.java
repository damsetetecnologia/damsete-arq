package br.com.damsete.arq.mvc.messages;

import lombok.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@Getter
@Setter
@Builder
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Message {

    private String message = null;
    private MessageType type = null;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public boolean isError() {
        return type != null && type.equals(MessageType.ERROR);
    }

    public boolean isInfo() {
        return type != null && type.equals(MessageType.INFO);
    }

    public boolean isSuccess() {
        return type != null && type.equals(MessageType.SUCCESS);
    }

    public boolean isWarning() {
        return type != null && type.equals(MessageType.WARNING);
    }

    public String getStyle() {
        return type != null ? type.style : "";
    }

    public enum MessageType {

        ERROR("alert-danger"), INFO("alert-info"), SUCCESS("alert-success"), WARNING("alert-warning");

        String style;

        MessageType(String style) {
            this.style = style;
        }
    }

}
