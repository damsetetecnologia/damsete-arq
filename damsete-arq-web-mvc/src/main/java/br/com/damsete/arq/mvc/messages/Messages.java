package br.com.damsete.arq.mvc.messages;

import br.com.damsete.arq.message.ArqMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

@Component
@Scope("prototype")
public class Messages {

    private List<Message> messages = newArrayList();
    private ArqMessageSource messageSource;

    @Autowired
    public Messages(ArqMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public void addError(Model model, MessageSourceResolvable... messages) {
        addMessages(Message.MessageType.ERROR, messages);
        showMessages(model);
    }

    public void addInfo(Model model, MessageSourceResolvable... messages) {
        addMessages(Message.MessageType.INFO, messages);
        showMessages(model);
    }

    public void addSuccess(Model model, MessageSourceResolvable... messages) {
        addMessages(Message.MessageType.SUCCESS, messages);
        showMessages(model);
    }

    public void addWarning(Model model, MessageSourceResolvable... messages) {
        addMessages(Message.MessageType.WARNING, messages);
        showMessages(model);
    }

    public String getMessage(MessageSourceResolvable message) {
        return this.messageSource.getMessage(message, Locale.getDefault());
    }

    private void addMessages(Message.MessageType messageType, MessageSourceResolvable... messages) {
        for (MessageSourceResolvable message : messages) {
            this.messages.add(new Message(getMessage(message), messageType));
        }
    }

    private void showMessages(Model model) {
        boolean hasErrors = !this.messages.isEmpty();
        if (hasErrors) {
            if (model instanceof RedirectAttributes) {
                RedirectAttributes redirectAttributes = (RedirectAttributes) model;
                redirectAttributes.addFlashAttribute("error", filterByType(Message.MessageType.ERROR));
                redirectAttributes.addFlashAttribute("info", filterByType(Message.MessageType.INFO));
                redirectAttributes.addFlashAttribute("success", filterByType(Message.MessageType.SUCCESS));
                redirectAttributes.addFlashAttribute("warning", filterByType(Message.MessageType.WARNING));
            } else {
                model.addAttribute("error", filterByType(Message.MessageType.ERROR));
                model.addAttribute("info", filterByType(Message.MessageType.INFO));
                model.addAttribute("success", filterByType(Message.MessageType.SUCCESS));
                model.addAttribute("warning", filterByType(Message.MessageType.WARNING));
            }
            this.messages = newArrayList();
        }
    }

    private List<Message> filterByType(Message.MessageType type) {
        return this.messages.stream().filter(item -> item.getType().equals(type)).collect(Collectors.toList());
    }
}
