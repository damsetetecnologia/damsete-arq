package br.com.damsete.arq.mvc.pagination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

@Getter
@Setter
public class PaginationRequest {

    private String draw;
    private int start;
    private int length;
    private List<PaginationSort> order;
    private PaginationSearch search;

    public PaginationRequest() {
        super();
    }

    @JsonIgnore
    public PaginationSort getSortBy() {
        return order.iterator().next();
    }

    @JsonIgnore
    public int getPage() {
        return getLength() > 0 ? Math.abs(getStart() / getLength()) : 0;
    }

    @JsonIgnore
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
