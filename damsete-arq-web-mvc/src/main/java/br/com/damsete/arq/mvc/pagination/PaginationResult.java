package br.com.damsete.arq.mvc.pagination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PaginationResult<T> {

    @JsonProperty("draw")
    private String draw;

    @JsonProperty("recordsFiltered")
    private String recordsFiltered;

    @JsonProperty("recordsTotal")
    private String recordsTotal;

    @JsonProperty("data")
    private List<T> values;

    public PaginationResult(List<T> values, String draw, String recordsTotal) {
        this.draw = draw;
        this.values = values;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsTotal;
    }

    @JsonIgnore
    public String toJson() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            return mapper.writeValueAsString(this);
        } catch (Exception e) {
            return "";
        }
    }
}
